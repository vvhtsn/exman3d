﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Notification))]
public class NotificationEditor : Editor
{
    private string titleText = "Test";
    private string contentText = "This is a test notification";
    private Sprite contentImage = null;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.Space(20);

        GUILayout.Label("Test Notification", EditorStyles.boldLabel);

        titleText = EditorGUILayout.TextField("Title", titleText);
        contentText = EditorGUILayout.TextField("Content", contentText);
        contentImage = (Sprite)EditorGUILayout.ObjectField("Image", contentImage, typeof(Sprite), true);

        if (!EditorApplication.isPlaying)
            GUI.enabled = false;

        if (GUILayout.Button("Show"))
        {
            (target as Notification).Display(titleText, contentText, contentImage, autoHideDelay: 2.5f);
        }

        if (!EditorApplication.isPlaying)
            GUI.enabled = true;
    }
}