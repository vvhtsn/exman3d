﻿using System.CodeDom;
using System.CodeDom.Compiler;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

[InitializeOnLoad]
public class CompiledTags
{
    private const string savePath = "Scripts/";
    private const string className = "Tags";

    private static bool initialized;
    private static int currentFrame = 0;
    private const int runOnFrame = 25;
    private static bool windowHadFocus;

    private static string[] listCopy;

    private static UnityEvent onListChanged = new UnityEvent();

    static CompiledTags()
    {
        EditorApplication.update += Update;
        onListChanged.AddListener(() =>
        {
            Debug.Log("<B><size=10><color=purple>Updating tags file.</color></size></B>");
            WriteFile(className, listCopy);
        });

        string baseDir = Path.Combine(Application.dataPath, savePath);
        if (!Directory.Exists(baseDir))
            Directory.CreateDirectory(baseDir);
        string saveFileName = Path.Combine(baseDir, className + ".cs");

        if (!File.Exists(saveFileName))
            ForceUpdate();
    }

    private static void Update()
    {
        Init();

        if (++currentFrame >= runOnFrame)
        {
            currentFrame = 0;

            bool hasFocus = InspectorHasFocus();

            if (windowHadFocus && !hasFocus)
            {
                if (!CheckList())
                {
                    FillList();
                    onListChanged.Invoke();
                }
            }

            windowHadFocus = hasFocus;
        }
    }

    [MenuItem("Assets/Update Tags")]
    private static void ForceUpdate()
    {
        FillList();
        onListChanged.Invoke();
    }

    private static bool InspectorHasFocus()
    {
        if (Selection.activeObject == GetAsset())
        {
            if (EditorWindow.focusedWindow != null && EditorWindow.focusedWindow.titleContent != null)
            {
                if (!string.IsNullOrEmpty(EditorWindow.focusedWindow.titleContent.text))
                {
                    if (EditorWindow.focusedWindow.titleContent.text == "Inspector")
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static void Init()
    {
        if (initialized)
        {
            return;
        }

        FillList();
        initialized = true;
    }

    private static void FillList()
    {
        var a = GetProperty("tags");
        if (!a.isArray)
        {
            Debug.LogError("Property is not an array!");
            return;
        }

        listCopy = new string[a.arraySize];
        for (int i = 0; i < a.arraySize; i++)
        {
            listCopy[i] = a.GetArrayElementAtIndex(i).stringValue;
        }
    }

    private static bool CheckList()
    {
        var a = GetProperty("tags");
        if (a.arraySize != listCopy.Length)
            return false;

        for (int i = 0; i < a.arraySize; i++)
        {
            if (!a.GetArrayElementAtIndex(i).stringValue.Equals(listCopy[i]))
            {
                return false;
            }
        }

        return true;
    }

    private static SerializedProperty GetProperty(string name)
    {
        return new SerializedObject(GetAsset()).FindProperty(name);
    }

    private static Object GetAsset()
    {
        return AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/TagManager.asset")[0];
    }

    private static void WriteFile(string className, string[] array)
    {
        var unit = new CodeCompileUnit();
        CodeNamespace newNamespace = new CodeNamespace("");
        CodeTypeDeclaration newClass = new CodeTypeDeclaration(className);
        newClass.IsClass = true;
        newClass.TypeAttributes = TypeAttributes.Public;
        newNamespace.Types.Add(newClass);

        foreach (var item in array)
        {
            CodeMemberField member = new CodeMemberField(typeof(string), ParseName(item));
            member.Attributes = MemberAttributes.Const | MemberAttributes.Public;
            member.InitExpression = new CodePrimitiveExpression(item);
            newClass.Members.Add(member);
        }

        unit.Namespaces.Add(newNamespace);

        CodeDomProvider provider = CodeDomProvider.CreateProvider("CSharp");
        CodeGeneratorOptions options = new CodeGeneratorOptions();
        options.BracingStyle = "C";
        options.BlankLinesBetweenMembers = true;
        string baseDir = Path.Combine(Application.dataPath, savePath);
        if (!Directory.Exists(baseDir))
            Directory.CreateDirectory(baseDir);
        string saveFileName = Path.Combine(baseDir, className + ".cs");
        using (StreamWriter sourceWriter = new StreamWriter(saveFileName))
        {
            provider.GenerateCodeFromCompileUnit(unit, sourceWriter, options);
        }
        AssetDatabase.ImportAsset(Path.Combine(Path.Combine("Assets/", savePath), className + ".cs"));
    }

    private static string ParseName(string name)
    {
        name = name.Trim().Replace(" ", "_");
        return char.ToLowerInvariant(name[0]) + name.Substring(1);
    }
}