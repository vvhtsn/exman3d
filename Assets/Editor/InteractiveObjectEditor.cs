﻿using ExMan.Components;
using UnityEditor;

[CustomEditor(typeof(InteractiveObject))]
public class InteractiveObjectEditor : Editor
{
    public override void OnInspectorGUI()
    {
        InteractiveObject t = (InteractiveObject)target;

        EditorGUILayout.PropertyField(serializedObject.FindProperty("requiresObjectInRange"));

        if (t.requiresObjectInRange)
        {
            EditorGUILayout.PropertyField(serializedObject.FindProperty("requiredRange"));
            t.requiredTag = EditorGUILayout.TagField("Required Tag", t.requiredTag);
        }

        EditorGUILayout.PropertyField(serializedObject.FindProperty("mouseEnter"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("mouseExit"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("mouseDown"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("mouseUp"));

        serializedObject.ApplyModifiedProperties();
    }
}