﻿using UnityEditor;
using UnityEngine;

public class AnchorToBoundsEditor : Editor
{
    [MenuItem("CONTEXT/RectTransform/Achors to bounds", false)]
    private static void AnchorsToBounds()
    {
        RectTransform rt = Selection.activeTransform as RectTransform;
        RectTransform pt = rt.parent as RectTransform;

        // http://answers.unity3d.com/questions/782478/unity-46-beta-anchor-snap-to-button-new-ui-system.html
        Vector2 newAnchorsMin = new Vector2(rt.anchorMin.x + rt.offsetMin.x / pt.rect.width,
                                            rt.anchorMin.y + rt.offsetMin.y / pt.rect.height);
        Vector2 newAnchorsMax = new Vector2(rt.anchorMax.x + rt.offsetMax.x / pt.rect.width,
                                            rt.anchorMax.y + rt.offsetMax.y / pt.rect.height);

        rt.anchorMin = newAnchorsMin;
        rt.anchorMax = newAnchorsMax;
        rt.offsetMin = rt.offsetMax = Vector2.zero;
    }

    [MenuItem("CONTEXT/RectTransform/Bounds to anchor", false)]
    private static void BoundsToAnchors()
    {
        RectTransform rt = Selection.activeTransform as RectTransform;
        rt.offsetMin = rt.offsetMax = Vector2.zero;
    }
}