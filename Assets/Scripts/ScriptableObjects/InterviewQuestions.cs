﻿using System.Collections.Generic;
using UnityEngine;

namespace ExMan.ScriptableObjects
{
    [CreateAssetMenu(menuName = "ExMan/Interview questions")]
    public class InterviewQuestions : ScriptableObject
    {
        public List<Question> questions = new List<Question>();

        [System.Serializable]
        public struct Question
        {
            public string question;
            public AudioClip questionClip;
            public List<Answer> answer;
            public string followUpQuestion;
            public AudioClip followUpQuestionClip;
            public List<Answer> followUpAnswer;

            public Question(string question, AudioClip questionClip, AudioClip followUpClip, params Answer[] answers)
            {
                this.question = question;
                this.questionClip = questionClip;
                this.followUpQuestionClip = followUpClip;
                answer = new List<Answer>(answers);
                followUpQuestion = "";
                followUpAnswer = new List<Answer>();
            }

            [System.Serializable]
            public struct Answer
            {
                public Chef chef;
                public string answer;
                public AudioClip clip;
            }
        }
    }
}