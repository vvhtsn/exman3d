﻿using UnityEngine;

namespace ExMan.ScriptableObjects
{
    [CreateAssetMenu(menuName = "ExMan/Vehicle")]
    public class Vehicle : ScriptableObject
    {
        public new string name;
        public GameObject prefab;
        public LicenseType requiredLicense;
        public float cost;
        public float carryingVolume;
        public float maxDrivingRange;
        public Sprite portrait;
        public float costPerKm;
        public int maxMealsPerDelivery;

        public enum LicenseType
        {
            None,
            Basic,
            Advanced
        }
    }
}