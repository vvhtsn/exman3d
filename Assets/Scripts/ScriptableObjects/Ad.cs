﻿using UnityEngine;

namespace ExMan.ScriptableObjects
{
    [CreateAssetMenu(menuName = "ExMan/Ad")]
    public class Ad : ScriptableObject
    {
        public new string name;
        public float cost;
        public int minResumes;
        public int maxResumes;
        public Sprite thumbnail;
    }
}