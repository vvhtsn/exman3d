﻿using UnityEngine;

namespace ExMan.ScriptableObjects
{
    [CreateAssetMenu(menuName = "ExMan/Character")]
    public class Character : ScriptableObject
    {
        public new string name;
        public GameObject prefab;
        public GameObject sceneObject;
        public Vehicle.LicenseType vehicleLicense;
        public float skill;
        public float cost;
        public Sprite portrait;
        public Texture2D neutralPicture;
    }
}