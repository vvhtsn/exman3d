﻿using UnityEngine;

namespace ExMan.ScriptableObjects
{
    [CreateAssetMenu(menuName = "ExMan/Chef")]
    public class Chef : Character
    {
        [TextArea]
        public string resume;
    }
}