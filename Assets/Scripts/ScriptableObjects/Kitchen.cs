﻿using UnityEngine;

namespace ExMan.ScriptableObjects
{
    [CreateAssetMenu(menuName = "ExMan/Kitchen")]
    public class Kitchen : ScriptableObject
    {
        public new string name;
        public GameObject prefab;
        public float cost;
        public float monthlyCost;
        public Character[] staff;
        public Sprite portrait;
        public int maxMeals;
    }
}