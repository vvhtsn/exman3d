﻿namespace ExMan.Tasks
{
    [System.Serializable]
    public abstract class Task
    {
        public TaskWrapper parent;

        public string name
        {
            get; protected set;
        }

        public virtual void StartTask(TaskWrapper parent)
        {
            this.parent = parent;
        }

        public virtual void Update()
        {
        }

        public virtual void Destroy()
        {
        }
    }
}