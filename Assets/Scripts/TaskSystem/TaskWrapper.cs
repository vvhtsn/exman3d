﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ExMan.Tasks
{
    public class TaskWrapper : ICloneable
    {
        public bool childsDone
        {
            get
            {
                return childTasks.Count == 0 && currentChild == null;
            }
        }

        public Task task;
        public TaskWrapper currentChild;
        public TaskWrapper parent;
        public Queue<TaskWrapper> childTasks = new Queue<TaskWrapper>();
        public bool isTopLevel;
        public GameObject pathfinderObject;
        public string waypointName;

        public string name;

        public TaskWrapper(Task task, bool isTopLevel, params TaskWrapper[] childs)
        {
            this.task = task;
            for (int i = 0; i < childs.Length; i++)
            {
                childTasks.Enqueue(childs[i]);
            }
            this.isTopLevel = isTopLevel;
            this.name = task.name;
        }

        public TaskWrapper(Task task, string waypointName, bool isTopLevel, params TaskWrapper[] childs)
        {
            this.task = task;
            for (int i = 0; i < childs.Length; i++)
            {
                childTasks.Enqueue(childs[i]);
            }
            this.isTopLevel = isTopLevel;
            this.name = task.name;
            this.waypointName = waypointName;
        }

        public TaskWrapper(Task task, bool isTopLevel, List<TaskWrapper> childs)
        {
            this.task = task;
            for (int i = 0; i < childs.Count; i++)
            {
                childTasks.Enqueue(childs[i]);
            }
            this.isTopLevel = isTopLevel;
            this.name = task.name;
        }

        /// <summary>
        /// Starts the next available child task
        /// </summary>
        public void GoToNextChild()
        {
            if (currentChild != null)
            {
                currentChild.Destroy();
                currentChild = null;
            }
            if (childTasks.Count != 0)
            {
                currentChild = childTasks.Dequeue();
                currentChild.StartTask(this);
            }
        }

        /// <summary>
        /// Completes the current task and prompts the parent <see cref="TaskWrapper"/> to go the next child
        /// </summary>
        public void CompleteTask()
        {
            if (!isTopLevel)
                parent.GoToNextChild();
            else
                TaskManager.CompleteTask();

            
        }

        public void StartTask(TaskWrapper parent)
        {
            this.parent = parent;
            task.StartTask(this);
            GoToNextChild();

            if (this.waypointName != null)
            {
                GameObject.FindWithTag("Pathfinder").GetComponent<PathFinding>().SetWaypoint(waypointName);
            }
        }

        public void Update()
        {
            if (task != null)
                task.Update();
            if (currentChild != null)
                currentChild.Update();
        }

        public void Destroy()
        {
            if (task != null)
            {
                task.Destroy();
                task = null;
            }
            if (currentChild != null)
            {
                currentChild.Destroy();
                currentChild = null;
            }

            GameObject.FindWithTag("Pathfinder").GetComponent<PathFinding>().ClearWaypoint();
        }

        public object Clone()
        {
            TaskWrapper t = new TaskWrapper(task, isTopLevel);
            foreach (var item in childTasks)
            {
                TaskWrapper clonedChild = (TaskWrapper)item.Clone();
                clonedChild.parent = t;
                t.childTasks.Enqueue(clonedChild);
            }
            if (currentChild != null)
                t.currentChild = (TaskWrapper)currentChild.Clone();

            return t;
        }
    }
}