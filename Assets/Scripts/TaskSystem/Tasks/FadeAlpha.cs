﻿using ExMan.Time;
using UnityEngine;

namespace ExMan.Tasks
{
    public class FadeAlpha : Task
    {
        private float target;
        private string text;
        private bool isDayChanging = false;

        public FadeAlpha(float val, string text = "", bool dayChange = false)
        {
            this.name = GetType().Name;
            target = val;
            this.isDayChanging = dayChange;

            if (text == "") {
                
            } else
            {
                this.text = text;
            }
            
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);

            if (isDayChanging)
                this.text = "Day " + (TimeManager.currentDay + TimeManager.GetDaysTillNextEvent());

            ExMan.UI.Fade.FadeAlpha(target, .75f, () =>
            {
                parent.CompleteTask();
            }, this.text);
        }
    }
}