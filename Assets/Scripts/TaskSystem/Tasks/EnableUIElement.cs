﻿using DG.Tweening;
using ExMan.UI;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan.Tasks
{
    /// <summary>
    /// Enables a UI panel
    /// </summary>
    public class EnableUIElement<T> : Task where T : UIElementTask
    {
        public UIElementTask element;

        public EnableUIElement(string tag)
        {
            this.name = GetType().Name + " " + typeof(T).Name + ": " + tag;

            element = Utils.CustomFind.FindComponentsWithTag<T>(tag, true);
            element.enabled = true;

            if (element == null)
                return;

            element.gameObject.SetActive(false);
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            if (element != null)
            {
                UnityEvent action = new UnityEvent();
                action.AddListener(() => { });
                element.StartTask(action);
                action.AddListener(Complete);

                element.gameObject.SetActive(true);
                RectTransform rt = (RectTransform)element.transform;
                Vector3 pos = rt.anchoredPosition;
                pos.x = -Screen.width;
                rt.anchoredPosition = pos;
                rt.DOAnchorPosX(0, Constants.uiTweenTime).SetEase(Constants.uiEaseStartType);
            }
        }

        public void Complete()
        {
            if (element != null)
            {
                RectTransform rt = (RectTransform)element.transform;
                rt.DOAnchorPosX(Screen.width, Constants.uiTweenTime).SetId("UIPanelTween").SetEase(Constants.uiEaseEndType).OnComplete(() =>
                {
                    element.gameObject.SetActive(false);
                });
            }
            parent.CompleteTask();
        }
    }
}