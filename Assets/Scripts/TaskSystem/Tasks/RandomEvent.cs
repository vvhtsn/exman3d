﻿namespace ExMan.Tasks
{
    public class RandomEvent : Task
    {
        public RandomEvent()
        {
            this.name = GetType().Name;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            parent.CompleteTask();
        }
    }
}