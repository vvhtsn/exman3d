﻿using ExMan.Components;
using ExMan.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan.Tasks
{
    /// <summary>
    /// Completes task wehen the given area is triggered
    /// </summary>
    public class ActivateTriggerArea : Task
    {
        protected TriggerEvent area;
        private bool activatedPrompt = false;
        private float promptDelay;
        private float startTime;

        private string notificationTitle;
        private string notificationContent;
        private Sprite notificationImage;

        public string waypointName;

        public ActivateTriggerArea(string areaTag, float promptDelay = 0, string notificationTitle = null, string notificationContent = null, Sprite notificationImage = null, string waypoint = null)
        {
            this.name = GetType().Name + ": " + areaTag;
            this.promptDelay = promptDelay;

            this.notificationTitle = notificationTitle;
            this.notificationContent = notificationContent;
            this.notificationImage = notificationImage;

            area = CustomFind.FindComponentsWithTag<TriggerEvent>(areaTag, true);

            if (area == null)
                Debug.LogWarningFormat("No matching area({0}) found.", areaTag);
            else
                area.gameObject.SetActive(false);

            this.waypointName = waypoint;
        }

        public override void Update()
        {
            base.Update();
            if (UnityEngine.Time.time >= startTime + promptDelay && !activatedPrompt)
            {
                area.StartPrompt();

                if (!string.IsNullOrEmpty(notificationTitle) && !string.IsNullOrEmpty(notificationContent))
                {
                    Notification.Show(notificationTitle, notificationContent, notificationImage, false);

                    if (this.waypointName != null)
                    {
                        GameObject.FindWithTag("Pathfinder").GetComponent<PathFinding>().SetWaypoint(waypointName);
                    }
                }

                activatedPrompt = true;
            }

            if (Input.GetKeyDown(KeyCode.F10) && Application.isEditor)
                if (area != null)
                    area.CompleteTask();
                else
                    Complete();
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            startTime = UnityEngine.Time.time;

            if (area != null)
            {
                area.gameObject.SetActive(true);

                UnityEvent action = new UnityEvent();
                action.AddListener(() => { });
                area.StartTask(action);
                action.AddListener(Complete);

                if (promptDelay == 0)
                {
                    area.StartPrompt();

                    if (!string.IsNullOrEmpty(notificationTitle) && !string.IsNullOrEmpty(notificationContent))
                    {
                        Notification.Show(notificationTitle, notificationContent, notificationImage, false);

                        if (this.waypointName != null)
                        {
                            GameObject.FindWithTag("Pathfinder").GetComponent<PathFinding>().SetWaypoint(waypointName);
                        }
                    }

                    activatedPrompt = true;
                }
            }
        }

        public void Complete()
        {
            Notification.Dismiss();
            if (area != null)
            {
                area.gameObject.SetActive(false);
            }
            parent.CompleteTask();
        }
    }
}