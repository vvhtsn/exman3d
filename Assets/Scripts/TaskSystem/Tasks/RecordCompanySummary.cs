﻿using ExMan.Objects;
using ExMan;

namespace ExMan.Tasks
{
    public class RecordCompanySummary : Task
    {
        private string title;
        private bool compareToPrevious;

        public RecordCompanySummary(string title, bool compareToPrevious = false)
        {
            this.name = GetType().Name;
            this.title = title;
            this.compareToPrevious = compareToPrevious;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            Logger.Log("<b>" + title + "</b>");
            Company company = CompanyManager.GetCompany();
            Summary.RecordEntry(title, company.customerSatisfaction, company.staffSatisfaction, company.currentMoney, compareToPrevious);
            parent.CompleteTask();
        }
    }
}
