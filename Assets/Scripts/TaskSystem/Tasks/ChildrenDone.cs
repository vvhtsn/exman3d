﻿namespace ExMan.Tasks
{
    public class ChildrenDone : Task
    {
        public ChildrenDone()
        {
            this.name = GetType().Name;
        }

        public override void Update()
        {
            base.Update();
            if (parent.childsDone)
                parent.CompleteTask();
        }
    }
}