﻿using ExMan.UI;
using UnityEngine;

namespace ExMan.Tasks
{
    [System.Serializable]
    public class GameOpening : Task
    {
        public override void Update()
        {
            base.Update();
            if (parent.childsDone)
                parent.CompleteTask();
        }

        public static TaskWrapper GetOpeningTask()
        {
            TaskWrapper recordSummaryBefore = new TaskWrapper(new RecordCompanySummary("Start of Company Setup"), false);
            TaskWrapper openingScreen = new TaskWrapper(new EnableUIElement<SimpleUIElement>(Tags.openingPanel), false);
            TaskWrapper managerSelectScreen = new TaskWrapper(new EnableUIElement<ManagerSelect>(Tags.managerSelect), false);
            TaskWrapper foodForAllScene = new TaskWrapper(new StartScene("FoodForAll"), false);
            //TaskWrapper bankManagerLoanGranted = new TaskWrapper(new StartDialogue("BankManagerLoanGranted"), false);
            TaskWrapper companyCreation = new TaskWrapper(new EnableUIElement<CompanyCreation>(Tags.companyCreation), false);
            //TaskWrapper bankManagerOutro = new TaskWrapper(new StartDialogue("BankManagerOutro"), false);
            //TaskWrapper enteringBuilding = new TaskWrapper(new StartScene("EnteringBuilding"), false);
            TaskWrapper skipToNextEvent = new TaskWrapper(new SkipToNextEvent(), false);
            TaskWrapper recordSummaryAfter = new TaskWrapper(new RecordCompanySummary("End of Company Setup"), false);
            TaskWrapper saveGame = new TaskWrapper(new SaveGame(), false);
            TaskWrapper openingTask = new TaskWrapper(new GameOpening(), true, openingScreen, recordSummaryBefore, managerSelectScreen, foodForAllScene, companyCreation, skipToNextEvent, recordSummaryAfter, saveGame);
            return openingTask;
        }

        public override void Destroy()
        {
            base.Destroy();
            Debug.Log("Opening Complete");
        }
    }
}