﻿using ExMan.Dialogue;
using System;
using UnityEngine;

namespace ExMan.Tasks
{
    public class StartDialogue : Task
    {
        public string dialogue;
        private Action doneCallback;
        // Character that moves to position at the beggining of the dialog
        private GameObject[] characters;
        // Position where character moves at the beggining of the dialog
        private Vector3 position;

        public StartDialogue(string dialogue)
        {
            this.dialogue = dialogue;
            this.name = GetType().Name + ": " + dialogue;
        }

        public StartDialogue(string dialogue, Action doneCallback)
        {
            this.dialogue = dialogue;
            this.doneCallback = doneCallback;
            this.name = GetType().Name + ": " + dialogue;
        }

        public StartDialogue(string dialogue, Action doneCallback, GameObject[] characters, Vector3 position)
        {
            this.dialogue = dialogue;
            this.doneCallback = doneCallback;
            this.characters = characters;
            this.position = position;
            this.name = GetType().Name + ": " + dialogue;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);

            if (dialogue.Contains("Chef") && !string.IsNullOrEmpty(CompanyManager.GetCompany().selectedChef.name))
            {
                this.dialogue = dialogue.Replace("Chef", CompanyManager.GetCompany().selectedChef.name);
            }
            this.name = GetType().Name + ": " + dialogue;

            TaskManager.SetPaused(true);
            DialogueNodeCanvas dialogueCanvas = Resources.Load<DialogueNodeCanvas>("Dialogue/" + dialogue);
            if (dialogueCanvas != null)
            {
                if (characters != null)
                {
                    DialogueManager.StartDialogue(dialogueCanvas, dialogue, OnDialogueCompleted, characters, position, 1);
                }
                else
                {
                    DialogueManager.StartDialogue(dialogueCanvas, dialogue, OnDialogueCompleted);
                }
            }
        }

        private void OnDialogueCompleted()
        {
            TaskManager.SetPaused(false);
            parent.CompleteTask();
            if (doneCallback != null)
            {
                doneCallback();
            }
        }
    }
}