﻿namespace ExMan.Tasks
{
    public class StartScene : Task
    {
        public string scene;

        public StartScene(string scene)
        {
            this.scene = scene;
            this.name = GetType().Name + ": " + scene;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            VideoPlayer.PlayMovie("Videos/" + scene + ".mp4", OnSceneCompleted);
            TaskManager.SetPaused(true);
        }

        private void OnSceneCompleted()
        {
            TaskManager.SetPaused(false);
            parent.CompleteTask();
        }
    }
}