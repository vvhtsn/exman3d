﻿namespace ExMan.Tasks
{
    public class MonthlySummary : Task
    {
        public MonthlySummary()
        {
            this.name = GetType().Name;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            parent.CompleteTask();
        }
    }
}