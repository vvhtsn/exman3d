﻿using UnityEngine;

namespace ExMan.Tasks
{
    /// <summary>
    /// Waits for X Seconds
    /// </summary>
    public class WaitForSeconds : Task
    {
        private float endTime;
        private float length;

        public WaitForSeconds(float time)
        {
            length = time;
            this.name = GetType().Name + ": " + time + "s";
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            endTime = UnityEngine.Time.time + length;
        }

        public override void Update()
        {
            base.Update();
            if (UnityEngine.Time.time >= endTime || (Input.GetKeyDown(KeyCode.F10) && Application.isEditor))
                parent.CompleteTask();
        }
    }
}