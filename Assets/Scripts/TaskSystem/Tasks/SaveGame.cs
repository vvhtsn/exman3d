﻿using UnityEngine;

namespace ExMan.Tasks
{
    public class SaveGame : Task
    {
        public SaveGame()
        {
            this.name = GetType().Name;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            // Saving always happens when scene is completed
            SceneQueue.instance.scenesCompleted++;
            SaveLoad.instance.Save();
            Debug.Log("Game Saved");
            parent.CompleteTask();
        }
    }
}
