﻿using ExMan.Objects;
using ExMan.Time;
using ExMan.UI;
using UnityEngine;

namespace ExMan.Tasks
{
    /// <summary>
    /// Checks if there is an order and if it can be completed, if there isn't an order it prompts the user to accept a new one.
    /// </summary>
    public class StartOrder : EnableUIElement<OrderInitializer>
    {
        private bool isFirstOrder;

        public StartOrder(bool isFirstOrder) : base(Tags.order)
        {
            this.isFirstOrder = isFirstOrder;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            SetupOrder();
        }

        private void SetupOrder()
        {
            if (element)
            {
                OrderInitializer orderUI = (OrderInitializer)element;
                orderUI.Reset();
                orderUI.isFirstOrder = isFirstOrder;
                if (orderUI != null)
                {
                    Order order = OrderQueue.GetCurrentOrder();
                    if (order == null)
                    {
                        order = OrderQueue.GetNextOrder();
                        if (order != null)
                        {
                            orderUI.SetOrder(order, () =>
                            {
                                OrderQueue.AcceptOrder(order);
                            }, () =>
                            {
                                TimeEvent t = new TimeEvent("Random Order", new TaskWrapper(new StartOrder(false), false), false);
                                TimeManager.RegisterTimeEvent(TimeManager.currentDay + 4, t);
                            //SetupOrder();
                        });
                        }
                        else
                        {
                            Debug.Log("I suck at getting orders");
                            Debug.Break();
                        }
                    }
                }
            }
        }
    }
}