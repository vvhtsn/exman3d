﻿using ExMan.UI;
using UnityEngine;

namespace ExMan.Tasks
{
    public class BudgetScene : Task
    {
        public BudgetScene()
        {
            this.name = GetType().Name;
        }

        public override void Update()
        {
            base.Update();
            if (parent.childsDone)
                parent.CompleteTask();
        }

        public static TaskWrapper GetBudgetSceneTask()
        {
            TaskWrapper recordSummaryBefore = new TaskWrapper(new RecordCompanySummary("Start of Company Budget", true), false);
            TaskWrapper disableHud = new TaskWrapper(new ToggleGameObject(false, Tags.hudCanvas), false);
            TaskWrapper enableHud = new TaskWrapper(new ToggleGameObject(true, Tags.hudCanvas), false);
            TaskWrapper buyAnAd = new TaskWrapper(new EnableUIElement<BuyAnAd>(Tags.buyAnAd), false);
            TaskWrapper employAChef = new TaskWrapper(new EnableUIElement<EmployAChef>(Tags.employAChef), false);
            TaskWrapper miscCompanySettings = new TaskWrapper(new EnableUIElement<MiscCompanySettings>(Tags.miscCompanySettings), false);
            TaskWrapper recordSummaryAfter = new TaskWrapper(new RecordCompanySummary("End of Company Budget"), false);
            TaskWrapper saveGame = new TaskWrapper(new SaveGame(), false);
            TaskWrapper budgetSceneTask = new TaskWrapper(new BudgetScene(), false, recordSummaryBefore, disableHud, buyAnAd, employAChef, miscCompanySettings, enableHud, recordSummaryAfter, saveGame);
            return budgetSceneTask;
        }

        public override void Destroy()
        {
            base.Destroy();
            Debug.Log("Budget Scene Complete");
            //Listeners for GameOver
            Dialogue.DialogueManager.RegisterEvent("GameOver", () => GameOver.EndGame(Dialogue.DialogueManager.instance.gameOverReason));
            var company = CompanyManager.GetCompany();
            company.onStaffSatisfactionChanged.AddListener(() => { if (company.staffSatisfaction < -100) GameOver.EndGame("Your staff satisfaction dropped too low"); });
            company.onCustomerSatisfactionChanged.AddListener(() => { if (company.customerSatisfaction < -100) GameOver.EndGame("Your customer satisfaction dropped too low"); });
            company.onCurrentMoneyChanged.AddListener(() => { if (company.currentMoney < -100) GameOver.EndGame("Your ran out of money"); });
        }
    }
}