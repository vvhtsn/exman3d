﻿using UnityEngine;

namespace ExMan.Tasks
{
    /// <summary>
    /// Waits for X seconds or till the given area is triggered
    /// </summary>
    public class WaitForSecondsOrTrigger : ActivateTriggerArea
    {
        private float endTime;
        private float length;

        public WaitForSecondsOrTrigger(float time, string tag, float promptDelay = 0f, string notificationTitle = null, string notificationContent = null, Sprite notificationImage = null, string waypoint = null)
            : base(tag, promptDelay: promptDelay, notificationTitle: notificationTitle, notificationContent: notificationContent, notificationImage: notificationImage, waypoint: waypoint)
        {
            length = UnityEngine.Time.time + time;
            this.name = GetType().Name + ": " + time + "s or ActivateTrigger: " + tag;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            endTime = UnityEngine.Time.time + length;
        }

        public override void Update()
        {
            base.Update();
            if (UnityEngine.Time.time >= endTime)
                if (area != null)
                    area.CompleteTask();
                else
                    Complete();
        }
    }
}