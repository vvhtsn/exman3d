﻿using ExMan.Time;
using UnityEngine;

namespace ExMan.Tasks
{
    /// <summary>
    /// Calculates the days to the next available event and skips that many days
    /// </summary>
    public class SkipToNextEvent : Task
    {
        public SkipToNextEvent()
        {
            this.name = GetType().Name;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            int days = TimeManager.GetDaysTillNextEvent();
            if (days != -1)
            {
                TimeManager.SkipDays(days);
                Debug.Log("It is now day: " + TimeManager.currentDay);
            }
            parent.CompleteTask();
        }
    }
}