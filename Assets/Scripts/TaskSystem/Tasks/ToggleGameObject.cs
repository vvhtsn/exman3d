﻿using ExMan.Utils;
using UnityEngine;

namespace ExMan.Tasks
{
    public class ToggleGameObject : Task
    {
        private bool state;
        private string tag;

        public ToggleGameObject(bool state, string tag)
        {
            this.state = state;
            this.tag = tag;
            this.name = GetType().Name;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            try
            {
                RandomWaypointMove moveController = Dialogue.DialogueManager.CurrentMovingCharacter.GetComponent<RandomWaypointMove>();
                moveController.onReachDestination += MoveController_onReachDestination;
            }
            catch
            {
                GameObject go = CustomFind.FindComponentsWithTag<Transform>(tag, true).gameObject;
                go.SetActive(state);
                parent.CompleteTask();
            }
        }

        private void MoveController_onReachDestination()
        {
            GameObject go = CustomFind.FindComponentsWithTag<Transform>(tag, true).gameObject;
            go.SetActive(state);
            parent.CompleteTask();
        }
    }
}