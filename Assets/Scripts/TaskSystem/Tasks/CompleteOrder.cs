﻿using ExMan.Objects;
using ExMan.UI;

namespace ExMan.Tasks
{
    public class CompleteOrder : EnableUIElement<SimpleTextPanel>
    {
        private Order order;

        public CompleteOrder(string tag, Order order) : base(tag)
        {
            this.order = order;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            SimpleTextPanel panel = element as SimpleTextPanel;
            element.gameObject.SetActive(true);
            OrderQueue.CompleteOrder();
            if (panel != null)
            {
                panel.SetTitle("Completed order for " + order.company);
                panel.SetText(order.ToStringCompleted());
            }
        }
    }
}