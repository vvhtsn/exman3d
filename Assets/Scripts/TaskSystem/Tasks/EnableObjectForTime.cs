﻿using DG.Tweening;
using UnityEngine;

namespace ExMan.Tasks
{
    /// <summary>
    /// Enables an object for X time
    /// </summary>
    public class EnableObjectForTime : Task
    {
        public GameObject element;
        public float length;
        private float endTime;

        public EnableObjectForTime(string panelTag, float enabledTime)
        {
            element = Utils.CustomFind.FindComponentsWithTag<Transform>(panelTag, true).gameObject;
            if (element == null)
                return;
            element.SetActive(false);
            this.name = GetType().Name + ": " + panelTag + " for " + enabledTime + "s";
            this.length = UnityEngine.Time.time + enabledTime;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            if (element != null)
                element.SetActive(true);
            endTime = UnityEngine.Time.time + length;
        }

        public override void Update()
        {
            base.Update();
            if (UnityEngine.Time.time >= endTime || (Input.GetKeyDown(KeyCode.F10) && Application.isEditor))
                OnPanelDone();
        }

        private void OnPanelDone()
        {
            RectTransform rt = (RectTransform)element.transform;
            rt.DOAnchorPosX(Screen.width, Constants.uiTweenTime).SetId("UIPanelTween").SetEase(Constants.uiEaseEndType).OnComplete(() =>
            {
                element.gameObject.SetActive(false);
            });
            parent.CompleteTask();
        }
    }
}