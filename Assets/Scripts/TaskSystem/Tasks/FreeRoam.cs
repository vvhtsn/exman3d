﻿using ExMan.UI;
using UnityEngine;

namespace ExMan.Tasks
{
    public class FreeRoam : Task
    {
        public string playerTag = "Player";
        public string playerSpawnTag = "SpawnPoint";
        private GameObject playerInstance;
        private GameObject playerPrefab;

        public FreeRoam()
        {
            this.name = GetType().Name;
        }

        public override void StartTask(TaskWrapper parent)
        {
            base.StartTask(parent);
            GameObject[] alivePlayers = GameObject.FindGameObjectsWithTag(playerTag);
            for (int i = 0; i < alivePlayers.Length; i++)
            {
                GameObject.Destroy(alivePlayers[i]);
            }
            GameObject spawnPoint = GameObject.FindGameObjectWithTag(playerSpawnTag);
            playerPrefab = CompanyManager.GetCompany().selectedManager.prefab;
            playerInstance = (GameObject)GameObject.Instantiate(playerPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation, null);
            GameObject.Find("A*").GetComponent<PathFinding>().seeker = playerInstance.transform;
            GameObject.FindObjectOfType<CameraFollowTag>().Init(playerInstance);
            HudManager.Visible(true);
        }

        public override void Update()
        {
            base.Update();
            if (parent.childsDone)
                parent.CompleteTask();
        }

        public override void Destroy()
        {
            base.Destroy();
            HudManager.Visible(false);
            Debug.Log("Free roam completed.");
            GameObject.Destroy(playerInstance);
        }
    }
}