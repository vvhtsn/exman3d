﻿using ExMan.Time;
using System.Collections.Generic;
using UnityEngine;

namespace ExMan.Tasks
{
    public class TaskManager : MonoBehaviour
    {
        public Queue<TaskWrapper> tasks = new Queue<TaskWrapper>();
        public TaskWrapper currentTask;
        public List<string> taskList = new List<string>();
        public bool debug;
        public bool paused
        {
            get; private set;
        }

        public static TaskManager instance;

        private void Awake()
        {
            instance = this;
            currentTask = null;
        }

        /// <summary>
        /// Logs all currently registered tasks to console.
        /// </summary>
        public static void logAllTasks()
        {
            Debug.Log("All registered tasks: ["+instance.tasks.Count+"]");

            foreach (TaskWrapper task in instance.tasks)
            {
                Debug.Log(task.childTasks.Count);
                
                foreach (TaskWrapper childTask in task.childTasks)
                {
                    foreach(TaskWrapper childChildTask in childTask.childTasks)
                    Debug.Log("Task name: " + childChildTask.name);
                }
                    
            }

            /*Debug.Log("Current task name: " + instance.currentTask.currentChild.currentChild.name);
            Debug.Log("All currently registered tasks: [" + instance.currentTask.currentChild.childTasks.Count + "]");

            foreach (TaskWrapper task in instance.currentTask.currentChild.childTasks)
            {
                Debug.Log("Task name: " + task.name);
            }*/
        }

        /// <summary>
        /// Logs all currently registered tasks to console.
        /// </summary>
        public static void currentTasks()
        {
            Debug.Log("Top parent task: "+instance.currentTask.name+", Childs: [" + instance.currentTask.childTasks.Count + "]");

            foreach (TaskWrapper task in instance.currentTask.childTasks)
            {
                Debug.Log("Parent task name: "+task.name+", Childs: ["+task.childTasks.Count+"]");

                foreach (TaskWrapper childTask in task.childTasks)
                {
                    Debug.Log("Task name: " + childTask.name);    
                }

            }
        }

        private void Update()
        {
            if (!paused)
                if (currentTask != null)
                    currentTask.Update();
            if (debug)
            {
                debug = false;
                taskList.Clear();
                foreach (TaskWrapper task in currentTask.childTasks)
                {
                    taskList.Add(task.name);
                }
            }
        }

        public static void AddTask(TaskWrapper task)
        {
            instance.tasks.Enqueue(task);
            if (instance.currentTask == null)
                CompleteTask();
        }

        public static void SetPaused(bool state)
        {
            if (instance != null)
                instance.paused = state;
        }

        public static TaskWrapper GetDayEndTask()
        {
            TaskWrapper doorTrigger = new TaskWrapper(new ActivateTriggerArea(Tags.doorTrigger, 30f, notificationTitle: "Job's done", notificationContent: "You are done for today. You should go home", waypoint: "ExitWaypoint"), false);
            TaskWrapper skipToNextEvent = new TaskWrapper(new SkipToNextEvent(), false);
            TaskWrapper fadeToBlack = new TaskWrapper(new FadeAlpha(1, "Day " + (TimeManager.currentDay + TimeManager.GetDaysTillNextEvent()), true), false);
            TaskWrapper wait = new TaskWrapper(new WaitForSeconds(.2f), false);
            TaskWrapper dayEnd = new TaskWrapper(new ChildrenDone(), true, doorTrigger, fadeToBlack, wait, skipToNextEvent);
            return dayEnd;
        }

        public static void CompleteTask()
        {
            if (instance.currentTask != null)
            {
                instance.currentTask.Destroy();
                instance.currentTask = null;
            }
            if (instance.tasks.Count != 0)
            {
                instance.currentTask = instance.tasks.Dequeue();
                instance.currentTask.StartTask(null);
            }
        }
    }
}