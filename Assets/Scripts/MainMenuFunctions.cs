﻿using ExMan;
using System.IO;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuFunctions : MonoBehaviour
{
    public AudioMixer audioMixer;
    public Slider masterVolumeSlider;
    public Slider musicVolumeSlider;
    public Slider dialogueVolumeSlider;
    public GameObject loadingPanel;
    public InputField companyInputField;
    public GameObject invalidCompanyText;
    public GameObject startNewGameButton;

    private const float minVolVal = -80f;
    private const float maxVolVal = 0f;

    private void Start()
    {
        //InitSlider(masterVolumeSlider, Constants.mixerMasterVolume);
        //InitSlider(musicVolumeSlider, Constants.mixerMusicVolume);
        //InitSlider(dialogueVolumeSlider, Constants.mixerDialogueVolume);
    }

    public void LoadScene(int scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void QuitGame()
    {
        #if UNITY_EDITOR
           UnityEditor.EditorApplication.isPlaying = false;
        #else
           Application.Quit();
        #endif
    }

    public void LoadGame()
    {
        string filename = Application.persistentDataPath + "/" + companyInputField.text + ".dat";
        if (File.Exists(filename))
        {
            PlayerPrefs.SetString("gameToLoad", filename);
            loadingPanel.SetActive(true);
            LoadScene(1);
        }
        else
        {
            invalidCompanyText.SetActive(true);
            startNewGameButton.SetActive(true);
        }
    }

    private void InitSlider(Slider slider, string group)
    {
        slider.onValueChanged.AddListener(val => SetVolume(group, val));

        float outVal;
        audioMixer.GetFloat(group, out outVal);
        slider.minValue = minVolVal;
        slider.maxValue = maxVolVal;
        slider.value = PlayerPrefs.GetFloat(group, outVal);
    }

    public void SetVolume(string group, float val)
    {
        audioMixer.SetFloat(group, val);
        PlayerPrefs.SetFloat(group, val);
    }
}