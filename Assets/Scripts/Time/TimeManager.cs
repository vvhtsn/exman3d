﻿using ExMan.Tasks;
using ExMan.UI;
using ExMan.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan.Time
{
    public class TimeManager : MonoBehaviour
    {
        public static TimeManager instance;

        private int _currentDay = 0;
        public static int currentDay
        {
            get
            {
                return instance._currentDay;
            }
            set
            {
                instance._currentDay = value;
                onDayChanged.Invoke();
            }
        }

        private UnityEvent _onDayChanged = new UnityEvent();
        public static UnityEvent onDayChanged { get { return instance._onDayChanged; } }

        public SortedList<int, TimeEvent> timeEvents = new SortedList<int, TimeEvent>(new DuplicateKeyComparer<int>());
        private Queue<TaskWrapper> backloggedEvents = new Queue<TaskWrapper>();

        private void Awake()
        {
            instance = this;
        }

        private void Start()
        {
            // Company Setup
            if (SaveLoad.instance.loadedGame == null || SaveLoad.instance.loadedGame.scenesCompleted < 1)
            {
                TimeEvent t = new TimeEvent("Game Opening", GameOpening.GetOpeningTask(), false);
                TaskManager.AddTask(t.action);
            }
            // Company Budget
            if (SaveLoad.instance.loadedGame == null || SaveLoad.instance.loadedGame.scenesCompleted < 2)
            {
                TaskWrapper waitOrTrigger = new TaskWrapper(new WaitForSecondsOrTrigger(30, Tags.officeTrigger, 15f, "Office", "You should go to your office"), "OfficeWaypoint", false);

                TaskWrapper trigger = new TaskWrapper(new ActivateTriggerArea(Tags.telephoneTrigger, notificationTitle: "Setup your company", notificationContent: "Go to the laptop in your office to continue setting up your company"), "OfficeWaypoint", false);
                RegisterTimeEvent(1, new TimeEvent("Free Roam", new TaskWrapper(new ChildrenDone(), false, waitOrTrigger, trigger), false));
                RegisterTimeEvent(1, new TimeEvent("Budget Scene", BudgetScene.GetBudgetSceneTask(), false));
            }
            // After Company Budged but before next scene
            if (SaveLoad.instance.loadedGame == null || SaveLoad.instance.loadedGame.scenesCompleted < 3)
            {
                // This delay is applied to both random events and the first order
                TaskWrapper delay = new TaskWrapper(new Tasks.WaitForSeconds(10), false);

                // WaitOrTrigger is disabled to prevent player entering the trigger right away from previous task
                // waitOrTrigger = new TaskWrapper(new WaitForSecondsOrTrigger(30, Tags.officeTrigger, notificationTitle: "Office", notificationContent: "You should go to your office"), false);
                TaskWrapper trigger = new TaskWrapper(new ActivateTriggerArea(Tags.telephoneTrigger, notificationTitle: "Telephone", notificationContent: "Your phone is ringing, go to your desk to pick it up"), "OfficeWaypoint", false);
                TaskWrapper firstOrderScene = new TaskWrapper(new StartScene("FirstOrder"), false);
                TaskWrapper order = new TaskWrapper(new StartOrder(true), false);
                RegisterTimeEvent(1, new TimeEvent("First Order", new TaskWrapper(new ChildrenDone(), false, delay, trigger, firstOrderScene, order), false));
            }

            // Monthly Report
            TaskWrapper summaryTrigger = new TaskWrapper(new ActivateTriggerArea(Tags.telephoneTrigger, notificationTitle: "Pay company wages", notificationContent: "Monthly wages and expenses are not paid. Make sure to pay them at your office."), "OfficeWaypoint", false);
            TaskWrapper monthlyRandomEvent = new TaskWrapper(new EnableUIElement<UI.RandomEvent>(Tags.randomEvent), false);
            TaskWrapper monthlySummary = new TaskWrapper(new EnableUIElement<UI.MonthlySummary>(Tags.monthlySummary), false);
            TaskWrapper monthlyReportTasks = new TaskWrapper(new ChildrenDone(), false, monthlyRandomEvent, summaryTrigger, monthlySummary);

            TimeEvent monthlyReport = new TimeEvent("Monthly Summary", monthlyReportTasks, false);
            RegisterReoccurringEvents(30, 30, 20, monthlyReport);

        }

        /// <summary>
        /// Logs all currently registered events to console.
        /// </summary>
        public static void logAllEvents(){
            Debug.Log("All currently registered events:");
            foreach (KeyValuePair<int, TimeEvent> timeEvent in instance.timeEvents)
            {
                Debug.Log("Event day: "+timeEvent.Key+", name: "+timeEvent.Value.name);
            }
        }

        /// <summary>
        /// Logs current task.
        /// </summary>
        public static void logCurrentTask()
        {
            Debug.Log("Current task name: "+instance.timeEvents[0].action.childTasks.Count);
        }

        /// <summary>
        /// Registers an automaticly triggering event every X starting from Y for Z times.
        /// </summary>
        public static void RegisterReoccurringEvents(int startDay, int everyXDays, int howOften, TimeEvent call)
        {
            for (int i = 0; i < howOften; i++)
            {
                RegisterTimeEvent(startDay + everyXDays * i, (TimeEvent)call.Clone());
            }
        }

        /// <summary>
        /// Registers an automatically triggering event on day X.
        /// </summary>
        /// <param name="day">Day to trigger on</param>
        /// <param name="call">Callback</param>
        public static void RegisterTimeEvent(int day, TimeEvent call)
        {
            if (call != null) { }
                instance.timeEvents.Add(day, call);

            Debug.Log("Registered event day: "+day+", Event name: "+call.name);

            //TaskManager.GetDayEndTask().Update();
        }

        /// <summary>
        /// Returns the amount of days left till the next event.
        /// </summary>
        /// <returns></returns>
        public static int GetDaysTillNextEvent(bool useBlockDayProgression = true)
        {
            if (instance.timeEvents.Count > 0)
            {
                var firstEvent = 0;
                if (useBlockDayProgression)
                {
                    var evnt = instance.timeEvents.FirstOrDefault(x => x.Value.blockDayProgression == true);
                    if (evnt.Key != 0)
                        firstEvent = evnt.Key;
                    else
                        return -1;
                }
                else
                {
                    firstEvent = instance.timeEvents.Keys[0];
                }
                return firstEvent - currentDay;
            }
            return -1;
        }

        /// <summary>
        /// Skip a certain amount of days.
        /// It will stop skipping if there was a day with events unless the <paramref name="allowedToInterrupt"/> was set to true.
        /// </summary>
        /// <param name="amount">Amount of days to skip</param>
        /// <param name="allowedToInterrupt">Should it stop skipping days when there is an interruption. If false, return value will say if there would have been an interruption.</param>
        /// <returns>Returns false if there was no interruption, returns true if there was</returns>
        public static bool SkipDays(int amount, bool allowedToInterrupt = true)
        {
            bool retVal = false;

            for (int i = 0; i < amount; i++)
            {
                bool[] interrupt = NextDay();

                if (interrupt.Any(x => x == true))
                {
                    if (allowedToInterrupt)
                        return true;
                    else
                        retVal = true;
                }
            }

            if (!allowedToInterrupt)
                return retVal;
            else
                return false;
        }

        /// <summary>
        /// Go to the next day.
        /// Any events for the next day will be called.
        /// </summary>
        /// <returns>Returns a list of bools, size is the same as the amount of event, each bool representing if it should block day progression</returns>
        public static bool[] NextDay()
        {
            List<bool> retVal = new List<bool>();
            instance._currentDay++;
            onDayChanged.Invoke();

            List<TaskWrapper> tasks = new List<TaskWrapper>();

            while (instance.timeEvents.Count != 0)
            {
                if (currentDay == instance.timeEvents.Keys[0])
                {
                    if (instance.timeEvents.Values[0].blockDayProgression)
                        tasks.Add(instance.timeEvents.Values[0].action);
                    else
                        instance.backloggedEvents.Enqueue(instance.timeEvents.Values[0].action);

                    retVal.Add(instance.timeEvents.Values[0].blockDayProgression);
                    instance.timeEvents.RemoveAt(0);
                }
                else
                {
                    break;
                }
            }
            if (tasks.Count != 0)
            {
                while (instance.backloggedEvents.Count > 0)
                    tasks.Insert(0, instance.backloggedEvents.Dequeue());
                
                tasks.Insert(0, new TaskWrapper(new FadeAlpha(0, "Day " + currentDay), false));
                tasks.Add(TaskManager.GetDayEndTask());
                TaskWrapper freeRoam = new TaskWrapper(new FreeRoam(), true, tasks);
                TaskManager.AddTask(freeRoam);

                //Log next day's tasks on day change
                //TaskManager.logAllTasks();
            }

            return retVal.ToArray();
        }

        /// <summary>
        /// Returns the index of the given <see cref="TimeEvent"/>.
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static int FindTimeEventIndex(TimeEvent action)
        {
            return instance.timeEvents.IndexOfValue(action);
        }

        /// <summary>
        /// Returns the day of the given <see cref="TimeEvent"/>.
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static int FindTimeEventDay(TimeEvent action)
        {
            return instance.timeEvents.Keys[instance.timeEvents.IndexOfValue(action)];
        }

        /// <summary>
        /// Removes the first available <see cref="TimeEvent"/>.
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static void RemoveTimeEvent(TimeEvent action)
        {
            instance.timeEvents.RemoveAt(instance.timeEvents.IndexOfValue(action));
        }

        /// <summary>
        /// Removes all events on the given day.
        /// </summary>
        /// <param name="day"></param>
        public static void ClearDay(int day)
        {
            int index;
            while ((index = instance.timeEvents.IndexOfKey(day)) != -1)
            {
                instance.timeEvents.RemoveAt(index);
            }
        }

        /// <summary>
        /// Returns all events on the given day.
        /// </summary>
        /// <param name="day"></param>
        /// <returns></returns>
        public static List<TimeEvent> FindEventsOnDay(int day)
        {
            List<TimeEvent> actions = new List<TimeEvent>();
            foreach (var item in instance.timeEvents)
            {
                if (item.Key == day)
                    actions.Add(item.Value);
            }
            return actions;
        }
    }

    public class TimeEvent : ICloneable
    {
        public string name;
        public TaskWrapper action;
        public bool visible;
        public bool blockDayProgression;

        public TimeEvent(string name, TaskWrapper action, bool visible = true, bool blockDayProgression = true)
        {
            this.name = name;
            this.action = action;
            this.visible = visible;
            this.blockDayProgression = blockDayProgression;
        }

        public object Clone()
        {
            TimeEvent t = new TimeEvent(name, (TaskWrapper)action.Clone(), visible, blockDayProgression);
            return t;
        }
    }
}