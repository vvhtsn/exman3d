﻿using DG.Tweening;
using ExMan.Dialogue;
using ExMan.Objects;
using ExMan.ScriptableObjects;
using ExMan.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace ExMan
{
    public class CompanyManager : MonoBehaviour
    {
        private static CompanyManager instance;
        public Company currentCompany = new Company();

        private void Awake()
        {
            instance = this;
            DOTween.Init(true, true, LogBehaviour.Default);
        }

        public static Company GetCompany()
        {
            return instance.currentCompany;
        }

        public static List<Expense> GetMonthlyExpenses()
        {
            List<Expense> expenses = new List<Expense>();
            Company company = GetCompany();
            for (int i = 0; i < company.employees.Count; i++)
            {
                expenses.Add(new Expense("Wage for " + company.employees[i].name, company.employees[i].cost));
            }
            expenses.AddRange(company.staticMonthlyExpenses);
            expenses.Add(new Expense("Wage for manager " + company.selectedManager.name, company.managerSalary));
            expenses.Add(new Expense("Wage for chef " + company.selectedChef.name, company.selectedChef.cost));
            expenses.Add(new Expense("Kitchen costs", company.selectedKitchen.monthlyCost));
            return expenses;
        }

        public static void BuyAd(Ad ad)
        {
            instance.currentCompany.selectedAd = ad;
            SpendMoney(new Expense(ad.name + " Purchased", ad.cost));
        }

        public static void SpendMoney(Expense expense)
        {
            instance.currentCompany.currentMoney -= expense.cost;
            instance.currentCompany.onceOffExpenses.Add(expense);
            if (!string.IsNullOrEmpty(expense.title))
                Logger.Log(string.Format("Spend {0} money on {1}", expense.cost.AsMoney(), expense.title.ToLower()));
        }

        public static void SpendMoney(Expense[] expenses)
        {
            for (int i = 0; i < expenses.Length; i++)
            {
                SpendMoney(expenses[i]);
            }
        }

        public static void SpendMonthlyExpenses()
        {
            List<Expense> expenses = GetMonthlyExpenses();
            for (int i = 0; i < expenses.Count; i++)
            {
                instance.currentCompany.currentMoney -= expenses[i].cost;
                if (!string.IsNullOrEmpty(expenses[i].title))
                    Logger.Log(string.Format("Spend {0} money on {1}", expenses[i].cost.AsMoney(), expenses[i].title.ToLower()));
            }
        }

        public static void ReceiveMoney(float amount)
        {
            instance.currentCompany.currentMoney += amount;
        }

        public static void ReceiveMoney(float amount, string reason)
        {
            instance.currentCompany.currentMoney += amount;
            if (!string.IsNullOrEmpty(reason))
                Logger.Log(string.Format("Received {0} money for {1}", amount.AsMoney(), reason.ToLower()));
        }

        public static void BuyKitchen(Kitchen kitchen)
        {
            instance.currentCompany.selectedKitchen = kitchen;
            SpendMoney(new Expense(kitchen.name, kitchen.cost));

            for (int i = 0; i < kitchen.staff.Length; i++)
            {
                HireCharacter(kitchen.staff[i]);
            }
        }

        public static void BuyVehicle(Vehicle vehicle)
        {
            SpendMoney(new Expense(vehicle.name, vehicle.cost));
            instance.currentCompany.selectedVehicle = vehicle;
        }

        public static void SetManagerSalary(float salary)
        {
            instance.currentCompany.managerSalary = salary;
            Logger.Log(string.Format("Set manager salary to {0}", salary.AsMoney()));
        }

        public static void SetStockReplenishment(StockReplenishment stockReplenishment)
        {
            instance.currentCompany.stockReplenishment = stockReplenishment;
            Logger.Log(string.Format("Selected {0} stock replenishment", stockReplenishment.ToString().ToLower()));
        }

        public static void SetInsurance(Insurance insurance)
        {
            instance.currentCompany.idemnityInsurance = insurance;
            Logger.Log(string.Format("Selected {0} indemnity insurance", insurance.ToString().ToLower()));
        }

        public static void HireManager(Character character)
        {
            instance.currentCompany.selectedManager = character;
            Logger.Log(string.Format("Hired {0}", character.name));
            DialogueManager.characterDefinitions.manager = character;
        }

        public static void HireChef(Chef character)
        {
            instance.currentCompany.selectedChef = character;
            Logger.Log(string.Format("Hired {0}", character.name));
            DialogueManager.characterDefinitions.chef = character;
            Transform parent = GameObject.Find("Characters").transform;
            var chef = Instantiate(character.prefab, parent.position, Quaternion.identity, parent);
            GameObject go = GameObject.Find("Waypoints");
            List<Transform> waypoints = new List<Transform>();
            foreach (Transform waypoint in go.transform)
            {
                waypoints.Add(waypoint);
            }
            chef.GetComponent<RandomWaypointMove>().waypoints = waypoints;
            DialogueManager.characterDefinitions.chef.sceneObject = chef;
        }

        public static void HireCharacter(Character character)
        {
            instance.currentCompany.employees.Add(character);
            Logger.Log(string.Format("Hired {0}", character.name));
        }

        public static void ModifyCustomerSatisfaction(float mod, string reason = "")
        {
            instance.currentCompany.customerSatisfaction += mod;
            if (!string.IsNullOrEmpty(reason))
                Logger.Log(string.Format("Customer statifaction went {0} because of {1}", Mathf.Sign(mod) == 1 ? "up" : "down", reason));
        }

        public static void ModifyStaffSatisfaction(float mod, string reason = "")
        {
            float actualMod = mod;
            if (GetCompany().currentMoney < 0)
            {
                if (mod > 0)
                {
                    actualMod = Mathf.Floor(actualMod / 2);
                }
                else
                {
                    actualMod *= 2;
                }
            }
            instance.currentCompany.staffSatisfaction += actualMod;
            if (!string.IsNullOrEmpty(reason))
                Logger.Log(string.Format("Staff statifaction went {0} because of {1}", Mathf.Sign(actualMod) == 1 ? "up" : "down", reason));
        }
    }
}