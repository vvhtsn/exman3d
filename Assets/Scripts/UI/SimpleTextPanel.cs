﻿using UnityEngine.Events;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class SimpleTextPanel : UIElementTask
    {
        public Button okButton;
        public Text text;
        public Text titleText;

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        public override void StartTask(UnityEvent doneCallback)
        {
            base.StartTask(doneCallback);
            okButton.onClick.RemoveAllListeners();
            okButton.onClick.AddListener(CompleteTask);
        }

        public void SetTitle(string text)
        {
            if (titleText != null)
                titleText.text = text;
        }

        public void SetText(string text)
        {
            if (this.text != null)
                this.text.text = text;
        }

        public void SetOnClick(UnityAction action)
        {
            if (okButton != null)
                okButton.onClick.AddListener(action);
        }
    }
}