﻿using ExMan.ScriptableObjects;
using ExMan.Utils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class VehiclePortrait : MonoBehaviour
{
    public new Text name;
    public Text cost;
    public Text costPerKmText;
    public Image portrait;
    public Button button;
    public GameObject border;

    public void SetVehicle(Vehicle vehicle, UnityAction onClick)
    {
        if (button != null)
            button.onClick.AddListener(onClick);

        name.text = vehicle.name;
        portrait.sprite = vehicle.portrait;
        cost.text = "Initial Cost: " + vehicle.cost.AsMoney();
        costPerKmText.text = "Cost/km: " + vehicle.costPerKm.AsMoney();
    }
}