﻿using ExMan.ScriptableObjects;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class CharacterPortrait : MonoBehaviour
    {
        public new Text name;
        public Image portrait;
        public Button button;
        public GameObject border;

        public void SetCharacter(Character character, UnityAction onClick)
        {
            name.text = character.name;
            portrait.sprite = character.portrait;
            if (button != null)
                button.onClick.AddListener(onClick);
        }
    }
}