﻿using UnityEngine.Events;

namespace ExMan.UI
{
    public class MonthlyIncome : SimpleTextPanel
    {
        public override void StartTask(UnityEvent doneCallback)
        {
            base.StartTask(doneCallback);
            titleText.text = "Monthly Income";
            text.text = "You have completed 2 orders this month, you've earned 100000 rand by completing the orders.";
            CompanyManager.ReceiveMoney(100000, string.Format("Monthly income of {0}", 100000));
        }

        public override void CompleteTask()
        {
            base.CompleteTask();
        }
    }
}