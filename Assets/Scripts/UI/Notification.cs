﻿using DG.Tweening;
using ExMan;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Notification : MonoBehaviour
{
    private static Notification instance;

    public RectTransform mainPanel;
    public Text titleText;
    public Text contentText;
    public Image contentImage;
    public Sprite confirmedBG;
    public Sprite originalBG;

    private Coroutine autoHideCoroutine;
    private Coroutine showCoroutine;

    public static bool isShowing { get { return instance.mainPanel.gameObject.activeInHierarchy; } }

    private void Awake()
    {
        instance = this;

        mainPanel.anchoredPosition = new Vector2(Screen.width, mainPanel.anchoredPosition.y);
        mainPanel.gameObject.SetActive(false);
    }

    public IEnumerator Display(string title, string content, Sprite image = null, bool autoHide = true, float autoHideDelay = 5f)
    {
        yield return StartCoroutine(Display(title, content, false));
    }

    public IEnumerator Display(string title, string content, bool orderCompleted, Sprite image = null, bool autoHide = true, float autoHideDelay = 5f)
    {
        Debug.Log("Showing notification '" + title + "'");
        if (autoHideCoroutine != null)
            StopCoroutine(autoHideCoroutine);

        if (showCoroutine != null)
            StopCoroutine(showCoroutine);

        if (isShowing)
        {
            var tween = mainPanel.DOAnchorPosX(Screen.width, Constants.uiTweenTime)
                .SetId("NotificationTween")
                .SetEase(Constants.uiEaseEndType);
            yield return tween.WaitForCompletion();
        }

        yield return null;

        DOTween.Kill("NotificationTween");
        mainPanel.DOAnchorPosX(0, Constants.uiTweenTime)
            .SetId("NotificationTween")
            .SetEase(Constants.uiEaseStartType);

        mainPanel.gameObject.SetActive(true);
        if (orderCompleted)
        {
            mainPanel.GetComponent<Image>().sprite = confirmedBG;
        }
        titleText.text = title;
        contentText.text = content;

        if (image != null)
        {
            contentText.alignment = TextAnchor.MiddleLeft;
            contentImage.sprite = image;
            contentImage.gameObject.SetActive(true);
        }
        else
        {
            contentText.alignment = TextAnchor.MiddleCenter;
            contentImage.gameObject.SetActive(false);
        }

        if (autoHide)
            autoHideCoroutine = StartCoroutine(AutoHide(autoHideDelay));
    }


    public static void Show(string title, string content,Sprite image = null, bool autoHide = true, float autoHideDelay = 5f, bool orderCompleted = false)
    {
        if (instance.showCoroutine != null)
            instance.StopCoroutine(instance.showCoroutine);

        instance.showCoroutine = instance.StartCoroutine(instance.Display(title, content, orderCompleted, image, autoHide, autoHideDelay));
    }

    public static void Show(string title, string content)
    {
        Show(title, content, null);
    }

    private IEnumerator AutoHide(float delay)
    {
        yield return new WaitForSeconds(delay);
        Hide();
    }

    public void Hide()
    {
        Debug.Log("Hiding notification");

        if (autoHideCoroutine != null)
            StopCoroutine(autoHideCoroutine);

        mainPanel.GetComponent<Image>().sprite = originalBG;
        DOTween.Kill("NotificationTween");
        mainPanel.DOAnchorPosX(Screen.width, Constants.uiTweenTime)
            .SetId("NotificationTween")
            .SetEase(Constants.uiEaseEndType)
            .OnComplete(() => mainPanel.gameObject.SetActive(false));
    }

    public static void Dismiss()
    {
        instance.Hide();
    }
}