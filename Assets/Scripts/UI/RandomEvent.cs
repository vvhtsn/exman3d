﻿using ExMan.Events;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class RandomEvent : UIElementTask
    {
        public Text title;
        public Text description;
        public Button button;

        public List<Events.Event> events = new List<Events.Event>()
        {
            new PetrolPriceHike(), new ThermoOven(), new SalmonellaPoisoning(), new SauceBuyout(), new MenuAdaption(), new MenuNegotiation(), new StoreRoomTheft(), new AirconBroken(), new TooManyOrders(), new DishBuyout(), new StaffRoomDump(), new DishReplacement(), new OrderMixup(), new BurnedCarrots(), new WaitressBoyfriend(), new HighProfile(), new RegionalCompetition(), new ButchersGuildAward(), new LocalFoodCritic(), new SupplyQuality(), new SupermarketContract(), new StaffVolunteering(), new FunctionEntertainment(), new CrayfishAllergy(), new SuperVeggieSlicer(), new SoftwareUpgrade(), new UpsideDownFork(), new PotjieKosCompetition(), new ChefClientArgue(), new UsableTomatoes(), new ConsolidationDiploma(), new OrderCustomization(), new StaffReassignment(), new DoubleShifts()
        };

        public override void StartTask(UnityEvent doneCallback)
        {
            base.StartTask(doneCallback);
            Events.Event e = GetRandomEvent();
            title.text = e.title;
            description.text = e.description;

            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(e.OnComplete);
            button.onClick.AddListener(CompleteTask);

            var newEvents = e.GetUnlockedEvents();
            if (newEvents != null)
                events.AddRange(newEvents);
        }

        public Events.Event GetRandomEvent()
        {
            Events.Event e;
            do
            {
                e = events[Random.Range(0, events.Count)];
            } while (!e.IsAvailable());
            return e;
        }
    }
}