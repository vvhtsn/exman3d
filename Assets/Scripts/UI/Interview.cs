﻿using ExMan.ScriptableObjects;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ExMan.Tasks;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class Interview : MonoBehaviour
    {
        public GameObject hireChefPanel;
        public Transform hireChefParent;
        public EmployAChef task;
        public Transform resumeParent, questionParent;
        public GenericButton genericButtonPrefab;
        public CharacterPortrait characterPortrait;
        public InterviewQuestions questions;
        public int maxInterviewCount = 5, maxQuestionCount = 1;
        public List<Chef> selectedChefs = new List<Chef>();
        private List<int> selectedQuestionIndexes = new List<int>();
        private Chef chefToHire;
        private List<CharacterPortrait> spawnedChefButtons = new List<CharacterPortrait>();

        public GameObject emptyButton;

        private void OnEnable()
        {
            task.SetupResumes(resumeParent, x =>
            {
                bool contains = selectedChefs.Contains(x.chef);
                if (contains)
                {
                    selectedChefs.Remove(x.chef);
                    x.border.SetActive(false);
                }
                else if (selectedChefs.Count < maxInterviewCount)
                {
                    selectedChefs.Add(x.chef);
                    x.border.SetActive(true);
                }
            });

           
        }

        public void SetupQuestions()
        {
            for (int i = 0; i < questions.questions.Count; i++)
            {
                var item = questions.questions[i];
                GameObject go = GameObject.Instantiate(genericButtonPrefab.gameObject);
                go.transform.SetParent(questionParent, false);
                GenericButton button = go.GetComponent<GenericButton>();
                int index = i;
                button.Setup(item.question, () =>
                {
                    bool contains = selectedQuestionIndexes.Contains(index);
                    if (contains)
                    {
                        if (selectedQuestionIndexes.Count > 1)
                        {
                            selectedQuestionIndexes.Remove(index);
                            button.border.SetActive(false);
                        }
                    }
                    else if (selectedQuestionIndexes.Count < maxQuestionCount)
                    {
                        selectedQuestionIndexes.Add(index);
                        button.border.SetActive(true);
                    }
                });
                if (i == 0)
                    button.button.onClick.Invoke();
            }
        }
        public void ShowInterview(int i)
        {
            if (selectedQuestionIndexes.Count >= maxQuestionCount)
            {
                ShowTheInterview(i);
                GameObject.Find("Canvas/CompanyBudgetScene/Employ A Chef/SelectChef/QuestionSelect").SetActive(false);
            }
        }
        public void ShowTheInterview(int i)
        {
            DialogueNodeCanvas d = ScriptableObject.CreateInstance<DialogueNodeCanvas>();
            d.startNodeIndex = 0;
            Dialogue.DialogueManager.characterDefinitions.chef = selectedChefs[i];

            for (int x = 0; x < questions.questions.Count; x++)
            {
                if (!selectedQuestionIndexes.Contains(x))
                    continue;
                var question = questions.questions[x];
                if (!string.IsNullOrEmpty(question.question) && question.answer.Count != 0)
                {
                    DialogueExtensionNode questionNode = new DialogueExtensionNode();

                    questionNode = (DialogueExtensionNode)questionNode.CreateRuntime();
                    questionNode.talkingText = question.question;
                    if (d.nodes.Count > 0)
                        questionNode.Inputs[0].ApplyConnection(d.nodes[d.nodes.Count - 1].Outputs[0]);
                    questionNode.talkingClip = question.questionClip;
                    questionNode.talkingCharacter = CharacterTypes.Manager;
                    questionNode.characterImage = Dialogue.DialogueManager.characterDefinitions.manager.neutralPicture;

                    DialogueExtensionNode answerNode = new DialogueExtensionNode();
                    answerNode = (DialogueExtensionNode)answerNode.CreateRuntime();
                    var answer = question.answer.FirstOrDefault(t => t.chef == selectedChefs[i]);
                    answerNode.talkingText = answer.answer;
                    answerNode.Inputs[0].ApplyConnection(questionNode.Outputs[0]);
                    answerNode.talkingClip = answer.clip;
                    answerNode.talkingCharacter = CharacterTypes.Chef;
                    answerNode.characterImage = Dialogue.DialogueManager.characterDefinitions.chef.neutralPicture;
                    d.nodes.Add(questionNode);
                    d.nodes.Add(answerNode);

                    if (!string.IsNullOrEmpty(question.followUpQuestion) && question.followUpAnswer.Count != 0)
                    {
                        answer = question.followUpAnswer.FirstOrDefault(t => t.chef == selectedChefs[i]);
                        if (!string.IsNullOrEmpty(answer.answer))
                        {
                            DialogueExtensionNode followUpQuestion = new DialogueExtensionNode();
                            followUpQuestion = (DialogueExtensionNode)followUpQuestion.CreateRuntime();
                            followUpQuestion.talkingText = question.followUpQuestion;
                            followUpQuestion.Inputs[0].ApplyConnection(d.nodes[d.nodes.Count - 1].Outputs[0]);
                            followUpQuestion.talkingClip = question.followUpQuestionClip;
                            followUpQuestion.talkingCharacter = CharacterTypes.Manager;
                            followUpQuestion.characterImage = Dialogue.DialogueManager.characterDefinitions.manager.neutralPicture;
                            d.nodes.Add(followUpQuestion);

                            DialogueExtensionNode followUpAnswer = new DialogueExtensionNode();
                            followUpAnswer = (DialogueExtensionNode)followUpAnswer.CreateRuntime();
                            followUpAnswer.talkingText = answer.answer;
                            followUpAnswer.Inputs[0].ApplyConnection(followUpQuestion.Outputs[0]);
                            followUpAnswer.talkingClip = answer.clip;
                            followUpAnswer.talkingCharacter = CharacterTypes.Chef;
                            followUpAnswer.characterImage = Dialogue.DialogueManager.characterDefinitions.chef.neutralPicture;
                            d.nodes.Add(followUpAnswer);
                        }
                    }
                }
            }
            
            DialogueExitNode exit = ScriptableObject.CreateInstance<DialogueExitNode>();
            exit = (DialogueExitNode)exit.CreateRuntime();
            exit.Inputs[0].ApplyConnection(d.nodes[d.nodes.Count - 1].Outputs[0]);
            d.nodes.Add(exit);

            int index = i;

            Dialogue.DialogueManager.StartDialogue(d, "Interview", () =>
            {
                int newIndex = index + 1;
                if (newIndex < selectedChefs.Count)
                    ShowInterview(newIndex);
                else
                {
                    SetupHirePanel();
                }
            });

        }

        public void SetupHirePanel()
        {
            hireChefPanel.SetActive(true);
            for (int i = 0; i < selectedChefs.Count; i++)
            {
                GameObject go = GameObject.Instantiate(characterPortrait.gameObject);
                go.transform.SetParent(hireChefParent, false);
                CharacterPortrait button = go.GetComponent<CharacterPortrait>();
                spawnedChefButtons.Add(button);
                int index = i;
                button.SetCharacter(selectedChefs[index], () =>
                {
                    chefToHire = selectedChefs[index];
                    for (int x = 0; x < spawnedChefButtons.Count; x++)
                    {
                        spawnedChefButtons[x].border.SetActive(selectedChefs.IndexOf(chefToHire) == x);
                    }
                });
                button.border.SetActive(false);
                chefToHire = null;
            }
        }

        public void Complete()
        {
            if (chefToHire != null)
            {
                GameObject.Find("Canvas/CompanyBudgetScene/Employ A Chef").SetActive(false);
               CompanyManager.HireChef(chefToHire);
                task.CompleteTask();
            }
        }

        public void ChooseInterviewType()
        {
           if(selectedChefs.Count > 0)
            {              
                GameObject.Find("Canvas/CompanyBudgetScene/Employ A Chef/InterviewTypeSelection").SetActive(true);
                GameObject.Find("Canvas/CompanyBudgetScene/Employ A Chef/SelectChef/QuestionSelect").SetActive(true);
                GameObject.Find("Canvas/CompanyBudgetScene/Employ A Chef/SelectChef/ResumeSelect").SetActive(false);
            }
        }

        public void Reset()
        {
            string selectAChef = "Canvas/CompanyBudgetScene/Employ A Chef/SelectChef/";
            GameObject resumeNextButton = GameObject.Find(selectAChef +"ResumeSelect/Next");
            var newButton = Instantiate(emptyButton, resumeNextButton.transform.position, resumeNextButton.transform.rotation, resumeNextButton.transform.parent);
            newButton.name = "Next";
            Destroy(resumeNextButton);
            newButton.GetComponent<Button>().onClick.AddListener( () => 
            {
                GameObject.Find(selectAChef + "HireChef").SetActive(true);
                GameObject.Find(selectAChef + "ResumeSelect").SetActive(false);
            } );
        }
    }
}