﻿using ExMan.Dialogue;
using ExMan.ScriptableObjects;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class AlternativesOrConflict : UIElementTask
    {
        public Button conflictButton, alternativesButton;
        public string conflictDialogue, alternativesDialogue, clientAlternatives;

        public override void StartTask(UnityEvent doneCallback)
        {
            int dialogsCompleted = 0;
            base.StartTask(doneCallback);
            conflictButton.onClick.AddListener(() =>
            {
                gameObject.SetActive(false);
                DialogueNodeCanvas dialogueCanvas = Resources.Load<DialogueNodeCanvas>("Dialogue/" + conflictDialogue.Replace("Chef", CompanyManager.GetCompany().selectedChef.name));
                if (dialogueCanvas != null)
                {
                    DialogueManager.StartDialogue(dialogueCanvas, conflictDialogue.Replace("Chef", CompanyManager.GetCompany().selectedChef.name), () =>
                    {
                        dialogsCompleted++;
                        if (dialogsCompleted >= 2)
                        {
                            DialogueManager.characterDefinitions.chef.sceneObject.GetComponent<RandomWaypointMove>().StartWandering();
                            CompleteTask();
                        }
                        else
                        {
                            gameObject.SetActive(true);
                        }
                    }, new GameObject[] { DialogueManager.characterDefinitions.chef.sceneObject }, new Vector3(-5, 0, -0.5f), 0);
                }
                else
                    Debug.LogError("Can't load " + conflictDialogue.Replace("Chef", CompanyManager.GetCompany().selectedChef.name));
            });

            alternativesButton.onClick.AddListener(() =>
            {
                gameObject.SetActive(false);
                DialogueNodeCanvas dialogueCanvas = Resources.Load<DialogueNodeCanvas>("Dialogue/" + alternativesDialogue.Replace("Chef", CompanyManager.GetCompany().selectedChef.name));
                if (dialogueCanvas != null)
                {
                    DialogueManager.StartDialogue(dialogueCanvas, alternativesDialogue.Replace("Chef", CompanyManager.GetCompany().selectedChef.name), () =>
                    {
                        dialogueCanvas = Resources.Load<DialogueNodeCanvas>("Dialogue/" + clientAlternatives.Replace("Chef", CompanyManager.GetCompany().selectedChef.name));
                        if (dialogueCanvas != null)
                        {
                            DialogueManager.StartDialogue(dialogueCanvas, clientAlternatives.Replace("Chef", CompanyManager.GetCompany().selectedChef.name), () =>
                            {
                                dialogsCompleted++;
                                if (dialogsCompleted >= 2)
                                {
                                    DialogueManager.characterDefinitions.chef.sceneObject.GetComponent<RandomWaypointMove>().StartWandering();
                                    CompleteTask();
                                }
                                else
                                {
                                    gameObject.SetActive(true);
                                }
                            });
                        }
                        else
                            Debug.LogError("Can't load " + alternativesDialogue.Replace("Chef", CompanyManager.GetCompany().selectedChef.name));
                    }, new GameObject[] { DialogueManager.characterDefinitions.chef.sceneObject }, new Vector3(-5, 0, -0.5f), 0);
                }
                else
                    Debug.LogError("Can't load " + conflictDialogue.Replace("Chef", CompanyManager.GetCompany().selectedChef.name));
            });
        }
    }
}