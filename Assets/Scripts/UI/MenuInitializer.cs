﻿using UnityEngine.Events;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class MenuInitializer : GenericButton
    {
        public Text content;

        public void SetMenu(MenuPresenting.Menu menu, UnityAction onClick)
        {
            if (this.text != null)
                this.text.text = menu.name;
            if (this.content != null)
                this.content.text = menu.content;
            if (this.button != null)
                button.onClick.AddListener(onClick);
        }
    }
}