﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class Fade : MonoBehaviour
    {
        public CanvasGroup group;
        public Text text;
        private static Fade instance;

        private void Awake()
        {
            instance = this;
            group.alpha = 0;
        }

        public static void FadeAlpha(float target, float length, UnityAction onComplete, string text = "")
        {
            //instance.text.text = text;
            instance.group.DOFade(target, length).OnComplete(() =>
            {
                if (onComplete != null)
                    onComplete.Invoke();
            });
        }
    }
}