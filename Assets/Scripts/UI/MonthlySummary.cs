﻿using ExMan.Events;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class MonthlySummary : UIElementTask
    {
        public Text title;
        public Text description;
        public Button button;

        public override void StartTask(UnityEvent doneCallback)
        {
            base.StartTask(doneCallback);
            CompanyManager.SpendMonthlyExpenses();
            description.text = "";
            title.text = "Your monthly summary:";
            var monthlyExpenses = CompanyManager.GetMonthlyExpenses();
            for (int i = 0; i < monthlyExpenses.Count; i++)
            {
                description.text += monthlyExpenses[i].title + "\r\n";
            }

            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(CompleteTask);
        }
    }
}