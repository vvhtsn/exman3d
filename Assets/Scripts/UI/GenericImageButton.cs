﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GenericImageButton : MonoBehaviour
{
    public Text text;
    public Button button;
    public Image image;
    public GameObject border;

    public void Setup(string text, UnityAction onClick)
    {
        if (this.text != null)
            this.text.text = text;
        if (button != null)
            button.onClick.AddListener(onClick);
    }

    public void Setup(string text, Sprite image, UnityAction onClick)
    {
        if (this.text != null)
            this.text.text = text;
        if (button != null)
            button.onClick.AddListener(onClick);
        if (this.image != null)
            this.image.sprite = image;
    }
}