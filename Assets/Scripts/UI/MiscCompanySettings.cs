﻿using ExMan.Objects;
using ExMan.Utils;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan.UI
{
    public class MiscCompanySettings : UIElementTask
    {
        public GenericImageButton buttonPrefab;

        public GameObject salaryParent, insuranceParent, stockParent;

        public float[] availableManagerSalaries = new float[3] { 0, 10, 100 };
        public Sprite[] salaryImages, insuranceImages, stockImages;

        private Insurance[] availableInsurances;
        private StockReplenishment[] availableStockReplenisments;
        private int selectedSalary, selectedInsurance, selectedStockReplenishment;

        private GenericImageButton[] salaryButtons, insuranceButtons, stockButtons;

        private void OnEnable()
        {
            Reset();

            availableInsurances = (Insurance[])System.Enum.GetValues(typeof(Insurance));
            availableStockReplenisments = (StockReplenishment[])System.Enum.GetValues(typeof(StockReplenishment));
            salaryButtons = new GenericImageButton[availableManagerSalaries.Length];
            insuranceButtons = new GenericImageButton[availableInsurances.Length];
            stockButtons = new GenericImageButton[availableStockReplenisments.Length];

            if (salaryImages == null)
                salaryImages = new Sprite[availableManagerSalaries.Length];
            if (insuranceImages == null)
                insuranceImages = new Sprite[availableInsurances.Length];
            if (stockImages == null)
                stockImages = new Sprite[availableStockReplenisments.Length];

            SetupSalaries();
            SetupInsurances();
            SetupStockReplenishments();
        }

        public override void StartTask(UnityEvent doneCallback)
        {
            base.StartTask(doneCallback);
            doneCallback.AddListener(() =>
            {
                CompanyManager.SetManagerSalary(availableManagerSalaries[selectedSalary]);
                CompanyManager.SetInsurance(availableInsurances[selectedInsurance]);
                CompanyManager.SetStockReplenishment(availableStockReplenisments[selectedStockReplenishment]);
            });
        }

        private void Reset()
        {
            ClearChildren();
            selectedInsurance = 0;
            selectedSalary = 0;
            selectedStockReplenishment = 0;
        }

        private void ClearChildren()
        {
            if (salaryButtons != null)
                for (int i = 0; i < salaryButtons.Length; i++)
                    GameObject.Destroy(salaryButtons[i].gameObject);
            if (insuranceButtons != null)
                for (int i = 0; i < insuranceButtons.Length; i++)
                    GameObject.Destroy(insuranceButtons[i].gameObject);
            if (stockButtons != null)
                for (int i = 0; i < stockButtons.Length; i++)
                    GameObject.Destroy(stockButtons[i].gameObject);
        }

        private void SetupStockReplenishments()
        {
            for (int i = 0; i < availableStockReplenisments.Length; i++)
            {
                int index = i;
                GameObject go = Instantiate(buttonPrefab.gameObject);
                go.transform.SetParent(stockParent.transform, false);
                GenericImageButton button = go.GetComponent<GenericImageButton>();
                stockButtons[i] = button;
                button.Setup(((int)availableStockReplenisments[i]).AsMoney(), stockImages[i], () =>
                {
                    selectedStockReplenishment = index;
                    for (int x = 0; x < stockButtons.Length; x++)
                        stockButtons[x].border.SetActive(x == selectedStockReplenishment);
                });
                stockButtons[i].border.SetActive(i == selectedStockReplenishment);
            }
        }

        private void SetupInsurances()
        {
            for (int i = 0; i < availableInsurances.Length; i++)
            {
                int index = i;
                GameObject go = Instantiate(buttonPrefab.gameObject);
                go.transform.SetParent(insuranceParent.transform, false);
                GenericImageButton button = go.GetComponent<GenericImageButton>();
                insuranceButtons[i] = button;
                button.Setup(availableInsurances[i].ToString(), insuranceImages[i], () =>
                {
                    selectedInsurance = index;
                    for (int x = 0; x < insuranceButtons.Length; x++)
                        insuranceButtons[x].border.SetActive(x == selectedInsurance);
                });
                insuranceButtons[i].border.SetActive(i == selectedInsurance);
            }
        }

        private void SetupSalaries()
        {
            for (int i = 0; i < availableManagerSalaries.Length; i++)
            {
                int index = i;
                GameObject go = Instantiate(buttonPrefab.gameObject);
                go.transform.SetParent(salaryParent.transform, false);
                GenericImageButton button = go.GetComponent<GenericImageButton>();
                salaryButtons[i] = button;
                button.Setup(((int)availableManagerSalaries[i]).AsMoney(), salaryImages[i], () =>
                {
                    selectedSalary = index;
                    for (int x = 0; x < salaryButtons.Length; x++)
                        salaryButtons[x].border.SetActive(x == selectedSalary);
                });
                salaryButtons[i].border.SetActive(i == selectedSalary);
            }
        }
    }
}