﻿using ExMan.Dialogue;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan.UI
{
    public class MenuPresenting : UIElementTask
    {
        public MenuInitializer menuPrefab;
        public GameObject menuParent;
        public Menu coldMenu;
        public Menu originalMenu;

        public string sentSolutionDialogue, chefAlternativesDialogue;
        public GameObject unhappyResponse;
        public MenuSelection selectionsResponse;
        public float originalMenuCost;
        public float forceClientSatisfactionPenalty;

        private bool starter, mainMeat, mainSide, dessert;
        private bool ended;

        private Menu selectedMenu;
        private List<MenuInitializer> spawnedMenus = new List<MenuInitializer>();

        [Serializable]
        public struct Menu
        {
            public string name;

            [TextArea]
            public string content;
        }

        private void Awake()
        {
            SetupMenu(originalMenu);
            SetupMenu(coldMenu);
            DialogueManager.RegisterEvent("PowerOutageBuyEntireMenu", BuyOriginalMenu);
            DialogueManager.RegisterEvent("PowerOutageCancelOrder", CancelOrder);

            DialogueManager.RegisterEvent("PowerOutageStarterSuccess", StarterSuccess);

            DialogueManager.RegisterEvent("PowerOutageMainMeatSuccess", MainMeatSuccess);

            DialogueManager.RegisterEvent("PowerOutageMainSideSuccess", MainSideSuccess);

            DialogueManager.RegisterEvent("PowerOutageDessertSuccess", DessertSuccess);
        }

        private void StarterSuccess()
        {
            starter = true;
        }

        private void MainMeatSuccess()
        {
            mainMeat = true;
        }

        private void MainSideSuccess()
        {
            mainSide = true;
        }

        private void DessertSuccess()
        {
            dessert = true;
        }

        private void SetupMenu(Menu menu)
        {
            GameObject go = Instantiate(menuPrefab.gameObject);
            go.transform.SetParent(menuParent.transform, false);
            MenuInitializer menuObject = go.GetComponent<MenuInitializer>();
            menuObject.SetMenu(menu, () =>
            {
                selectedMenu = menu;
                for (int i = 0; i < spawnedMenus.Count; i++)
                {
                    spawnedMenus[i].border.SetActive(false);
                }
                menuObject.border.SetActive(true);
            });
            spawnedMenus.Add(menuObject);
        }

        public void Continue()
        {
            if (selectedMenu.name == coldMenu.name)
            {
                DialogueNodeCanvas dialogueCanvas = Resources.Load<DialogueNodeCanvas>("Dialogue/" + sentSolutionDialogue);
                DialogueManager.StartDialogue(dialogueCanvas, sentSolutionDialogue, () =>
                {
                    unhappyResponse.SetActive(true);
                });
            }
            else
            {
                DialogueNodeCanvas dialogueCanvas = Resources.Load<DialogueNodeCanvas>("Dialogue/" + chefAlternativesDialogue.Replace("Chef", CompanyManager.GetCompany().selectedChef.name));
                DialogueManager.StartDialogue(dialogueCanvas, chefAlternativesDialogue.Replace("Chef", CompanyManager.GetCompany().selectedChef.name), () =>
                {
                    dialogueCanvas = Resources.Load<DialogueNodeCanvas>("Dialogue/" + sentSolutionDialogue);
                    DialogueManager.StartDialogue(dialogueCanvas, sentSolutionDialogue, () =>
                    {
                        if (ended == false)
                        {
                            selectionsResponse.gameObject.SetActive(true);
                            selectionsResponse.Init(starter, mainMeat, mainSide, dessert);
                        }
                    });
                });
            }
        }

        public void CancelOrder()
        {
            CompleteTask();
            GameOver.EndGame();
        }

        public void BuyOriginalMenu()
        {
            CompanyManager.SpendMoney(new Objects.Expense("Buy entire original menu 4", originalMenuCost));
            CompleteTask();
        }

        public void ForceYourSelections()
        {
            CompanyManager.ModifyCustomerSatisfaction(forceClientSatisfactionPenalty * (Convert.ToInt32(starter) + Convert.ToInt32(mainMeat) + Convert.ToInt32(mainSide) + Convert.ToInt32(dessert)));
            CompleteTask();
        }

        public override void CompleteTask()
        {
            base.CompleteTask();
            ended = true;
            unhappyResponse.gameObject.SetActive(false);
            gameObject.SetActive(false);
        }

        public override void StartTask(UnityEvent doneCallback)
        {
            base.StartTask(doneCallback);
        }
    }
}