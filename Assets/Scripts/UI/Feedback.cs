﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ExMan;

public class Feedback : MonoBehaviour
{
    public Text questionText;
    public Text answerText;
    public Text effectText;

    [HideInInspector]
    public string otherCharacter;

    [HideInInspector]
    public string Question
    {
        set
        {
            questionText.text = otherCharacter + " said: " + value;
        }
        get
        {
            return questionText.text;
        }
    }

    [HideInInspector]
    public string Answer
    {
        set
        {
            answerText.text = "You said: " + value;
        }
        get
        {
            return answerText.text;
        }
    }

    [HideInInspector]
    public string Effect
    {
        set
        {
            effectText.text = value;
        }
        get
        {
            return effectText.text;
        }
    }

    public override string ToString()
    {
        return Question + Environment.NewLine + Environment.NewLine + Answer + Environment.NewLine + Environment.NewLine + Effect;
    }
}
