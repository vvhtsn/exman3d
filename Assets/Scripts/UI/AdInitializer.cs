﻿using ExMan.ScriptableObjects;
using ExMan.Utils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class AdInitializer : MonoBehaviour
{
    public Button button;
    public Image adThumbnail;
    public Text cost;
    public Text adName;
    public GameObject border;

    public void SetAd(Ad ad, UnityAction onClick)
    {
        if (button != null)
            button.onClick.AddListener(onClick);

        cost.text = "Cost: " + ad.cost.AsMoney();
        adName.text = ad.name;
        adThumbnail.sprite = ad.thumbnail;
    }
}