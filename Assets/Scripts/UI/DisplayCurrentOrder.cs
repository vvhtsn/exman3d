﻿using ExMan.Objects;
using UnityEngine;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class DisplayCurrentOrder : MonoBehaviour
    {
        public Text orderInfo;
        public Text orderTitle;

        private void Start()
        {
            gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            Order curr = OrderQueue.GetCurrentOrder();
            if (curr != null)
            {
                orderTitle.text = "Current Order";
                orderInfo.text = curr.ToString();
            }
            else
            {
                orderTitle.text = "Current Order";
                orderInfo.text = "Current Order not available.";
            }
        }
    }
}