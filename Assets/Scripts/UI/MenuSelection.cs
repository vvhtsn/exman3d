﻿using UnityEngine;
using UnityEngine.UI;

public class MenuSelection : MonoBehaviour
{
    public Toggle starter, mainMeat, mainSide, dessert;
    public Button[] buttons;
    public Button continueButton;

    public void Init(bool starter, bool mainMeat, bool mainSide, bool dessert)
    {
        this.starter.isOn = starter;
        this.mainMeat.isOn = mainMeat;
        this.mainSide.isOn = mainSide;
        this.dessert.isOn = dessert;
        if (starter && mainSide && mainMeat && dessert)
        {
            for (int i = 0; i < buttons.Length; i++)
            {
                buttons[i].gameObject.SetActive(false);
            }
            continueButton.gameObject.SetActive(true);
        }
    }
}