﻿using ExMan.ScriptableObjects;
using ExMan.Utils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class KitchenPortrait : MonoBehaviour
{
    public new Text name;
    public Text cost;
    public Text monthlyCostText;
    public Image portrait;
    public Button button;
    public GameObject border;

    public void SetKitchen(Kitchen kitchen, UnityAction onClick)
    {
        if (button != null)
            button.onClick.AddListener(onClick);

        name.text = kitchen.name;
        portrait.sprite = kitchen.portrait;
        cost.text = "Initial Cost: " + kitchen.cost.AsMoney();
        float monthlyCost = kitchen.monthlyCost;
        for (int i = 0; i < kitchen.staff.Length; i++)
        {
            monthlyCost += kitchen.staff[i].cost;
        }
        monthlyCostText.text = "Monthly Cost: " + monthlyCost.AsMoney();
    }
}