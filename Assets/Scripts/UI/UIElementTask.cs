﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan.UI
{
    public abstract class UIElementTask : MonoBehaviour
    {
        protected UnityEvent doneCallback;

        public virtual void StartTask(UnityEvent doneCallback)
        {
            this.doneCallback = doneCallback;
        }

        protected virtual void Update()
        {
            if (Application.isEditor)
                if (Input.GetKeyDown(KeyCode.F10))
                {
                    CompleteTask();
                    DOTween.Complete("UIPanelTween");
                }
        }

        public virtual void CompleteTask()
        {
            doneCallback.Invoke();
        }
    }
}