﻿using ExMan.ScriptableObjects;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan.UI
{
    public class ManagerSelect : UIElementTask
    {
        public GameObject managerParent;
        public GameObject portraitPrefab;
        public Character[] availableManagers;
        private int selectedIndex = 0;
        private CharacterPortrait[] spawnedPortraits;

        public void Awake()
        {
            Reset();
            spawnedPortraits = new CharacterPortrait[availableManagers.Length];
            SetupManagers();
        }

        public override void StartTask(UnityEvent doneCallback)
        {
            base.StartTask(doneCallback);
            doneCallback.AddListener(() =>
           {
               CompanyManager.HireManager(availableManagers[selectedIndex]);
           });
        }

        private void Reset()
        {
            ClearChildren();
            selectedIndex = 0;
        }

        private void ClearChildren()
        {
            if (spawnedPortraits != null)
                for (int i = 0; i < spawnedPortraits.Length; i++)
                    GameObject.Destroy(spawnedPortraits[i].gameObject);
        }

        private void SetupManagers()
        {
            for (int i = 0; i < availableManagers.Length; i++)
            {
                int index = i;
                GameObject go = Instantiate(portraitPrefab);
                go.transform.SetParent(managerParent.transform, false);
                CharacterPortrait portrait = go.GetComponent<CharacterPortrait>();

                portrait.SetCharacter(availableManagers[i], () =>
                {
                    selectedIndex = index;
                    for (int x = 0; x < spawnedPortraits.Length; x++)
                    {
                        spawnedPortraits[x].border.SetActive(x == selectedIndex);
                    }
                });
                spawnedPortraits[i] = portrait;
                spawnedPortraits[i].border.SetActive(i == selectedIndex);
            }
        }
    }
}