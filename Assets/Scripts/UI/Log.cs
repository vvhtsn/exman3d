﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class Log : MonoBehaviour
    {
        public Text text;
        public ScrollRect scrollView;

        private void Start()
        {
            Logger.onLogChanged.AddListener(OnLogChanged);
            gameObject.SetActive(false);
        }

        private void OnEnable()
        {
            UpdateUI();
            scrollView.verticalNormalizedPosition = 0;
        }

        private void OnLogChanged()
        {
            if (gameObject.activeSelf)
            {
                UpdateUI();
                scrollView.DOVerticalNormalizedPos(0, .1f);
            }
        }

        private void UpdateUI()
        {
            text.text = Logger.ToString();
        }
    }
}