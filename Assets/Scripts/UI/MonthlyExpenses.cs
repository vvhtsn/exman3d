﻿using UnityEngine.Events;

namespace ExMan.UI
{
    public class MonthlyExpenses : SimpleTextPanel
    {
        public override void StartTask(UnityEvent doneCallback)
        {
            base.StartTask(doneCallback);
            CompanyManager.SpendMonthlyExpenses();
            text.text = "";
            titleText.text = "Your monthly costs have been deducted.";
            var monthlyExpenses = CompanyManager.GetMonthlyExpenses();
            for (int i = 0; i < monthlyExpenses.Count; i++)
            {
                text.text += monthlyExpenses[i].title + "\r\n";
            }
        }
    }
}