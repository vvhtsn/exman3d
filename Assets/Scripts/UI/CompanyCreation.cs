﻿using DG.Tweening;
using ExMan.ScriptableObjects;
using ExMan.Utils;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class CompanyCreation : UIElementTask
    {
        public RectTransform selectionPanel, repaymentTermsPanel;
        public GameObject kitchenParent, vehicleParent;
        public InputField companyNameInputField;

        public Kitchen[] availableKitchens;
        public GameObject[] availableKitchenObjects;
        public Vehicle[] availableVehicles;
        public GameObject[] availableVehicleObjects;

        public int repaymentPeriod = 5;
        public float loanInterest = 10.5f;
        public float startupCapital = 15000;

        public Text kitchenName, kitchenCost, vehicleName, vehicleCost, startupCapitalText, totalAmount, loanPeriod, interest, monthlyInstallment;

        public GameObject kitchenPrefab, vehiclePrefab;

        private int selectedKitchenIndex, selectedVehicleIndex;
        private KitchenPortrait[] spawnedKitchens;
        private VehiclePortrait[] spawnedVehicles;

        public void OnEnable()
        {
            Reset();

            spawnedKitchens = new KitchenPortrait[availableKitchens.Length];
            spawnedVehicles = new VehiclePortrait[availableVehicles.Length];

            SetupKitchens();
            SetupVehicles();
        }

        public override void StartTask(UnityEvent doneCallback)
        {
            base.StartTask(doneCallback);
            doneCallback.AddListener(() =>
            {
                CompanyManager.ReceiveMoney(availableKitchens[selectedKitchenIndex].cost + availableVehicles[selectedVehicleIndex].cost + startupCapital, string.Format("Loan for {1} and {2}", availableKitchens[selectedKitchenIndex].cost + availableVehicles[selectedVehicleIndex].cost + 10000, availableKitchens[selectedKitchenIndex].name, availableVehicles[selectedVehicleIndex].name));
                CompanyManager.BuyKitchen(availableKitchens[selectedKitchenIndex]);
                CompanyManager.BuyVehicle(availableVehicles[selectedVehicleIndex]);
                CompanyManager.GetCompany().name = companyNameInputField.text;
                enabled = false;
            });

            Vector3 pos = repaymentTermsPanel.anchoredPosition;
            pos.x = -Screen.width;
            repaymentTermsPanel.anchoredPosition = pos;
        }

        public void UpdateTerms()
        {
            if (selectedKitchenIndex >=0 && selectedVehicleIndex >= 0 && companyNameInputField.text != "")
            {
                UpdateRepaymentTerms();
                TweenToRepaymentTerms();
            }
        }

        public void UpdateRepaymentTerms()
        {
            var kCost = availableKitchens[selectedKitchenIndex].cost;
            var vCost = availableVehicles[selectedVehicleIndex].cost;
            kitchenName.text = availableKitchens[selectedKitchenIndex].name;
            vehicleName.text= availableVehicles[selectedVehicleIndex].name;
            kitchenCost.text = kCost.AsMoney();
            vehicleCost.text = vCost.AsMoney();
            startupCapitalText.text = startupCapital.AsMoney();
            totalAmount.text = (kCost + vCost).AsMoney();

            loanPeriod.text = repaymentPeriod + " Years";
            interest.text = loanInterest + "%";

            monthlyInstallment.text = (((kCost + vCost + startupCapital) * Mathf.Pow(1f + (loanInterest / 12f), repaymentPeriod * 12)) / (repaymentPeriod * 12)).AsMoney();
        }

        public void TweenToRepaymentTerms()
        {
            repaymentTermsPanel.gameObject.SetActive(true);
            repaymentTermsPanel.DOAnchorPosX(0, Constants.uiTweenTime).SetEase(Constants.uiEaseStartType);
            selectionPanel.DOAnchorPosX(Screen.width, Constants.uiTweenTime).SetEase(Constants.uiEaseEndType).OnComplete(()=>selectionPanel.gameObject.SetActive(false));
        }

        public void TweenToSelectionPanel()
        {
            selectionPanel.gameObject.SetActive(true);
            repaymentTermsPanel.DOAnchorPosX(-Screen.width, Constants.uiTweenTime).SetEase(Constants.uiEaseStartType).OnComplete(() => repaymentTermsPanel.gameObject.SetActive(false));
            selectionPanel.DOAnchorPosX(0, Constants.uiTweenTime).SetEase(Constants.uiEaseEndType);
        }

        private void Reset()
        {
            ClearChildren();
            selectedKitchenIndex = 0;
            selectedVehicleIndex = 0;
        }

        private void ClearChildren()
        {
            if (spawnedKitchens != null)
                for (int i = 0; i < spawnedKitchens.Length; i++)
                    GameObject.Destroy(spawnedKitchens[i].gameObject);
            if (spawnedVehicles != null)
                for (int i = 0; i < spawnedVehicles.Length; i++)
                    GameObject.Destroy(spawnedVehicles[i].gameObject);
        }

        private void SetupVehicles()
        {
            for (int i = 0; i < availableVehicles.Length; i++)
            {
                int index = i;
                GameObject go = Instantiate(vehiclePrefab);
                go.transform.SetParent(vehicleParent.transform, false);
                VehiclePortrait vehicle = go.GetComponent<VehiclePortrait>();
                vehicle.SetVehicle(availableVehicles[i], () =>
                {
                    selectedVehicleIndex = index;
                   
                    for (int x = 0; x < spawnedVehicles.Length; x++)
                    {
                        spawnedVehicles[x].border.SetActive(x == selectedVehicleIndex);
                        if (availableVehicleObjects[x] != null)
                        {
                            availableVehicleObjects[x].SetActive(x == selectedVehicleIndex);
                        }
                    }
                });
                spawnedVehicles[i] = vehicle;
                spawnedVehicles[i].border.SetActive(false);
            }
            selectedVehicleIndex = -1;
        }

        private void SetupKitchens()
        {
            for (int i = 0; i < availableKitchens.Length; i++)
            {
                int index = i;
                GameObject go = Instantiate(kitchenPrefab);
                go.transform.SetParent(kitchenParent.transform, false);
                KitchenPortrait kitchen = go.GetComponent<KitchenPortrait>();
                kitchen.SetKitchen(availableKitchens[i], () =>
                {
                    selectedKitchenIndex = index;
                    for (int x = 0; x < spawnedKitchens.Length; x++)
                    {
                        spawnedKitchens[x].border.SetActive(x == selectedKitchenIndex);
                        if (availableKitchenObjects[x] != null)
                        {
                            availableKitchenObjects[x].SetActive(x <= selectedKitchenIndex);
                        }
                    }
                });
                spawnedKitchens[i] = kitchen;
                spawnedKitchens[i].border.SetActive(false);
            }
            selectedKitchenIndex = -1;
        }
    }
}