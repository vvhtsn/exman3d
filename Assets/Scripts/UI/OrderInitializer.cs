﻿using DG.Tweening;
using ExMan.Objects;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class OrderInitializer : UIElementTask
    {
        private bool _isFirstOrder;

        public bool isFirstOrder
        {
            get
            {
                return _isFirstOrder;
            }
            set
            {
                _isFirstOrder = value;
                decline.gameObject.SetActive(!_isFirstOrder);
            }
        }

        public Button accept;
        public Button decline;
        public Text orderInfo;
        public Text orderTitle;

        public void SetOrder(Order order, UnityAction acceptAction, UnityAction declineAction)
        {
            orderTitle.text = "A new order has arrived!";
            orderInfo.text = order.ToStringStart();

            RectTransform rt = (RectTransform)transform;

            accept.onClick.RemoveAllListeners();
            decline.onClick.RemoveAllListeners();

            accept.onClick.AddListener(CompleteTask);
            accept.onClick.AddListener(acceptAction);
            decline.onClick.AddListener(CompleteTask);
            decline.onClick.AddListener(declineAction);

            accept.onClick.AddListener(() => rt.DOAnchorPosX(Screen.width, Constants.uiTweenTime).SetId("OrderTween").SetEase(Constants.uiEaseEndType));
            accept.onClick.AddListener(() => enabled = false);
            decline.onClick.AddListener(() => rt.DOAnchorPosX(Screen.width, Constants.uiTweenTime).SetId("OrderTween").SetEase(Constants.uiEaseEndType));
            decline.onClick.AddListener(() => enabled = false);

            Vector3 pos = rt.anchoredPosition;
            pos.x = -Screen.width;
            rt.anchoredPosition = pos;
            rt.DOAnchorPosX(0, Constants.uiTweenTime).SetEase(Constants.uiEaseStartType);
        }

        protected override void Update()
        {
            if (Application.isEditor)
                if (Input.GetKeyDown(KeyCode.F10))
                {
                    DOTween.Complete("OrderTween");
                    DOTween.Complete("UIPanelTween");
                    accept.onClick.Invoke();
                }
        }

        public void Reset()
        {
            isFirstOrder = false;
            accept.onClick.RemoveAllListeners();
            decline.onClick.RemoveAllListeners();
            orderInfo.text = "";
        }
    }
}