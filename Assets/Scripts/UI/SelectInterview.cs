﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ExMan.ScriptableObjects;

namespace ExMan.UI
{
    public class SelectInterview : MonoBehaviour {
        public GameObject SelectChefMenu;
        public InterviewQuestions structured;
        public InterviewQuestions semiStructured;

        public void Structured()
        {
            SelectChefMenu.GetComponent<Interview>().questions = structured;
            SelectChefMenu.GetComponent<Interview>().SetupQuestions();
            SelectChefMenu.SetActive(true);
        }

        public void SemiStructured()
        {
            SelectChefMenu.GetComponent<Interview>().questions = semiStructured;
            SelectChefMenu.GetComponent<Interview>().SetupQuestions();
            SelectChefMenu.SetActive(true);
        }
    }
}