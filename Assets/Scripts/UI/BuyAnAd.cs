﻿using ExMan.ScriptableObjects;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan.UI
{
    public class BuyAnAd : UIElementTask
    {
        public Ad[] availableAds;
        public GameObject adParent;
        public GameObject adPrefab;
        private Vector3 initPos;
        private int selectedAdIndex;
        private AdInitializer[] spawnedAds;
        bool initialised;
        private void OnEnable()
        {
            if (initialised == false)
            {
                initialised = true;
                initPos = GetComponent<RectTransform>().position;
            }
            Reset();
            
            spawnedAds = new AdInitializer[availableAds.Length];

            SetupAds();
        }

        public override void StartTask(UnityEvent doneCallback)
        {
            base.StartTask(doneCallback);

            doneCallback.AddListener(() =>
            {
                CompanyManager.BuyAd(availableAds[selectedAdIndex]);
            });
        }

        public void Reset()
        {
            ClearChildren();
            GetComponent<RectTransform>().position = initPos;
            selectedAdIndex = 0;
        }

        private void ClearChildren()
        {
            if (spawnedAds != null)
                for (int i = 0; i < spawnedAds.Length; i++)
                {
                    GameObject.Destroy(spawnedAds[i].gameObject);
                    Debug.Log(i);
                }
        }

        private void SetupAds()
        {
            for (int i = 0; i < availableAds.Length; i++)
            {
                int index = i;
                GameObject go = Instantiate(adPrefab);
                go.transform.SetParent(adParent.transform, false);
                AdInitializer ad = go.GetComponent<AdInitializer>();

                ad.SetAd(availableAds[i], () =>
                {
                    selectedAdIndex = index;
                    for (int x = 0; x < spawnedAds.Length; x++)
                    {
                        spawnedAds[x].border.SetActive(x == selectedAdIndex);
                    }
                });
                spawnedAds[i] = ad;
                spawnedAds[i].border.SetActive(i == selectedAdIndex);
            }
        }
    }
}