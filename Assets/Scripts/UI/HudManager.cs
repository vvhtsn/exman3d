﻿using ExMan.Objects;
using ExMan.Time;
using ExMan.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace ExMan.UI
{
    public class HudManager : MonoBehaviour
    {
        private static HudManager instance;
        public Text moneyText;
        public CustomImage customerSatisfactionSlider;
        public CustomImage staffSatisfactionSlider;
        public Image customerSatisfactionSmiley;
        public Image staffSatisfactionSmiley;
        public Text dayText;

        public Sprite happy, neutral, sad;

        public int minSliderValue, maxSliderValue;

        private Company company;

        public static bool isVisible
        {
            get { return (instance != null ? instance.gameObject.activeSelf : false); }
        }

        private void Start()
        {
            instance = this;

            company = CompanyManager.GetCompany();
            company.onCurrentMoneyChanged.AddListener(() =>
            {
                moneyText.text = company.currentMoney.AsMoney();
            });

            company.onCustomerSatisfactionChanged.AddListener((UnityEngine.Events.UnityAction)(() =>
            {
                customerSatisfactionSlider.fillAmount = (company.customerSatisfaction + Mathf.Abs(minSliderValue)) / (maxSliderValue + Mathf.Abs(minSliderValue));
                if (company.customerSatisfaction > 50)
                    this.customerSatisfactionSmiley.sprite = happy;
                else if (company.customerSatisfaction < -50)
                    this.customerSatisfactionSmiley.sprite = sad;
                else
                    this.customerSatisfactionSmiley.sprite = neutral;
            }));

            company.onStaffSatisfactionChanged.AddListener(() =>
            {
                staffSatisfactionSlider.fillAmount = (company.staffSatisfaction + Mathf.Abs(minSliderValue)) / (maxSliderValue + Mathf.Abs(minSliderValue));
                if (company.staffSatisfaction > 50)
                    staffSatisfactionSmiley.sprite = happy;
                else if (company.staffSatisfaction < -50)
                    staffSatisfactionSmiley.sprite = sad;
                else
                    staffSatisfactionSmiley.sprite = neutral;
            });

            company.staffSatisfaction = company.staffSatisfaction;
            company.customerSatisfaction = company.customerSatisfaction;

            dayText.text = TimeManager.currentDay.ToString();
            TimeManager.onDayChanged.AddListener(() =>
            {
                dayText.text = "Month:" + ((TimeManager.currentDay / 30) + 1) + "\nDay: " + (TimeManager.currentDay % 29).ToString();
            });

            gameObject.SetActive(false);
        }

        public static void Visible(bool v)
        {
            instance.gameObject.SetActive(v);
        }
    }
}