﻿using ExMan.ScriptableObjects;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ResumeInitializer : MonoBehaviour
{
    public Text cv;
    public Button button;
    public GameObject border;
    public GameObject namePlate;

    public Chef chef;

    public void SetResume(Chef chef, UnityAction onClick)
    {
        Text nameText = namePlate.GetComponentInChildren<Text>();
        if (button != null)
            button.onClick.AddListener(onClick);

        this.chef = chef;
        cv.text += chef.resume;
        nameText.text += chef.name;
    }
}