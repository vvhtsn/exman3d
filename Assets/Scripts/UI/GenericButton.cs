﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GenericButton : MonoBehaviour
{
    public Text text;
    public Button button;
    public GameObject border;

    public virtual void Setup(string text, UnityAction onClick)
    {
        if (this.text != null)
            this.text.text = text;
        if (button != null)
            button.onClick.AddListener(onClick);
    }
}