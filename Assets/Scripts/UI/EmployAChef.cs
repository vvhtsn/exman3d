﻿using DG.Tweening;
using ExMan.ScriptableObjects;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan.UI
{
    public class EmployAChef : UIElementTask
    {
        public Chef[] availableChefs;
        public GameObject resumePrefab;

        bool initialised;
        private Vector3 initPos;
        public Transform resumeParent;
        public Transform hireResumeParent;

        public Chef[] submittedResumes;
         
        private void OnEnable()
        {
            if (initialised == false)
            {
                initialised = true;
                initPos = GetComponent<RectTransform>().position;
            }

            GetComponent<RectTransform>().position = initPos;

            var tempList = new List<Chef>(submittedResumes);
            tempList.Clear();
            submittedResumes = tempList.ToArray();
            ClearResumeParents(resumeParent);
            ClearResumeParents(hireResumeParent);
            if (!SelectRandomResumes())
                return;

        }
        

        void ClearResumeParents(Transform parent)
        {
            foreach (Transform t in parent)
            {
                Destroy(t.gameObject);
            }
        }
        protected override void Update()
        {
            if (Application.isEditor)
                if (Input.GetKeyDown(KeyCode.F10))
                {
                    CompanyManager.HireChef(availableChefs[0]);
                    CompleteTask();
                    DOTween.Complete("UIPanelTween");
                }
        }

        public override void StartTask(UnityEvent doneCallback)
        {
            base.StartTask(doneCallback);
        }

        public bool SelectRandomResumes()
        {
            Ad selectedAd = CompanyManager.GetCompany().selectedAd;

            if (selectedAd == null)
                return false;

            int submittedResumeCount = Mathf.Clamp(UnityEngine.Random.Range(selectedAd.minResumes, selectedAd.maxResumes), 0, availableChefs.Length);
            submittedResumes = new Chef[submittedResumeCount];

            for (int i = 0; i < submittedResumeCount; i++)
            {
                Chef chef;
                do
                {
                    chef = availableChefs[UnityEngine.Random.Range(0, availableChefs.Length)];
                } while (Array.IndexOf(submittedResumes, chef) >= 0);
                submittedResumes[i] = chef;
            }
            return true;
        }

        public void SetupResumes(Transform parent, UnityAction<ResumeInitializer> callback)
        {        
            for (int i = 0; i < submittedResumes.Length; i++)
            {
                GameObject go = Instantiate(resumePrefab);
                go.transform.SetParent(parent, false);

                ResumeInitializer resume = go.GetComponent<ResumeInitializer>();
                resume.border.SetActive(false);
                resume.SetResume(submittedResumes[i], () => callback.Invoke(resume));
                //if (i == 0)
                //    resume.button.onClick.Invoke();
            }
        }
    }
}