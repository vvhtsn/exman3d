﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ExMan.Utils
{
    public static class TextFormatter
    {
        public static string AsMoney(this int number)
        {
            string str = "R";
            if (number > 1000000)
                str += Mathf.Floor(number / 1000000).ToString() + "m";
            else if (number > 1000)
                str += Mathf.Floor(number / 1000).ToString() + "k";
            else
                str += number.ToString();

            return str;
        }

        public static string AsMoney(this float number)
        {
            string str = "R";
            if (number > 1000000)
                str += Mathf.Floor(number / 1000000).ToString() + "m";
            else if (number > 1000)
                str += Mathf.Floor(number / 1000).ToString() + "k";
            else
                str += number.ToString();

            return str;
        }
    }
}
