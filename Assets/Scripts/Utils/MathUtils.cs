﻿using UnityEngine;

namespace ExMan.Utils
{
    public class MathUtils
    {
        /// <summary>
        /// Returns <paramref name="len"/> amount of <see cref="int"/> that sum to <paramref name="sum"/> with a difference of <paramref name="difference"/>.
        /// </summary>
        /// <param name="len"></param>
        /// <param name="sum"></param>
        /// <param name="difference"></param>
        /// <returns></returns>
        public static int[] RandomSumArray(int len, int sum, int difference)
        {
            var _sum = 0;
            int n = 0;
            int[] arr = new int[len];

            if (difference == 0)
            {
                difference = 100;
            }

            for (int i = 0; i < len; i++)
            {
                int from = (100 - difference) * 1000,
                    to = (100 + difference) * 1000;
                n = Mathf.FloorToInt(Random.value * (to - from + 1) + from); //random integer between from..to

                _sum += n;
                arr[i] = n;
            }

            var x = sum / _sum;

            _sum = 0; //count sum (again)
            for (int i = 0; i < len; i++)
            {
                arr[i] = Mathf.RoundToInt(arr[i] * x);
                _sum += arr[i];
            }

            var diff = sum - _sum;

            // Correct the array if its sum does not match required sum (usually by a small bit)
            if (System.Convert.ToBoolean(diff))
            {
                x = diff / Mathf.Abs(diff); //x will be 1 or -1
                var j = 0;
                while (System.Convert.ToBoolean(diff) && j < 1000)
                { //limit to a finite number of 'corrections'
                    int v = Mathf.FloorToInt(Random.value * (len)); //random index in the array
                    if (arr[v] + x >= 0)
                    {
                        arr[v] += x;
                        diff -= x;
                    }
                    j++;
                }
            }

            return arr;
        }
    }
}