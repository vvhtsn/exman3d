﻿using UnityEngine;

namespace ExMan.Utils
{
    public static class CustomFind
    {
        /// <summary>
        /// Finds all actives <see cref="Component"/> with a tag
        /// </summary>
        /// <typeparam name="T"><see cref="Component"/> to find</typeparam>
        /// <param name="tag">Tag to find</param>
        /// <param name="findInactive">Find inactive <see cref="Component"/></param>
        /// <returns></returns>
        public static T FindComponentsWithTag<T>(string tag, bool findInactive) where T : Component
        {
            T[] objects = Resources.FindObjectsOfTypeAll<T>();
            for (int i = 0; i < objects.Length; i++)
            {
                if (!string.IsNullOrEmpty(objects[i].gameObject.scene.name))
                {
                    if (objects[i].gameObject.CompareTag(tag))
                    {
                        if (objects[i].gameObject.activeSelf || (!objects[i].gameObject.activeSelf && findInactive))
                        {
                            return objects[i];
                        }
                    }
                }
            }
            return null;
        }
    }
}