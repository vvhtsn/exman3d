﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using ExMan.Tasks;
using ExMan.Objects;
using ExMan;
using ExMan.Time;
using ExMan.ScriptableObjects;
using System;
using UnityEngine.UI;
using System.Linq;
using ExMan.Utils;

public class SaveLoad : MonoBehaviour
{
    public static SaveLoad instance;
    public SaveGameData loadedGame;

    public GameObject openingPanel;

    public Chef[] chefs;
    public Character[] managers;
    public Vehicle[] vehicles;
    public Kitchen[] kitchens;

    private void Awake()
    {
        instance = this;
        if (PlayerPrefs.HasKey("gameToLoad"))
        {
            Load(PlayerPrefs.GetString("gameToLoad"));
            PlayerPrefs.DeleteKey("gameToLoad");
            StartCoroutine(WaitEndOfFrame());
        }
    }

    public IEnumerator WaitEndOfFrame()
    {
        yield return new WaitForEndOfFrame();
        Apply();
    }

    public void Save()
    {
        SaveGameData savedGame = new SaveGameData();
        savedGame.scenesCompleted = SceneQueue.instance.scenesCompleted;
        // Summary
        savedGame.summaryText = Summary.summaryText;
        savedGame.previousCustomerSatisfaction = Summary.previousCustomerSatisfaction;
        savedGame.previousStaffSatisfaction = Summary.previousStaffSatisfaction;
        savedGame.previousRevenue = Summary.previousRevenue;
        // Log
        savedGame.logDays = new List<int>();
        savedGame.logEvents = new List<string>();
        foreach(Tuple<int, string> entry in ExMan.Logger.log)
        {
            savedGame.logDays.Add(entry.item1);
            savedGame.logEvents.Add(entry.item2);
        }
        // OrderQueue
        savedGame.orderQueue = OrderQueue.instance.orders;
        savedGame.currentOrder = OrderQueue.GetCurrentOrder();
        // TimeManager
        savedGame.currentDay = TimeManager.currentDay;
        // Company
        Company company = CompanyManager.GetCompany();
        savedGame.companyName = company.name;
        savedGame.kitchen = company.selectedKitchen.name;
        savedGame.vehicle = company.selectedVehicle.name;
        savedGame.manager = company.selectedManager.name;
        savedGame.currentMoney = company.currentMoney;
        savedGame.customerSatisfaction = company.customerSatisfaction;
        savedGame.staffSatisfaction = company.staffSatisfaction;
        savedGame.staticMonthlyExpenses = company.staticMonthlyExpenses;
        savedGame.onceOffExpenses = company.onceOffExpenses;
        savedGame.menus = company.menus;
        savedGame.stockReplenishment = company.stockReplenishment;
        savedGame.idemnityInsurance = company.idemnityInsurance;
        savedGame.managerSalary = company.managerSalary;
        if (CompanyManager.GetCompany().selectedChef != null)
        {
            savedGame.chef = CompanyManager.GetCompany().selectedChef.name;
        }
        else
        {
            savedGame.chef = string.Empty;
        }

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/" + savedGame.companyName + ".dat");
        file.Position = 0;
        bf.Serialize(file, savedGame);
        file.Close();
    }

    public void Load(string filename)
    {
        if (File.Exists(filename))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(filename, FileMode.Open);
            file.Position = 0;
            loadedGame = (SaveGameData)bf.Deserialize(file);
            file.Close();
        }
    }

    public void Apply()
    {
        // Summary
        Summary.summaryText = loadedGame.summaryText;
        Summary.previousCustomerSatisfaction = loadedGame.previousCustomerSatisfaction;
        Summary.previousStaffSatisfaction = loadedGame.previousStaffSatisfaction;
        Summary.previousRevenue = loadedGame.previousRevenue;
        // TimeManager
        TimeManager.currentDay = loadedGame.currentDay - 1;
        // Clear any events that have been registered before current day
        List<KeyValuePair<int, TimeEvent>> timeEvents = TimeManager.instance.timeEvents.Where(x => x.Key >= TimeManager.currentDay).ToList();
        TimeManager.instance.timeEvents = new SortedList<int, TimeEvent>(new DuplicateKeyComparer<int>());
        foreach (KeyValuePair<int, TimeEvent> timeEvent in timeEvents)
        {
            TimeManager.instance.timeEvents.Add(timeEvent.Key, timeEvent.Value);
        }
        // Company
        Company company = CompanyManager.GetCompany();
        company.name = loadedGame.companyName;
        company.selectedKitchen = Array.Find(kitchens, x => x.name == loadedGame.kitchen);
        company.selectedVehicle = Array.Find(vehicles, x => x.name == loadedGame.vehicle);
        company.currentMoney = loadedGame.currentMoney;
        company.customerSatisfaction = loadedGame.customerSatisfaction;
        company.staffSatisfaction = loadedGame.staffSatisfaction;
        company.staticMonthlyExpenses = loadedGame.staticMonthlyExpenses;
        company.onceOffExpenses = loadedGame.onceOffExpenses;
        company.menus = loadedGame.menus;
        company.stockReplenishment = loadedGame.stockReplenishment;
        company.idemnityInsurance = loadedGame.idemnityInsurance;
        company.managerSalary = loadedGame.managerSalary;
        CompanyManager.HireManager(Array.Find(managers, x => x.name == loadedGame.manager));
        if (loadedGame.chef != string.Empty)
        {
            CompanyManager.HireChef(Array.Find(chefs, x => x.name == loadedGame.chef));
        }
        SpawnManager();

        // Create scenes if Company Budget is completed
        SceneQueue.instance.scenesCompleted = loadedGame.scenesCompleted;
        if (loadedGame.scenesCompleted >= 2)
        {
            SceneQueue.instance.CreateScenes();
        }
        OrderQueue.instance.orders = loadedGame.orderQueue;
        // Scenes have to be set before setting current order
        if (loadedGame.currentOrder != null)
        {
            OrderQueue.SetCurrentOrder(loadedGame.currentOrder);
        }
        if (TaskManager.instance.tasks.Count == 0)
        {
            TaskManager.AddTask(new TaskWrapper(new SkipToNextEvent(), true));
        }
        else
        {
            TaskManager.CompleteTask();
        }
        // Update log after everything is done so we don't get any extra logs from previous actions
        ExMan.Logger.log.Clear();
        for (int i = 0; i < loadedGame.logDays.Count; i++)
        {
            ExMan.Logger.log.Add(new Tuple<int, string>(loadedGame.logDays[i], loadedGame.logEvents[i]));
        }
        // Listeners for gameOver
        if (loadedGame.scenesCompleted >= 2)
        {
            Debug.Log("GameOverListeners");
            ExMan.Dialogue.DialogueManager.RegisterEvent("GameOver", () => GameOver.EndGame(ExMan.Dialogue.DialogueManager.instance.gameOverReason));
            company.onStaffSatisfactionChanged.AddListener(() => { if (company.staffSatisfaction < -100) GameOver.EndGame("Your staff satisfaction dropped too low"); });
            company.onCustomerSatisfactionChanged.AddListener(() => { if (company.customerSatisfaction < -100) GameOver.EndGame("Your customer satisfaction dropped too low"); });
            company.onCurrentMoneyChanged.AddListener(() => { if (company.currentMoney < -100) GameOver.EndGame("Your ran out of money"); });
        }
        openingPanel.SetActive(false);
    }

    public void SpawnManager()
    {
        GameObject[] alivePlayers = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < alivePlayers.Length; i++)
        {
            Destroy(alivePlayers[i]);
        }
        GameObject spawnPoint = GameObject.FindGameObjectWithTag("SpawnPoint");
        GameObject playerPrefab = CompanyManager.GetCompany().selectedManager.prefab;
        GameObject playerInstance = Instantiate(playerPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation, null);
        GameObject.Find("A*").GetComponent<PathFinding>().seeker = playerInstance.transform;
        FindObjectOfType<CameraFollowTag>().Init(playerInstance);
        ExMan.UI.HudManager.Visible(true);
    }
}

[System.Serializable]
public class SaveGameData
{
    public int scenesCompleted;
    public int currentDay;
    // Summary
    public string summaryText;
    public float previousCustomerSatisfaction;
    public float previousStaffSatisfaction;
    public float previousRevenue;
    // Log
    public List<int> logDays;
    public List<string> logEvents;
    // OrderQueue
    public Queue<Order> orderQueue;
    public Order currentOrder;
    // Company
    public string companyName;
    public float customerSatisfaction;
    public float staffSatisfaction;
    public float currentMoney;
    public List<Expense> staticMonthlyExpenses;
    public List<Expense> onceOffExpenses;
    public List<Menu> menus;
    public float managerSalary;
    public Insurance idemnityInsurance;
    public StockReplenishment stockReplenishment;
    public string chef;
    public string manager;
    public string kitchen;
    public string vehicle;
}