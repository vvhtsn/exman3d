﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;
[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
public class RandomWaypointMove : MonoBehaviour
{
    public delegate void reachEvent();
    public event reachEvent onReachDestination;

    public List<Transform> waypoints = new List<Transform>();
    public float minWaitTime = 1f;
    public float maxWaitTime = 5f;

    private UnityEngine.AI.NavMeshAgent agent;

    public bool hasDestination;

    public GameObject dest;
    public bool reachedDestination;

    public bool debug;
    private void Awake()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    private void Start()
    {
        StartCoroutine(Do());
    }

    private void Update()
    {
        if (debug)
        {
            onReachDestination += () => { ExMan.Dialogue.DialogueManager.CurrentMovingCharacter = null; };
            Debug.Log(dest.transform.position);
            StartCoroutine(MoveTo(dest.transform.position));
            debug = false;
        }
    }
    private IEnumerator Do()
    {
        if (!hasDestination)
        {
            yield return null;
            Transform point = waypoints[Random.Range(0, waypoints.Count)];
            if (point != null)
                agent.destination = point.position;

            yield return new WaitForSeconds(Random.Range(minWaitTime, maxWaitTime));
        }
        yield return new WaitForEndOfFrame();
        StartCoroutine(Do());

    }

    //turns off wandering behaviour, sends agent to specific point in space
    public void GoTo(Vector3 point)
    {
        StartCoroutine(MoveTo(point));
    }

    //turns on wandering behaviour
    public void StartWandering()
    {
        hasDestination = false;
    }

    IEnumerator MoveTo(Vector3 point)
    {
        reachedDestination = false;
        hasDestination = true;
        agent.destination = point;
        while (!reachedDestination)
        {
            if (CloseToDestination())
            {
                reachedDestination = true;
                onReachDestination();
                onReachDestination = null;
                break;
            }
            yield return new WaitForSeconds(1 / 20);
        }
    }

    bool CloseToDestination()
    {
        if (Vector3.Distance(agent.destination, transform.position) < 0.5f) 
            return true;

        return false;
    }

 

}