﻿using RenderHeads.Media.AVProVideo;
using System;
using UnityEngine;

public class VideoPlayer : MonoBehaviour
{
    private static VideoPlayer instance;
    private Action completeCallback;
    public MediaPlayer player;

    private void Awake()
    {
        instance = this;
        instance.player.Events.AddListener((player, type, err) =>
        {
            if (type == MediaPlayerEvent.EventType.FinishedPlaying)
            {
                if (completeCallback != null)
                    completeCallback.Invoke();

                completeCallback = null;
                gameObject.SetActive(false);
            }
        });
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F10) && Application.isEditor)
        {
            player.Control.Seek(player.Info.GetDurationMs());
        }
    }

    public static void PlayMovie(string path, Action callback)
    {
        instance.gameObject.SetActive(true);
        instance.completeCallback = callback;
        instance.player.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, path, true);
    }
}