﻿using ExMan.Time;
using ExMan.Utils;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan
{
    public class Logger : MonoBehaviour
    {
        public static List<Tuple<int, string>> log = new List<Tuple<int, string>>();
        public static UnityEvent onLogChanged = new UnityEvent();

        public static void Log(string text)
        {
            log.Add(new Tuple<int, string>(Mathf.Max(TimeManager.currentDay, 1), text));
            onLogChanged.Invoke();
        }

        public new static string ToString()
        {
            string text = "";
            for (int i = 0; i < log.Count; i++)
            {
                text += string.Format("Day {0}: {1}{2}", log[i].item1, log[i].item2, System.Environment.NewLine);
            }
            return text;
        }
    }
}