﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

namespace ExMan
{
    public class GameOver : MonoBehaviour
    {
        static GameOver instance;

        void Awake()
        {
            Debug.Log("Awake GameOver");
            gameObject.SetActive(false);
            instance = this;
        }

        void Start()
        {
            Debug.Log("start GameOver");
            //Moved to BudgedScene and GameOver

            //Dialogue.DialogueManager.RegisterEvent("GameOver", () => EndGame(Dialogue.DialogueManager.instance.gameOverReason));
            //var company = CompanyManager.GetCompany();
            //company.onStaffSatisfactionChanged.AddListener(() => { if (company.staffSatisfaction < -100) EndGame("Your staff satisfaction dropped too low"); });
            //company.onCustomerSatisfactionChanged.AddListener(() => { if (company.customerSatisfaction < -100) EndGame("Your customer satisfaction dropped too low"); });
            //company.onCurrentMoneyChanged.AddListener(() => { if (company.currentMoney < -100) EndGame("Your ran out of money"); });
        }

        public static void EndGame(string reason = "")
        {
            Debug.Log("EndGame");
            ExMan.Logger.Log("Game over " + ((!string.IsNullOrEmpty(reason)) ? reason : ""));
            instance.gameObject.SetActive(true);
        }

        public void LoadScene(string name)
        {
            SceneManager.LoadScene(name);
        }
    }
}