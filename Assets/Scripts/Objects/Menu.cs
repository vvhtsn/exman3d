﻿using System;

namespace ExMan.Objects
{
    [Serializable]
    public class Menu
    {
        public string name;
        public string description;
        public float cost;
    }
}