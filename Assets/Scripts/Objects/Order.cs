﻿using ExMan.Time;
using ExMan.Utils;

namespace ExMan.Objects
{
    [System.Serializable]
    public class Order
    {
        public string company;
        public float distance;
        public float incomePerHead;
        public string type;

        [UnityEngine.Serialization.FormerlySerializedAs("guestCount")]
        public int mealCount;
        
        public int guestCount;

        public int day;

        [UnityEngine.Range(0f, 1f)]
        public float upfrontPaymentPercentage;

        [UnityEngine.SerializeField]
        private int selectedMenuIndex;

        public Menu selectedMenu
        {
            get
            {
                return CompanyManager.GetCompany().menus[selectedMenuIndex];
            }
            set
            {
                selectedMenuIndex = CompanyManager.GetCompany().menus.IndexOf(value);
            }
        }

        public override string ToString()
        {
            string ret = "";
            ret += "Company: " + company + "\n";
            ret += "Type: " + type + "\n";
            ret += "Distance: " + distance + "KM" + "\n";
            //ret += "Income per head: " + (incomePerHead - CompanyManager.GetCompany().menus[selectedMenuIndex].cost).AsMoney() + "\n";
            ret += "Days away: " + (day - TimeManager.currentDay).ToString() + "\n";
            ret += "Guest count: " + mealCount + "\n";
            ret += "Upfront payment percentage: " + (upfrontPaymentPercentage * 100).ToString() + "%\n";
            //ret += "Menu: " + CompanyManager.GetCompany().menus[selectedMenuIndex].name.ToString() + "\n";
            return ret;
        }

        public string ToStringStart()
        {
            string ret = "";
            ret += "Company: " + company + "\n";
            ret += "Type: " + type + "\n";
            ret += "Distance: " + distance + "KM" + "\n";
            //ret += "Income per head: " + (incomePerHead - CompanyManager.GetCompany().menus[selectedMenuIndex].cost).AsMoney() + "\n";
            ret += "Days away: " + day.ToString() + "\n";
            ret += "Guest count: " + mealCount + "\n";
            ret += "Upfront payment percentage: " + (upfrontPaymentPercentage * 100).ToString() + "%\n";
            //ret += "Menu: " + CompanyManager.GetCompany().menus[selectedMenuIndex].name.ToString() + "\n";
            return ret;
        }

        public string ToStringCompleted()
        {
            string ret = "";
            ret += "Company: " + company + "\n";
            ret += "Type: " + type + "\n";
            ret += "Distance: " + distance + "KM" + "\n";
            //ret += "Income per head: " + (incomePerHead - CompanyManager.GetCompany().menus[selectedMenuIndex].cost).AsMoney() + "\n";
            ret += "Guest count: " + mealCount + "\n";
            ret += "Upfront payment percenage: " + (upfrontPaymentPercentage * 100).ToString() + "%\n";
            //ret += "Menu: " + CompanyManager.GetCompany().menus[selectedMenuIndex].name.ToString() + "\n";
            return ret;
        }

        public enum PaymentAgreement
        {
            Before,
            Half,
            After
        }
    }
}