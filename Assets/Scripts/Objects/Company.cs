﻿using ExMan.ScriptableObjects;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan.Objects
{
    public class Company
    {
        public string name;
        public Kitchen selectedKitchen;
        public Vehicle selectedVehicle;
        public Character selectedManager;
        public Chef selectedChef;
        public Ad selectedAd;
        public List<Character> employees = new List<Character>();
        public List<Expense> staticMonthlyExpenses = new List<Expense>();
        public List<Expense> onceOffExpenses = new List<Expense>();
        public List<Menu> menus = new List<Menu>();
        public float managerSalary;
        private float _currentMoney;
        private float _customerSatisfaction;
        private float _staffSatisfaction;
        public Insurance idemnityInsurance;
        public StockReplenishment stockReplenishment;

        public UnityEvent onCurrentMoneyChanged = new UnityEvent();
        public UnityEvent onCustomerSatisfactionChanged = new UnityEvent();
        public UnityEvent onStaffSatisfactionChanged = new UnityEvent();

        public float currentMoney
        {
            get
            {
                return _currentMoney;
            }

            set
            {
                _currentMoney = value;
                onCurrentMoneyChanged.Invoke();
            }
        }

        /// <summary>
        /// Should be a value between -100 and 100
        /// </summary>
        public float customerSatisfaction
        {
            get
            {
                return _customerSatisfaction;
            }

            set
            {
                _customerSatisfaction = Mathf.Clamp(value, -100f, 100f);
                onCustomerSatisfactionChanged.Invoke();
            }
        }

        /// <summary>
        /// Should be a value between -100 and 100
        /// </summary>
        public float staffSatisfaction
        {
            get
            {
                return _staffSatisfaction;
            }

            set
            {
                _staffSatisfaction = Mathf.Clamp(value, -100f, 100f);
                onStaffSatisfactionChanged.Invoke();
            }
        }

        public Company()
        {
            Menu menu1 = new Menu();
            menu1.name = "Menu 1";
            menu1.description = "Test";
            menu1.cost = 150;
            menus.Add(menu1);

            Menu menu2 = new Menu();
            menu2.name = "Menu 2";
            menu2.description = "Test";
            menu2.cost = 260;
            menus.Add(menu2);

            Menu menu3 = new Menu();
            menu3.name = "Menu 3";
            menu3.description = "Test";
            menu3.cost = 300;
            menus.Add(menu3);

            Menu menu4 = new Menu();
            menu4.name = "Menu 4";
            menu4.description = "Test";
            menu4.cost = 120;
            menus.Add(menu4);

            Menu menu5 = new Menu();
            menu5.name = "Menu 5";
            menu5.description = "Test";
            menu5.cost = 400;
            menus.Add(menu5);
        }
    }

    public enum Insurance
    {
        None = 0, R1k = 1000, R3k = 3000, R5k = 5000
    }

    public enum StockReplenishment
    {
        Essential = 8000, Standard = 12000, Full = 20000
    }
}
