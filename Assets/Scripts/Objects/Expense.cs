﻿using System;

namespace ExMan.Objects
{
    [Serializable]
    public struct Expense
    {
        public Expense(string title, float cost)
        {
            this.title = title;
            this.cost = cost;
        }

        public string title;
        public float cost;
    }
}