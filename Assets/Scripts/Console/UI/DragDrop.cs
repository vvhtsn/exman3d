﻿using UnityEngine;

namespace ExMan.Console.UI
{
    public class DragDrop : MonoBehaviour
    {
        public RectTransform moveBox;

        private Vector2 offset;
        private bool dragging;

        private void Awake()
        {
            if (moveBox == null)
                Debug.LogError("MoveBox is null on DragDrop", this);
        }

        internal void BeginDrag()
        {
            if (moveBox == null)
                return;

            offset = moveBox.position - Input.mousePosition;
            dragging = true;
        }

        private void Update()
        {
            if (moveBox == null)
                return;

            if (dragging)
                moveBox.position = (Vector2)Input.mousePosition + offset;
        }

        internal void EndDrag()
        {
            dragging = false;
        }
    }
}