﻿using ExMan.Tasks;
using ExMan.Time;
using ExMan.UI;
using System.Linq;
using UnityEngine;

namespace ExMan.Console
{
    internal static class BasicCommands
    {
        [ConsoleCommand("quit", "Quit the application")]
        internal static void Quit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            UnityEngine.Application.Quit();
#endif
        }

        [ConsoleCommand("exit", "Close the console")]
        internal static void HideConsole()
        {
            DebugConsole.Hide();
        }

        #region Graphics

        [ConsoleCommand("graphics.fov", "Set the field of view of the main camera")]
        private static void SetFov(float fov)
        {
            if (Camera.main != null)
                Camera.main.fieldOfView = fov;
        }

        [ConsoleCommand("graphics.fov", "Get the field of view of the main camera")]
        private static float GetFov()
        {
            if (Camera.main != null)
                return Camera.main.fieldOfView;
            return 0;
        }

        [ConsoleCommand("graphics.fullscreen", "Toggle fullscreen")]
        private static void FullScreen()
        {
            Screen.fullScreen = !Screen.fullScreen;
        }

        [ConsoleCommand("graphics.quality", "Get the current quality level")]
        private static int GetQuality()
        {
            return QualitySettings.GetQualityLevel();
        }

        [ConsoleCommand("graphics.quality", "Set the quality level")]
        private static void SetQuality(int level)
        {
            QualitySettings.SetQualityLevel(level, true);
        }

        [ConsoleCommand("graphics.resolution", "Get the current resolution")]
        private static string GetResolution()
        {
            return Screen.currentResolution.ToString();
        }

        [ConsoleCommand("graphics.resolution", "Set the resolution")]
        private static void SetResolution(int width, int height)
        {
            Screen.SetResolution(width, height, Screen.fullScreen);
        }

        [ConsoleCommand("graphics.toggleHUD", "Toggle the hud visibility")]
        private static void ToggleHud()
        {
            HudManager.Visible(!HudManager.isVisible);
        }

        #endregion Graphics

        #region Company

        [ConsoleCommand("money", "Get or set the current amount of money")]
        private static float GetMoney()
        {
            return CompanyManager.GetCompany().currentMoney;
        }

        [ConsoleCommand("money", "Get or set the current amount of money")]
        private static void SetMoney(float money)
        {
            CompanyManager.GetCompany().currentMoney = money;
        }

        [ConsoleCommand("customerSatisfaction", "Get the customer satisfaction")]
        private static float GetCustomerSatisfaction()
        {
            return CompanyManager.GetCompany().customerSatisfaction;
        }

        [ConsoleCommand("customerSatisfaction", "Set the customer satisfaction")]
        private static void SetCustomerSatisfaction(float value)
        {
            CompanyManager.GetCompany().customerSatisfaction = value;
        }

        [ConsoleCommand("staffSatisfaction", "Get the staff satisfaction")]
        private static float GetStaffSatisfaction()
        {
            return CompanyManager.GetCompany().staffSatisfaction;
        }

        [ConsoleCommand("staffSatisfaction", "Set the staff satisfaction")]
        private static void SetStaffSatisfaction(float value)
        {
            CompanyManager.GetCompany().staffSatisfaction = value;
        }

        #endregion Company

        #region Days

        [ConsoleCommand("day")]
        private static int GetCurrentDay()
        {
            return TimeManager.currentDay;
        }

        [ConsoleCommand("skipDays")]
        private static void SkipDays(int amount)
        {
            TimeManager.SkipDays(amount);
        }

        [ConsoleCommand("skipDays")]
        private static void SkipDays(int amount, bool allowedToInterrupt)
        {
            TimeManager.SkipDays(amount, allowedToInterrupt);
        }

        [ConsoleCommand("nextDay", "Go to the next day. Returns true if there are any events for this day")]
        private static string NextDay()
        {
            return string.Join(",", TimeManager.NextDay().Select(x => x.ToString()).ToArray());
        }

        [ConsoleCommand("logAllEvents")]
        private static void logAllEvents()
        {
            TimeManager.logAllEvents();
        }

        [ConsoleCommand("logAllTasks")]
        private static void logAllTasks()
        {
            TaskManager.logAllTasks();
        }

        [ConsoleCommand("currentTasks")]
        private static void currentTasks()
        {
            TaskManager.currentTasks();
        }

        [ConsoleCommand("daysToNextEvent")]
        private static void daysToNextEvent()
        {
            Debug.Log(TimeManager.GetDaysTillNextEvent());
        }
        

        #endregion Days
    }
}