﻿using System;

namespace ExMan.Console
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class ConsoleCommandAttribute : Attribute
    {
        public string command { get; private set; }

        public string help { get; set; }

        public ConsoleCommandAttribute(string command)
        {
            this.command = command;
        }

        public ConsoleCommandAttribute(string command, string help)
        {
            this.command = command;
            this.help = help;
        }
    }
}