﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

namespace ExMan.Components
{
    [RequireComponent(typeof(Collider))]
    public class TriggerEvent : MonoBehaviour
    {
        public UnityEvent onEnter;
        public UnityEvent onTaskStart;
        public UnityEvent onPromptStart;
        public UnityEvent onTaskDone;
        public UnityEvent onExit;

        public string requiredTag;
        private UnityEvent doneCallback;

        private void Awake()
        {
            GetComponent<Collider>().isTrigger = true;
        }

        public void StartTask(UnityEvent doneCallback)
        {
            this.doneCallback = doneCallback;
            onTaskStart.Invoke();
        }

        public void StartPrompt()
        {
            onPromptStart.Invoke();
        }

        public void CompleteTask()
        {
            onTaskDone.Invoke();
            if (doneCallback != null)
                doneCallback.Invoke();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag(requiredTag))
            {
                onEnter.Invoke();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag(requiredTag))
                onExit.Invoke();
        }

    }
}