﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace ExMan.Components
{
    [RequireComponent(typeof(Text))]
    public class TypeWriter : MonoBehaviour
    {
        private Text textBox;
        private Coroutine currentCoroutine;

        public float charsPerSec = 10f;

        public string text
        {
            get { return textBox.text; }
            set { textBox.text = value; }
        }

        private void Awake()
        {
            textBox = GetComponent<Text>();
        }

        public void Write(string text)
        {
            if (currentCoroutine != null)
                StopCoroutine(currentCoroutine);

            currentCoroutine = StartCoroutine(DelayedWrite(text));
        }

        private IEnumerator DelayedWrite(string text)
        {
            textBox.text = string.Empty;

            if (string.IsNullOrEmpty(text))
                yield break;

            foreach (char character in text)
            {
                textBox.text += character;
                yield return new WaitForSeconds(1f / charsPerSec);
            }
            currentCoroutine = null;
        }
    }
}