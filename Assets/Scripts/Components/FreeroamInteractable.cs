﻿using UnityEngine;
using UnityEngine.Events;

namespace ExMan
{
	[RequireComponent(typeof(Collider))]
	public class FreeroamInteractable : MonoBehaviour
	{
		public string playerTag = "Player";
		public UnityEvent onInteraction;

		private bool inRange;

		private void OnTriggerEnter(Collider other)
		{
			if (other.CompareTag (playerTag))
				inRange = true;
		}

		private void OnTriggerExit(Collider other)
		{
			if (other.CompareTag (playerTag))
				inRange = false;
		}

		private void OnMouseDown() 
		{
			if (inRange && onInteraction != null) 
			{
				onInteraction.Invoke ();
				OnInteraction (GameObject.FindGameObjectWithTag(playerTag));
			}
		}

		public virtual void OnInteraction(GameObject player) { }
	}
}