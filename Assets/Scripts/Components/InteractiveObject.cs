﻿using UnityEngine;
using UnityEngine.Events;

namespace ExMan.Components
{
    [RequireComponent(typeof(Collider))]
    public class InteractiveObject : MonoBehaviour
    {
        public bool requiresObjectInRange;
        public float requiredRange = Mathf.Infinity;
        public string requiredTag;

        public UnityEvent mouseEnter;
        public UnityEvent mouseExit;
        public UnityEvent mouseDown;
        public UnityEvent mouseUp;

        private void OnMouseDown()
        {
            Invoke(mouseDown);
        }

        private void OnMouseUp()
        {
            Invoke(mouseUp);
        }

        private void OnMouseEnter()
        {
            Invoke(mouseEnter);
        }

        private void OnMouseExit()
        {
            Invoke(mouseExit);
        }

        private void Invoke(UnityEvent evnt)
        {
            if (requiresObjectInRange)
            {
                var go = GameObject.FindGameObjectWithTag(requiredTag);
                if (go == null || Vector3.Distance(go.transform.position, transform.position) > requiredRange)
                    return;
            }

            evnt.Invoke();
        }
    }
}