﻿using ExMan.Dialogue;
using ExMan.Tasks;
using System.Collections.Generic;
using UnityEngine;

namespace ExMan
{
    public class SceneQueue : MonoBehaviour
    {
        public static SceneQueue instance;
        private Queue<TaskWrapper> sceneQueue = new Queue<TaskWrapper>();
        public int scenesCompleted = 0;
        public bool debug;

        private void Awake()
        {
            instance = this;
        }

        public void CreateScenes()
        {
            if (scenesCompleted < 3)
              sceneQueue.Enqueue(CreateKosherFiascoScene());
            if (scenesCompleted < 4)
                sceneQueue.Enqueue(CreateChefVSWaitressScene());
            if (scenesCompleted < 5)
                sceneQueue.Enqueue(CreatePowerOutageScene());
            if (scenesCompleted < 6)
                sceneQueue.Enqueue(CreateSexualharassmentScene());
        }

        private TaskWrapper CreateKosherFiascoScene()
        {
            TaskWrapper recordSummaryBefore = new TaskWrapper(new RecordCompanySummary("Start of Customer Complaint", true), false);
            TaskWrapper disableHud = new TaskWrapper(new ToggleGameObject(false, Tags.hudCanvas), false);
            TaskWrapper enableHud = new TaskWrapper(new ToggleGameObject(true, Tags.hudCanvas), false);
            TaskWrapper waitOrTrigger = new TaskWrapper(new WaitForSecondsOrTrigger(30, Tags.officeTrigger, notificationTitle: "Office", notificationContent: "You should go to your office"), "OfficeWaypoint", false);
            TaskWrapper trigger = new TaskWrapper(new ActivateTriggerArea(Tags.telephoneTrigger, notificationTitle: "Telephone", notificationContent: "Your phone is ringing, go to your desk to pick it up"), "OfficeWaypoint", false);
            TaskWrapper managerClientDialogue = new TaskWrapper(new StartDialogue("Dissatisfied_customer"), false);
            TaskWrapper selection = new TaskWrapper(new EnableUIElement<UI.UIElementTask>(Tags.alternativesOrConflict), false);
            TaskWrapper recordSummaryAfter = new TaskWrapper(new RecordCompanySummary("End of Customer Complaint"), false);
            TaskWrapper saveGame = new TaskWrapper(new SaveGame(), false);

            TaskWrapper kosherFiascoScene = new TaskWrapper(new ChildrenDone(), false, recordSummaryBefore, waitOrTrigger, trigger, disableHud, managerClientDialogue, selection, enableHud, recordSummaryAfter, saveGame);
            return kosherFiascoScene;
        }

        private TaskWrapper CreateChefVSWaitressScene()
        {
            TaskWrapper recordSummaryBefore = new TaskWrapper(new RecordCompanySummary("Start of Staff Conflict", true), false);
            TaskWrapper disableHud = new TaskWrapper(new ToggleGameObject(false, Tags.hudCanvas), false);
            TaskWrapper enableHud = new TaskWrapper(new ToggleGameObject(true, Tags.hudCanvas), false);
            TaskWrapper waitOrTrigger = new TaskWrapper(new WaitForSecondsOrTrigger(30, Tags.officeTrigger, notificationTitle: "Office", notificationContent: "You should go to your office"), false);
            TaskWrapper chefVSWaitressDialogue = new TaskWrapper(new StartDialogue("Staff_conflict_Chef", () => 
            {
                DialogueManager.characterDefinitions.chef.sceneObject.GetComponent<RandomWaypointMove>().StartWandering();
                DialogueManager.characterDefinitions.waitress.sceneObject.GetComponent<RandomWaypointMove>().StartWandering();
            }, new GameObject[] { DialogueManager.characterDefinitions.waitress.sceneObject, DialogueManager.characterDefinitions.chef.sceneObject }, new Vector3(-5, 0, -0.5f)), false);
            TaskWrapper recordSummaryAfter = new TaskWrapper(new RecordCompanySummary("End of Staff Conflict"), false);
            TaskWrapper saveGame = new TaskWrapper(new SaveGame(), false);

            TaskWrapper chefVSWaitressScene = new TaskWrapper(new ChildrenDone(), false, recordSummaryBefore, waitOrTrigger, disableHud, chefVSWaitressDialogue, enableHud, recordSummaryAfter, saveGame);
            return chefVSWaitressScene;
        }

        private TaskWrapper CreateSexualharassmentScene()
        {
            TaskWrapper recordSummaryBefore = new TaskWrapper(new RecordCompanySummary("Start of Sexual Harassment", true), false);
            TaskWrapper disableHud = new TaskWrapper(new ToggleGameObject(false, Tags.hudCanvas), false);
            TaskWrapper enableHud = new TaskWrapper(new ToggleGameObject(true, Tags.hudCanvas), false);
            TaskWrapper waitOrTrigger = new TaskWrapper(new WaitForSecondsOrTrigger(30, Tags.deliveryTrigger, notificationTitle: "Delivery Area", notificationContent: "Something is going on in the delivery area, you should go check it out", waypoint: "GarageWaypoint"), false);
            TaskWrapper sexualHarassmentDialogue = new TaskWrapper(new StartDialogue("Sexual_harassment_Chef", () => { DialogueManager.characterDefinitions.waitress.sceneObject.GetComponent<RandomWaypointMove>().StartWandering(); }, new GameObject[] { DialogueManager.characterDefinitions.waitress.sceneObject }, new Vector3(3, 0, -5)), false);
            TaskWrapper recordSummaryAfter = new TaskWrapper(new RecordCompanySummary("End of Sexual Harassment"), false);
            TaskWrapper saveGame = new TaskWrapper(new SaveGame(), false);

            TaskWrapper sexualHarassmentScene = new TaskWrapper(new ChildrenDone(), false, recordSummaryBefore, waitOrTrigger, disableHud, sexualHarassmentDialogue, enableHud, recordSummaryAfter, saveGame);
            return sexualHarassmentScene;
        }

        private TaskWrapper CreatePowerOutageScene()
        {
            TaskWrapper recordSummaryBefore = new TaskWrapper(new RecordCompanySummary("Start of Problem Solving", true), false);
            TaskWrapper disableHud = new TaskWrapper(new ToggleGameObject(false, Tags.hudCanvas), false);
            TaskWrapper enableHud = new TaskWrapper(new ToggleGameObject(true, Tags.hudCanvas), false);
            // TODO: Toggle lights
            TaskWrapper waitOrTrigger = new TaskWrapper(new WaitForSecondsOrTrigger(30, Tags.kitchenTrigger, notificationTitle: "Talk to the chef", notificationContent: "You should go talk to the chef"), false);
            TaskWrapper beforeUIDialogue = new TaskWrapper(new StartDialogue("General_problem_solving_Chef", () => { DialogueManager.characterDefinitions.chef.sceneObject.GetComponent<RandomWaypointMove>().StartWandering(); }, new GameObject[] { DialogueManager.characterDefinitions.chef.sceneObject }, new Vector3(1.4f, 0, 1)), false);
            TaskWrapper selection = new TaskWrapper(new EnableUIElement<UI.UIElementTask>(Tags.menuPresenting), false);
            TaskWrapper recordSummaryAfter = new TaskWrapper(new RecordCompanySummary("End of Problem Solving"), false);
            TaskWrapper saveGame = new TaskWrapper(new SaveGame(), false);

            TaskWrapper powerOutageScene = new TaskWrapper(new ChildrenDone(), false, recordSummaryBefore, waitOrTrigger, disableHud, beforeUIDialogue, selection, enableHud, recordSummaryAfter, saveGame);
            return powerOutageScene;
        }

        public static TaskWrapper GetNextScene()
        {
            if (instance.sceneQueue.Count != 0)
                return instance.sceneQueue.Dequeue();
            else
                GameOver.EndGame("Congratulations! You've proven yourself as a skillful company manager! Thank you for playing the game.");

            return null;
        }
    }
}