﻿namespace ExMan.Events
{
    public class SoftwareUpgrade : Event
    {
        public SoftwareUpgrade()
        {
            this.title = "Random Event!";
            this.description = @"You upgrade your catering software and are able to eliminate unnecessary storeroom purchases...
You gain R6000";
        }

        public override void OnComplete()
        {
            CompanyManager.ReceiveMoney(6000, @"You upgrade your catering software and are able to eliminate unnecessary storeroom purchases...");
        }
    }
}