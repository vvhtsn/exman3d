﻿namespace ExMan.Events
{
    public class RegionalCompetition : Event
    {
        public RegionalCompetition()
        {
            this.title = "Random Event!";
            this.description = @"Your company wins a regional cook-off against rival companies...
Staff and client satisfaction increases";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(3, "Your company wins a regional cook-off against rival companies...");
            CompanyManager.ModifyCustomerSatisfaction(4, "Your company wins a regional cook-off against rival companies...");
        }
    }
}