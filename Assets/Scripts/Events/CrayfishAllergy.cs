﻿namespace ExMan.Events
{
    public class CrayfishAllergy : Event
    {
        public CrayfishAllergy()
        {
            this.title = "Random Event!";
            this.description = @"At your last event, one of the guests had a food allergy to the crayfish starter. The client greatly appreciated that your chef immediately adjusted this guest's starter...
Client satisfaction increases";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyCustomerSatisfaction(2, "At your event, one of the guests had a food allergy to the crayfish starter. The client greatly appreciated that your chef immediately adjusted this guest's starter...");
        }
    }
}