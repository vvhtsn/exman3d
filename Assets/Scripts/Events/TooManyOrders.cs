﻿namespace ExMan.Events
{
    public class TooManyOrders : Event
    {
        public TooManyOrders()
        {
            this.title = "Random Event!";
            this.description = @"You have accepted too many functions for your current number of kitchen staff...
Staff satisfaction drops";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedKitchen.name == "Small Kitchen";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(-8, "You have accepted too many functions for your current number of kitchen staff...");
        }

        public override Event[] GetUnlockedEvents()
        {
            return new Event[] { new AdditionalStaff(), new StaffMistakes() };
        }
    }
}