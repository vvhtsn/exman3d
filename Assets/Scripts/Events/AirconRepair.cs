﻿namespace ExMan.Events
{
    public class AirconRepair : Event
    {
        public AirconRepair()
        {
            this.title = "Random Event!";
            this.description = @"You have decided to repair the air conditioner after months that it has not been working...
You lose R2500";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("You have decided to repair the air conditioner after months that it has not been working...", 2500));
        }
    }
}