﻿namespace ExMan.Events
{
    public class SupermarketContract : Event
    {
        public SupermarketContract()
        {
            this.title = "Random Event!";
            this.description = @"You have signed a contract with a large supermarket to supply some of their hot convenience foods...
You gain R10000";
        }

        public override void OnComplete()
        {
            CompanyManager.ReceiveMoney(10000, "You have signed a contract with a large supermarket to supply some of their hot convenience foods...");
        }
    }
}