﻿namespace ExMan.Events
{
    public class UpsideDownFork : Event
    {
        public UpsideDownFork()
        {
            this.title = "Random Event!";
            this.description = @"Your ""Upside - down Fork"" presentation style has become a trend in the food service industry...
Staff satisfaction increases";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(3, @"Your ""Upside - down Fork"" presentation style has become a trend in the food service industry...");
        }
    }
}