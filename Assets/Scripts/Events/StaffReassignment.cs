﻿namespace ExMan.Events
{
    public class StaffReassignment : Event
    {
        public StaffReassignment()
        {
            this.title = "Random Event!";
            this.description = @"Your chef has reassigned the kitchen staff so that the skills of the staff are optimally matched to the kitchen tasks...
Staff satisfaction increases";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedChef.name == "Gina";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(4, "Your chef has reassigned the kitchen staff so that the skills of the staff are optimally matched to the kitchen tasks...");
        }
    }
}