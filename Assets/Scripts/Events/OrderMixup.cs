﻿namespace ExMan.Events
{
    public class OrderMixup : Event
    {
        public OrderMixup()
        {
            this.title = "Random Event!";
            this.description = @"You have simultaneous events with two different menus. The orders were mixed up and each of the Clients received the wrong food...
Client satisfaction drops";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyCustomerSatisfaction(-6, "You have simultaneous events with two different menus. The orders were mixed up and each of the Clients received the wrong food...");
        }
    }
}