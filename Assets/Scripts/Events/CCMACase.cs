﻿namespace ExMan.Events
{
    public class CCMACase : Event
    {
        public CCMACase()
        {
            this.title = "Random Event!";
            this.description = @"The cook you fired has filed a CCMA case against you for not following the appropriate dismissal procedure...
You lose R20000";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("The cook you fired has filed a CCMA case against you for not following the appropriate dismissal procedure...", 20000));
        }
    }
}