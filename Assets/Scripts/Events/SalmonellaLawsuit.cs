﻿namespace ExMan.Events
{
    public class SalmonellaLawsuit : Event
    {
        public SalmonellaLawsuit()
        {
            this.title = "Random Event!";
            this.description = @"One of the Clients who contracted salmonella are suing you for all their hospital expenses...
" + ((CompanyManager.GetCompany().idemnityInsurance == Objects.Insurance.None) ? "You lose R100000" : "You lose no revenue due to your insurance");
        }

        public override void OnComplete()
        {
            if (CompanyManager.GetCompany().idemnityInsurance == Objects.Insurance.None)
                CompanyManager.SpendMoney(new Objects.Expense("You have decided to repair the air conditioner after months that it has not been working...", 100000));
        }
    }
}