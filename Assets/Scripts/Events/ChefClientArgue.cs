﻿namespace ExMan.Events
{
    public class ChefClientArgue : Event
    {
        public ChefClientArgue()
        {
            this.title = "Random Event!";
            this.description = @"Your chef argues with one of the guests about the herbs that were used in the minestrone...
Client satisfaction drops";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedChef.name == "Antonio";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyCustomerSatisfaction(-6, "Your chef argues with one of the guests about the herbs that were used in the minestrone...");
        }
    }
}