﻿namespace ExMan.Events
{
    public class LocalFoodCritic : Event
    {
        public LocalFoodCritic()
        {
            this.title = "Random Event!";
            this.description = @"The local food critic was a guest at one of your events and has written a positive review on your catering in this week's newspaper...
You gain R7500 and staff satisfaction increases";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(5, "The local food critic was a guest at one of your events and has written a positive review on your catering in this week's newspaper...");
            CompanyManager.ReceiveMoney(7500, "The local food critic was a guest at one of your events and has written a positive review on your catering in this week's newspaper...");
        }
    }
}