﻿namespace ExMan.Events
{
    public class DishReplacement : Event
    {
        public DishReplacement()
        {
            this.title = "Random Event!";
            this.description = @"Some staff are ill and you have not trained your staff to work at different stations so some dishes had to be replaced...
Client satisfaction drops";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyCustomerSatisfaction(-4, "Some staff are ill and you have not trained your staff to work at different stations so some dishes had to be replaced...");
        }

        public override Event[] GetUnlockedEvents()
        {
            return new Event[] { new StaffTraining(), new ChefDisgruntled() };
        }
    }
}