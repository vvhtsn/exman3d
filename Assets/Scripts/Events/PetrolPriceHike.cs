﻿namespace ExMan.Events
{
    public class PetrolPriceHike : Event
    {
        public PetrolPriceHike()
        {
            this.title = "Random Event!";
            this.description = @"There has been a petrol price increase and you were not able to adjust your last order's prices...
You lose R1000";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("Petrol price hike", 1000));
        }
    }
}