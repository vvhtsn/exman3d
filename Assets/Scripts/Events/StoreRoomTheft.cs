﻿namespace ExMan.Events
{
    public class StoreRoomTheft : Event
    {
        public StoreRoomTheft()
        {
            this.title = "Random Event!";
            this.description = @"There have been multiple instances of storeroom theft...
You lose R3000";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedKitchen.name == "Large Kitchen";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("There have been multiple instances of storeroom theft...", 3000));
        }

        public override Event[] GetUnlockedEvents()
        {
            return new Event[] { new OutOfIngredients(), new StoreRoomTheftInvestigation() };
        }
    }
}