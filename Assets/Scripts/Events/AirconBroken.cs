﻿namespace ExMan.Events
{
    public class AirconBroken : Event
    {
        public AirconBroken()
        {
            this.title = "Random Event!";
            this.description = @"The air conditioner has stopped working and you keep putting the repairs off...
Staff satisfaction drops";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedKitchen.name == "Medium Kitchen";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(-7, "The air conditioner has stopped working and you keep putting the repairs off...");
        }

        public override Event[] GetUnlockedEvents()
        {
            return new Event[] { new AirconRepair() };
        }
    }
}