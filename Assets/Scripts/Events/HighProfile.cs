﻿namespace ExMan.Events
{
    public class HighProfile : Event
    {
        public HighProfile()
        {
            this.title = "Random Event!";
            this.description = @"You manage to land a high-profile celebrity event...
You gain R5000";
        }

        public override void OnComplete()
        {
            CompanyManager.ReceiveMoney(5000, "You manage to land a high-profile celebrity event...");
        }
    }
}