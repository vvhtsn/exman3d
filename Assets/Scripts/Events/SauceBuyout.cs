﻿namespace ExMan.Events
{
    public class SauceBuyout : Event
    {
        public SauceBuyout()
        {
            this.title = "Random Event!";
            this.description = @"The chef keeps buying the sauces out...
You lose R1000";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedChef.name == "Thabang";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("The chef keeps buying the sauces out...", 1000));
        }
    }
}