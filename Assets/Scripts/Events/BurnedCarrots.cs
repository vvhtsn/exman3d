﻿namespace ExMan.Events
{
    public class BurnedCarrots : Event
    {
        public BurnedCarrots()
        {
            this.title = "Random Event!";
            this.description = @"One of the cooks keeps on burning the carrots so you fire the cook...
You lose R1000 and Staff satisfaction drops";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("One of the cooks keeps on burning the carrots so you fire the cook...", 1000));
            CompanyManager.ModifyStaffSatisfaction(-2, "One of the cooks keeps on burning the carrots so you fire the cook...");
        }

        public override Event[] GetUnlockedEvents()
        {
            return new Event[] { new CCMACase() };
        }
    }
}