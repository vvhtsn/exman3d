﻿namespace ExMan.Events
{
    public class StaffTraining : Event
    {
        public StaffTraining()
        {
            this.title = "Random Event!";
            this.description = @"You send your staff to be trained for different stations...
You lose R10000";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("You send your staff to be trained for different stations...", 10000));
        }
    }
}