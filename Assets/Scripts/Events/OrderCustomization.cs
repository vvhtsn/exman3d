﻿namespace ExMan.Events
{
    public class OrderCustomization : Event
    {
        public OrderCustomization()
        {
            this.title = "Random Event!";
            this.description = @"Your chef has customised the last three orders exactly to the client's wishes...
Client satisfaction goes up";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedChef.name == "Thabang";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyCustomerSatisfaction(5, "Your chef has customised the last three orders exactly to the client's wishes...");
        }
    }
}