﻿namespace ExMan.Events
{
    [System.Serializable]
    public class Event
    {
        public string title;
        public string description;

        public virtual bool IsAvailable()
        {
            return true;
        }

        public virtual void OnComplete()
        {
        }

        public virtual Event[] GetUnlockedEvents()
        {
            return null;
        }
    }
}