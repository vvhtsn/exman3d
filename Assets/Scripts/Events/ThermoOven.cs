﻿namespace ExMan.Events
{
    public class ThermoOven : Event
    {
        public ThermoOven()
        {
            this.title = "Random Event!";
            this.description = @"Your thermo-fan oven breaks and you had to have it repaired...
You lose R3000";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("Your thermo-fan oven breaks and you had to have it repaired...", 3000));
        }
    }
}