﻿namespace ExMan.Events
{
    public class HACCPInspection : Event
    {
        public HACCPInspection()
        {
            this.title = "Random Event!";
            this.description = @"There has been a HACCP inspection of your kitchen and they have fined you for preparing the vegetables and chicken at the same station...
You lose R10000";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("There has been a HACCP inspection of your kitchen and they have fined you for preparing the vegetables and chicken at the same station...", 10000));
        }
    }
}