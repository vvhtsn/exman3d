﻿namespace ExMan.Events
{
    public class SuperVeggieSlicer : Event
    {
        public SuperVeggieSlicer()
        {
            this.title = "Random Event!";
            this.description = @"You install the renowned ""Super Veggie Slicer"" in order to make the vegetable station more efficient...
Staff satisfaction increases";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(5, @"You install the renowned ""Super Veggie Slicer"" in order to make the vegetable station more efficient...");
        }
    }
}