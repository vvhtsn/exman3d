﻿namespace ExMan.Events
{
    public class ConsolidationDiploma : Event
    {
        public ConsolidationDiploma()
        {
            this.title = "Random Event!";
            this.description = @"Your chef has completed an advanced diploma in order consolidation, maximising the kitchen production...
You gain R5000";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedChef.name == "Gina";
        }

        public override void OnComplete()
        {
            CompanyManager.ReceiveMoney(5000, "Your chef has completed an advanced diploma in order consolidation, maximising the kitchen production...");
        }
    }
}