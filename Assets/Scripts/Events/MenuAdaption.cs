﻿namespace ExMan.Events
{
    public class MenuAdaption : Event
    {
        public MenuAdaption()
        {
            this.title = "Random Event!";
            this.description = @"The chef continually adapts the set menus...
Client satisfaction drops";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedChef.name == "Antonio";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyCustomerSatisfaction(-5, "The chef continually adapts the set menus...");
        }
    }
}