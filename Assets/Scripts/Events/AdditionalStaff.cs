﻿namespace ExMan.Events
{
    public class AdditionalStaff : Event
    {
        public AdditionalStaff()
        {
            this.title = "Random Event!";
            this.description = @"You have employed additional staff to cope with the overload of orders you have accepted...
You lose R12000";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("You have employed additional staff to cope with the overload of orders you have accepted...", 12000));
        }
    }
}