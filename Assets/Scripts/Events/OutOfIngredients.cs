﻿namespace ExMan.Events
{
    public class OutOfIngredients : Event
    {
        public OutOfIngredients()
        {
            this.title = "Random Event!";
            this.description = @"Because of storeroom theft, there is not enough base ingredients for your next order and you have to buy ingredients at a local supermarket...
You lose R4000";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("Because of storeroom theft, there is not enough base ingredients for your next order and you have to buy ingredients at a local supermarket...", 4000));
        }
    }
}