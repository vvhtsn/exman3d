﻿namespace ExMan.Events
{
    public class DishBuyout : Event
    {
        public DishBuyout()
        {
            this.title = "Random Event!";
            this.description = @"Some staff members did not return to work after a day off and now you have to buy some dishes from a rival company...
You lose R7500";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("Some staff members did not return to work after a day off and now you have to buy some dishes from a rival company...", 7500));
        }
    }
}