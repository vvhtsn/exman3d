﻿namespace ExMan.Events
{
    public class UsableTomatoes : Event
    {
        public UsableTomatoes()
        {
            this.title = "Random Event!";
            this.description = @"Your chef throws away a batch of usable tomatoes because they were more than 3 days old...
You lose R1500";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedChef.name == "Antonio";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("Your chef throws away a batch of usable tomatoes because they were more than 3 days old...", 1500));
        }
    }
}