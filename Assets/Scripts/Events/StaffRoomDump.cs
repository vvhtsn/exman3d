﻿namespace ExMan.Events
{
    public class StaffRoomDump : Event
    {
        public StaffRoomDump()
        {
            this.title = "Random Event!";
            this.description = @"The staff room has become a dumping ground for the boxes from the non-perishable items you bought on special...
Staff satisfaction drops";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(-2, "The staff room has become a dumping ground for the boxes from the non-perishable items you bought on special...");
        }
    }
}