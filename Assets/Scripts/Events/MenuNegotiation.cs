﻿namespace ExMan.Events
{
    public class MenuNegotiation : Event
    {
        public MenuNegotiation()
        {
            this.title = "Random Event!";
            this.description = @"Chef negotiates a new menu with the client without informing you...
You lose R2500";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedChef.name == "Gina";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("Chef negotiates a new menu with the client without informing you...", 2500));
        }
    }
}