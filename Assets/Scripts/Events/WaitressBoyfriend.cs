﻿namespace ExMan.Events
{
    public class WaitressBoyfriend : Event
    {
        public WaitressBoyfriend()
        {
            this.title = "Random Event!";
            this.description = @"Your waitress is having boyfriend trouble and she is now being rude to all the male staff...
Staff satisfaction drops";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(-3, "Your waitress is having boyfriend trouble and she is now being rude to all the male staff...");
        }
    }
}