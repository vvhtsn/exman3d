﻿namespace ExMan.Events
{
    public class FunctionEntertainment : Event
    {
        public FunctionEntertainment()
        {
            this.title = "Random Event!";
            this.description = @"The entertainment you organised with your last event was enjoyed by everyone attending...
Client satisfaction increases";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyCustomerSatisfaction(3, "The entertainment you organised with your event was enjoyed by everyone attending...");
        }
    }
}