﻿namespace ExMan.Events
{
    public class StaffVolunteering : Event
    {
        public StaffVolunteering()
        {
            this.title = "Random Event!";
            this.description = @"Some of your staff have volunteered to cook at the old-age home on their off days...
Client satisfaction increases";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyCustomerSatisfaction(4, "Some of your staff have volunteered to cook at the old-age home on their off days...");
        }
    }
}