﻿namespace ExMan.Events
{
    public class SupplyQuality : Event
    {
        public SupplyQuality()
        {
            this.title = "Random Event!";
            this.description = @"Your clients are impressed with your company's catering and have spread the word that you supply quality meals...
You gain +R6000";
        }

        public override void OnComplete()
        {
            CompanyManager.ReceiveMoney(6000, "Your clients are impressed with your company's catering and have spread the word that you supply quality meals...");
        }
    }
}