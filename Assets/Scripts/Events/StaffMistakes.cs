﻿namespace ExMan.Events
{
    public class StaffMistakes : Event
    {
        public StaffMistakes()
        {
            this.title = "Random Event!";
            this.description = @"The staff are making mistakes because you are working the kitchen beyond its capacity...
Client satisfaction drops";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyCustomerSatisfaction(-4, "The staff are making mistakes because you are working the kitchen beyond its capacity...");
        }
    }
}