﻿namespace ExMan.Events
{
    public class PotjieKosCompetition : Event
    {
        public PotjieKosCompetition()
        {
            this.title = "Random Event!";
            this.description = @"Your chef wins the local potjie kos competition...
Staff satisfaction increases";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedChef.name == "Antonio";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(4, "Your chef wins the local potjie kos competition...");
        }
    }
}