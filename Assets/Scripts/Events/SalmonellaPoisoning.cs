﻿namespace ExMan.Events
{
    public class SalmonellaPoisoning : Event
    {
        public SalmonellaPoisoning()
        {
            this.title = "Random Event!";
            this.description = @"Ten people at your last function contracted salmonella poisoning...
Client satisfaction drops";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyCustomerSatisfaction(-5, "Ten people at your a function contracted salmonella poisoning...");
        }

        public override Event[] GetUnlockedEvents()
        {
            return new Event[] { new HACCPInspection(), new SalmonellaLawsuit() };
        }
    }
}