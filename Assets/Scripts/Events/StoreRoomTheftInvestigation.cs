﻿namespace ExMan.Events
{
    public class StoreRoomTheftInvestigation : Event
    {
        public StoreRoomTheftInvestigation()
        {
            this.title = "Random Event!";
            this.description = @"Your investigation into the storeroom theft has made some of your staff unhappy...
Staff satisfaction drops";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(-2, "Your investigation into the storeroom theft has made some of your staff unhappy...");
        }
    }
}