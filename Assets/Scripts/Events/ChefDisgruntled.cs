﻿namespace ExMan.Events
{
    public class ChefDisgruntled : Event
    {
        public ChefDisgruntled()
        {
            this.title = "Random Event!";
            this.description = @"The chef is disgruntled about having to man multiple stations and is taking it out on other staff...
Staff satisfaction drops";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(-5, "The chef is disgruntled about having to man multiple stations and is taking it out on other staff...");
        }
    }
}