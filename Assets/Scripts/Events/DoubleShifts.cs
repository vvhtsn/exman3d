﻿namespace ExMan.Events
{
    public class DoubleShifts : Event
    {
        public DoubleShifts()
        {
            this.title = "Random Event!";
            this.description = @"Your chef has worked three double shifts in a row and has knocked the four caviar trays for tonight's function onto the floor...
You lose R10000";
        }

        public override bool IsAvailable()
        {
            return CompanyManager.GetCompany().selectedChef.name == "Thabang";
        }

        public override void OnComplete()
        {
            CompanyManager.SpendMoney(new Objects.Expense("Your chef has worked three double shifts in a row and has knocked the four caviar trays for tonight's function onto the floor...", 10000));
        }
    }
}