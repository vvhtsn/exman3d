﻿namespace ExMan.Events
{
    public class ButchersGuildAward : Event
    {
        public ButchersGuildAward()
        {
            this.title = "Random Event!";
            this.description = @"Your chef has been awarded the butcher's guild for remarkable venison dishes...
Staff satisfaction increases";
        }

        public override void OnComplete()
        {
            CompanyManager.ModifyStaffSatisfaction(3, "Your chef has been awarded the butcher's guild for remarkable venison dishes...");
        }
    }
}