﻿using UnityEngine;

public class LocomotionNoRoot : MonoBehaviour
{
    private Animator anim;
    private UnityEngine.AI.NavMeshAgent agent;

    public float acceleration = 2f;
    public float deceleration = 60f;
    public float closeEnoughMeters = 4f;

    private void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        agent.updatePosition = true;
        agent.updateRotation = true;
    }

    private void Update()
    {
        if (!AgentDone())
        {
            if (agent.hasPath)
                agent.acceleration = (agent.remainingDistance < closeEnoughMeters) ? deceleration : acceleration;

            //var angle = Vector3.Angle(agent.steeringTarget - transform.position, transform.forward);
            //if (angle > 60f)
            //{
            //    agent.velocity = Vector3.zero;
            //}

            Vector3 p = transform.InverseTransformDirection(agent.desiredVelocity);
            anim.SetFloat("Vertical", p.z, .1f, Time.deltaTime);
            anim.SetFloat("Horizontal", p.x, .1f, Time.deltaTime);
            anim.SetBool("IsMoving", true);
        }
        else
        {
            anim.SetFloat("Vertical", 0);
            anim.SetFloat("Horizontal", 0);
            anim.SetBool("IsMoving", false);
        }
    }

    protected bool AgentDone()
    {
        return !agent.pathPending && AgentStopping();
    }

    protected bool AgentStopping()
    {
        return agent.remainingDistance <= agent.stoppingDistance;
    }
}