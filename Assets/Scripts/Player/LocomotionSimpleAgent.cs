﻿using UnityEngine;

[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class LocomotionSimpleAgent : MonoBehaviour
{
    private Animator anim;
    private UnityEngine.AI.NavMeshAgent agent;

#if UNITY_EDITOR
    public bool manualControl;
    public bool move;

    [Range(-1f, 1f)]
    public float horizontal = 0f;

    [Range(-1f, 1f)]
    public float vertical = 0f;

#endif

    private void Start()
    {
        anim = GetComponent<Animator>();
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        agent.updateRotation = false;
        agent.updatePosition = false;
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (manualControl)
        {
            anim.SetFloat("Vertical", vertical);
            anim.SetFloat("Horizontal", horizontal);
            anim.SetBool("IsMoving", move);
        }
        else
        {
#endif
            if (!AgentDone())
            {
                Vector3 p = transform.InverseTransformDirection(agent.desiredVelocity);

                var angle = Vector3.Angle(agent.steeringTarget - transform.position, transform.forward);
                if (angle > 60f)
                {
                    anim.SetFloat("Horizontal", AngleDir(transform.forward, agent.steeringTarget - transform.position, Vector3.up), .1f, Time.deltaTime);
                }
                else
                {
                    anim.SetFloat("Horizontal", p.x, .1f, Time.deltaTime);
                }

                anim.SetFloat("Vertical", p.z, .1f, Time.deltaTime);

                agent.nextPosition = transform.position;
                anim.SetBool("IsMoving", true);
            }
            else
            {
                anim.SetFloat("Vertical", 0);
                anim.SetFloat("Horizontal", 0);
                anim.SetBool("IsMoving", false);
            }
#if UNITY_EDITOR
        }
#endif
    }

    private void OnAnimatorMove()
    {
        transform.rotation = anim.rootRotation;
        Vector3 pos = anim.rootPosition;
        pos.y = agent.nextPosition.y;
        anim.rootPosition = pos;
        transform.position = anim.rootPosition;
    }

    private int AngleDir(Vector3 forward, Vector3 targetDir, Vector3 up)
    {
        Vector3 cross = Vector3.Cross(forward, targetDir);
        float dir = Vector3.Dot(cross, up);

        if (dir > 0)
            return 1;
        else
            return -1;
    }

    protected bool AgentDone()
    {
        return !agent.pathPending && AgentStopping();
    }

    protected bool AgentStopping()
    {
        return agent.remainingDistance <= agent.stoppingDistance;
    }
}