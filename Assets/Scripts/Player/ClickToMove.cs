﻿using ExMan.Dialogue;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ExMan.Player
{
    [RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
    public class ClickToMove : MonoBehaviour
    {
        public LayerMask hitLayer;

        private RaycastHit hitInfo = new RaycastHit();
        private UnityEngine.AI.NavMeshAgent agent;

        private void Awake()
        {
            agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        }

        public void Update()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                #if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
                if (Input.GetMouseButtonDown(0) && !DialogueManager.instance.dialogPlaying)
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    if (Physics.Raycast(ray.origin, ray.direction, out hitInfo, 100f, hitLayer))
                    {
                        UnityEngine.AI.NavMeshHit hit;
                        if (UnityEngine.AI.NavMesh.SamplePosition(hitInfo.point, out hit, 1f, UnityEngine.AI.NavMesh.AllAreas))
                        {
                            agent.destination = hit.position;
                        }
                    }
                }

                #elif UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE
                if (Input.touchCount > 0)
                {

                    Ray ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
                    if (Physics.Raycast(ray.origin, ray.direction, out hitInfo, 100f, hitLayer))
                    {
                        UnityEngine.AI.NavMeshHit hit;
                        if (UnityEngine.AI.NavMesh.SamplePosition(hitInfo.point, out hit, 1f, UnityEngine.AI.NavMesh.AllAreas))
                        {
                            agent.destination = hit.position;
                        }
                    }
                }
                #endif
            }
        }
    }
}