﻿using UnityEngine;

public class RandomIdleAnim : StateMachineBehaviour
{
    public int amount;

    private int previous = -1;

    override public void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    {
        if (amount >= 2)
        {
            int rand;
            while ((rand = Random.Range(0, amount)) == previous)
            { }

            animator.SetInteger("RandomIdle", rand);
            previous = rand;
        }
        else
            animator.SetInteger("RandomIdle", 0);
    }
}