﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour
{

    public Transform seeker, target;
    AStarGrid grid;

    LineRenderer lineRenderer;
    public bool drawing = false;
    void Awake()
    {

        lineRenderer = gameObject.GetComponent<LineRenderer>();

        if (lineRenderer == null)
        {
            lineRenderer = gameObject.AddComponent<LineRenderer>();
            lineRenderer.widthMultiplier = 0.05f;
        }

        grid = GetComponent<AStarGrid>();
        StartCoroutine(Draw());
    }

    void DrawPath(List<AStarNode> path)
    {
        Vector3[] positions = new Vector3[path.Count];

        for (int i = 0; i < positions.Length; i++)
        {
            positions[i] = path[i].worldPosition;
            positions[i].y += 1;
        }
        lineRenderer.numPositions = positions.Length;
        lineRenderer.SetPositions(positions);

    }

    void FindPath(Vector3 startPos, Vector3 targetPos)
    {
        AStarNode startAStarNode = grid.NodeFromWorldPoint(startPos);
        AStarNode targetAStarNode = grid.NodeFromWorldPoint(targetPos);

        List<AStarNode> openSet = new List<AStarNode>();
        HashSet<AStarNode> closedSet = new HashSet<AStarNode>();
        openSet.Add(startAStarNode);

        while (openSet.Count > 0)
        {
            AStarNode node = openSet[0];
            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < node.fCost || openSet[i].fCost == node.fCost)
                {
                    if (openSet[i].hCost < node.hCost)
                        node = openSet[i];
                }
            }

            openSet.Remove(node);
            closedSet.Add(node);

            if (node == targetAStarNode)
            {
                RetracePath(startAStarNode, targetAStarNode);
                return;
            }

            foreach (AStarNode neighbour in grid.GetNeighbours(node))
            {
                if (!neighbour.walkable || closedSet.Contains(neighbour))
                {
                    continue;
                }

                int newCostToNeighbour = node.gCost + GetDistance(node, neighbour);
                if (newCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                {
                    neighbour.gCost = newCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, targetAStarNode);
                    neighbour.parent = node;

                    if (!openSet.Contains(neighbour))
                        openSet.Add(neighbour);
                }
            }
        }
    }

    void RetracePath(AStarNode startAStarNode, AStarNode endAStarNode)
    {
        List<AStarNode> path = new List<AStarNode>();
        AStarNode currentAStarNode = endAStarNode;

        while (currentAStarNode != startAStarNode)
        {
            path.Add(currentAStarNode);
            currentAStarNode = currentAStarNode.parent;
        }
        path.Reverse();

        grid.path = path;
        DrawPath(path);
    }

    int GetDistance(AStarNode nodeA, AStarNode nodeB)
    {
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        return 14 * dstX + 10 * (dstY - dstX);
    }

    // Sets waypoint line for player on specific target object
    public void SetWaypoint(string waypointName)
    {
        this.target = GameObject.Find(waypointName).transform;
        drawing = true;
    }

    public void ClearWaypoint()
    {
        this.target = null;
        lineRenderer.numPositions = 0;
        drawing = false;
    }

    IEnumerator Draw()
    {
        while (true)
        {
            if (drawing)
                FindPath(seeker.position, target.position);
            yield return new WaitForSeconds(1 / 30);
        }

     }
}