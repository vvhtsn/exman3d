﻿using UnityEngine;

namespace ExMan.Player
{
    [RequireComponent(typeof(Animator))]
    public class IKHandler : MonoBehaviour
    {
        private Animator animator;

        private bool ikActive;
        private Vector3 targetPosition;
        private Quaternion targetRotation;
        public bool lookAtTarget;

        public float lerpTime = 1f;
        private float elapsedTime;

        private Transform target;

        public AnimationCurve transitionCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

#if DEBUG

        [SerializeField]
        private Transform manualTarget;

#endif

        private void Awake()
        {
            animator = GetComponent<Animator>();
        }

        public void SetTarget(Transform target)
        {
            SetTarget(target, lookAtTarget);
        }

        public void SetTarget(Transform target, bool lookAtTarget)
        {
            ikActive = true;
            this.target = target;
            this.lookAtTarget = lookAtTarget;
        }

        public void SetTarget(Vector3 position, Quaternion rotation)
        {
            SetTarget(position, rotation, lookAtTarget);
        }

        public void SetTarget(Vector3 position, Quaternion rotation, bool lookAtTarget)
        {
            ikActive = true;
            targetPosition = position;
            targetRotation = rotation;
            target = transform;
            this.lookAtTarget = lookAtTarget;
        }

        public void Stop()
        {
            ikActive = false;
            target = null;
        }

        private void OnAnimatorIK()
        {
#if DEBUG
            if (manualTarget != null)
            {
                target = manualTarget;
                ikActive = true;
            }
            else if (target == null || target == transform)
                ikActive = false;
#endif

            if (target != null && target != transform)
            {
                targetPosition = target.position;
                targetRotation = target.rotation;
            }

            float weightLerp = transitionCurve.Evaluate(elapsedTime / lerpTime);

            if (lookAtTarget)
            {
                animator.SetLookAtWeight(weightLerp);
                animator.SetLookAtPosition(targetPosition);
            }

            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, weightLerp);
            animator.SetIKRotationWeight(AvatarIKGoal.RightHand, weightLerp);
            animator.SetIKPosition(AvatarIKGoal.RightHand, targetPosition);
            animator.SetIKRotation(AvatarIKGoal.RightHand, targetRotation);

            if (ikActive)
                elapsedTime = Mathf.Clamp01(elapsedTime + UnityEngine.Time.deltaTime);
            else
                elapsedTime = Mathf.Clamp01(elapsedTime - UnityEngine.Time.deltaTime);
        }
    }
}