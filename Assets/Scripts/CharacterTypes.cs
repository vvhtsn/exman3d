﻿namespace ExMan
{
    public enum CharacterTypes
    {
        None = 0,
        Manager = 1,
        Chef = 2,
        Waitress = 3,
        Staff = 4,
        Staff2 = 5,
        Staff3 = 6,
        Staff4 = 7,
        Client = 8,
        Other = 9,
    }
}