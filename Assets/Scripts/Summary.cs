﻿using ExMan.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Summary : MonoBehaviour
{
    public Text summaryContent;
    public ScrollRect scrollView;

    public static string summaryText = string.Empty;

    public static float previousCustomerSatisfaction;
    public static float previousStaffSatisfaction;
    public static float previousRevenue;

    private void OnEnable()
    {
        summaryContent.text = summaryText;
        scrollView.verticalNormalizedPosition = 0;
    }

    public static void RecordEntry(string title, float customerSatisfaction, float staffSatisfaction, float revenue, bool compareToPrevious = false)
    {
        // Adds text between entries if the entries have different values
        if (compareToPrevious)
        {
            if (customerSatisfaction != previousCustomerSatisfaction || staffSatisfaction != previousStaffSatisfaction || revenue != previousRevenue)
            {
                summaryText += "<b>Please refer to the activity log to better understand why your company status has changed between these scenes.</b>" + Environment.NewLine + Environment.NewLine;
            }
        }

        summaryText += "<b>" + title + "</b>" + Environment.NewLine;
        summaryText += "Customer satisfaction: " + customerSatisfaction + Environment.NewLine;
        summaryText += "Staff satisfaction: " + staffSatisfaction + Environment.NewLine;
        summaryText += "Revenue: " + TextFormatter.AsMoney(revenue) + Environment.NewLine;
        summaryText += Environment.NewLine;

        previousCustomerSatisfaction = customerSatisfaction;
        previousStaffSatisfaction = staffSatisfaction;
        previousRevenue = revenue;

        Debug.Log("Recorded summary: " + title);
    }
}
