﻿using ExMan.Objects;
using ExMan.Tasks;
using ExMan.Time;
using ExMan.Utils;
using System.Collections.Generic;
using UnityEngine;

namespace ExMan
{
    public class OrderQueue : MonoBehaviour
    {
        public static OrderQueue instance;
        public Queue<Order> orders = new Queue<Order>();
        private Order currentOrder;
        private List<Order> completedOrders = new List<Order>();
        private int totalOrders = 5;
        private int maxDays = 365;

        [SerializeField]
        private List<Order> availableOrders = new List<Order>();
        public bool debug;

        private void Awake()
        {
            instance = this;
        }

        private void Update()
        {
            if(debug)
            {
                debug = false;
            }
        }

        void CreateOrders(float times)
        {
            for (int i = 0; i < times; i++)
            {
                Order order = GetNextOrder();
                if (order == null) Debug.Log("Try number " + i + " unsuccessful");

            }
            Debug.Log("done");
        }
        private void Start()
        {
            Order firstOrder = new Order();
            firstOrder.company = "Best Bed Hotel";
            firstOrder.day = 30;
            firstOrder.distance = 20;
            firstOrder.selectedMenu = CompanyManager.GetCompany().menus[1];
            firstOrder.incomePerHead = 360;
            firstOrder.type = "5-year existence anniversary";
            firstOrder.mealCount = 250;
            firstOrder.upfrontPaymentPercentage = 1;
            orders.Enqueue(firstOrder);
        }

        public static Order GetNextOrder()
        {
            if (instance.orders.Count == 0)
                return GetRandomOrder();

            return instance.orders.Dequeue();
        }

        private static Order GetRandomOrder()
        {
            if (instance.availableOrders.Count > 0)
            {
                int randomRange = Random.Range(0, (instance.availableOrders.Count - 1));
                Order order = instance.availableOrders[randomRange];
                //Debug.Log("Available order count: " + (instance.availableOrders.Count - 1));
                //Debug.Log("Random range: " + randomRange);
                if (order != null)
                    return order;
                GetRandomOrder();
                Debug.Log("Random order null, trying again.");
            }
            return null;
        }

        public static void AcceptOrder(Order order)
        {
            if (instance.currentOrder != null)
            {
                Debug.LogError("Current order is not finished.");
                return;
            }

            if (instance.availableOrders.Contains(order))
                instance.availableOrders.Remove(order);

            var maxMeals = CompanyManager.GetCompany().selectedKitchen.maxMeals;
            if (order.guestCount < maxMeals)
            {
                RegisterOrder(order);
            }
            else if (order.guestCount < (maxMeals * 1.2f))
            {
                CompanyManager.ModifyStaffSatisfaction(-20);
                Debug.Log("ModStaffSatisfaction: -20");
                RegisterOrder(order);
            }
            else if (order.guestCount < (maxMeals * 1.5f))
            {
                CompanyManager.ModifyStaffSatisfaction(-30);
                CompanyManager.ModifyCustomerSatisfaction(-10);
                Debug.Log("ModStaffSatisfaction: -30");
                RegisterOrder(order);
            }
            else
            {
                Logger.Log(string.Format(@"You have accepted an order for {0} that is beyond the capacity of your
kitchen and you had to cancel the order.", order.company));
                Transform canceledPanel = CustomFind.FindComponentsWithTag<Transform>(Tags.orderCanceled, true);
                canceledPanel.gameObject.SetActive(true);
                CompanyManager.ModifyCustomerSatisfaction(-50);

                TaskWrapper delay = new TaskWrapper(new Tasks.WaitForSeconds(10), false);
                TaskWrapper startOrder = new TaskWrapper(new StartOrder(false), false);
                TimeEvent t = new TimeEvent("Random Order", new TaskWrapper(new ChildrenDone(), false, delay, startOrder), false);
                TimeManager.RegisterTimeEvent(TimeManager.currentDay + 4, t);
            }
        }

        public static void RegisterOrder(Order order)
        {
            instance.currentOrder = order;

            if (order.upfrontPaymentPercentage != 0)
            {
                CompanyManager.ReceiveMoney(order.mealCount * order.incomePerHead * order.upfrontPaymentPercentage, string.Format("Upfront payment of the order from {0}", order.company));
            }
            else
            {
                ExMan.Logger.Log(string.Format("Accepted order of {0} for {1}.", order.company, order.type));
            }

            // Delay used for both, order completion and random event
            TaskWrapper delay = new TaskWrapper(new Tasks.WaitForSeconds(10), false);

            //TimeEvent t = new TimeEvent("Order Completed", new TaskWrapper(new CompleteOrder(Tags.simpleTextPanel, order), false), true);
            TaskWrapper completeOrder = new TaskWrapper(new CompleteOrder(Tags.simpleTextPanel, order), false);
            TaskWrapper newOrderTrigger = new TaskWrapper(new ActivateTriggerArea(Tags.telephoneTrigger, notificationTitle: "Your telephone is ringing", notificationContent: "You should answer the phone in your office."), "OfficeWaypoint", false);
            TaskWrapper startOrder = new TaskWrapper(new StartOrder(false), false);
            TaskWrapper completeAndCreateOrder = new TaskWrapper(new ChildrenDone(), false, completeOrder, newOrderTrigger, startOrder);
            TimeEvent t = new TimeEvent("Order Completed", completeAndCreateOrder, true);
            TimeManager.RegisterTimeEvent(TimeManager.currentDay + order.day, t);

            TaskWrapper randomEvent = new TaskWrapper(new EnableUIElement<UI.RandomEvent>(Tags.randomEvent), false);
            TaskWrapper randomEventWithDelay = new TaskWrapper(new ChildrenDone(), false, delay, randomEvent);

            TaskWrapper nextScene = new TaskWrapper(new ChildrenDone(), false, SceneQueue.GetNextScene(), randomEventWithDelay);
            if (nextScene != null)
            {
                TimeEvent scene = new TimeEvent(nextScene.name, nextScene, false);
                TimeManager.RegisterTimeEvent((int)Mathf.Lerp(TimeManager.currentDay, TimeManager.currentDay + order.day, .5f), scene);
            }
            order.day = TimeManager.currentDay + order.day;
        }

        public static void CompleteOrder()
        {
            if (instance.currentOrder == null)
            {
                Debug.LogError("Cant complete because there is none.");
                return;
            }
            Order order = GetCurrentOrder();
            CompanyManager.SpendMoney(new Expense("Gas cost", (order.distance * 2) * (Mathf.CeilToInt(order.distance / CompanyManager.GetCompany().selectedVehicle.maxMealsPerDelivery)) * CompanyManager.GetCompany().selectedVehicle.costPerKm));
            CompanyManager.SpendMoney(new Expense("Menu cost", order.selectedMenu.cost * order.mealCount));

            if (1 - order.upfrontPaymentPercentage != 0)
            {
                CompanyManager.ReceiveMoney(order.mealCount * order.incomePerHead * (1 - order.upfrontPaymentPercentage), string.Format("Payment for the order for {0}", order.company));
            }

            instance.completedOrders.Add(instance.currentOrder);
            instance.currentOrder = null;

            //TimeEvent t = new TimeEvent("Random order", new TaskWrapper(new StartOrder(false), false), false);
            //TimeManager.RegisterTimeEvent(TimeManager.currentDay + 4, t);
        }

        public static Order GetCurrentOrder()
        {
            return instance.currentOrder;
        }

        public static void SetCurrentOrder(Order order)
        {
            instance.currentOrder = order;
            // Delay used for both, order completion and random event
            TaskWrapper delay = new TaskWrapper(new Tasks.WaitForSeconds(10), false);

            TaskWrapper completeOrder = new TaskWrapper(new CompleteOrder(Tags.simpleTextPanel, order), false);
            TaskWrapper newOrderTrigger = new TaskWrapper(new ActivateTriggerArea(Tags.telephoneTrigger, notificationTitle: "Your telephone is ringing", notificationContent: "You should answer the phone in your office."), "OfficeWaypoint", false);
            TaskWrapper startOrder = new TaskWrapper(new StartOrder(false), false);
            TaskWrapper completeAndCreateOrder = new TaskWrapper(new ChildrenDone(), false, completeOrder, newOrderTrigger, startOrder);
            TimeEvent t = new TimeEvent("Order Completed", completeAndCreateOrder, true);
            TimeManager.RegisterTimeEvent(order.day, t);

            TaskWrapper randomEvent = new TaskWrapper(new EnableUIElement<UI.RandomEvent>(Tags.randomEvent), false);
            TaskWrapper randomEventWithDelay = new TaskWrapper(new ChildrenDone(), false, delay, randomEvent);

            TaskWrapper nextScene = new TaskWrapper(new ChildrenDone(), false, SceneQueue.GetNextScene(), randomEventWithDelay);
            if (nextScene != null)
            {
                TimeEvent scene = new TimeEvent(nextScene.name, nextScene, false);
                TimeManager.RegisterTimeEvent((int)Mathf.Lerp(TimeManager.currentDay, order.day, .5f), scene);
            }
        }

        public static int GetAvailableDayForOrder()
        {
            int daysLeft = instance.maxDays - TimeManager.currentDay;
            if (daysLeft > 0)
            {
                int ordersLeft = instance.totalOrders - instance.completedOrders.Count;
                int[] orderLengths = MathUtils.RandomSumArray(ordersLeft, daysLeft, 10);
                return orderLengths[0];
            }
            return -1;
        }
    }
}