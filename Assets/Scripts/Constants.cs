﻿using DG.Tweening;

namespace ExMan
{
    public class Constants
    {
        public const float uiTweenTime = .3f;
        public const Ease uiEaseStartType = Ease.OutSine;
        public const Ease uiEaseEndType = Ease.InSine;

        public const string mixerMasterVolume = "MasterVolume";
        public const string mixerMusicVolume = "MusicVolume";
        public const string mixerDialogueVolume = "DialogueVolume";
    }
}