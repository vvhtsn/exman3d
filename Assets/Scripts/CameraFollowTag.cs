﻿using UnityEngine;

public class CameraFollowTag : MonoBehaviour
{
    public float speed = 1;
    public Vector3 offset;
    public GameObject target;

    public void Init(GameObject target)
    {
        this.target = target;
        transform.position = target.transform.position + offset;
        transform.LookAt(target.transform.position);
    }

    private void LateUpdate()
    {
        if (target != null)
        {
            Vector3 camPos = transform.position;
            Vector3 targetPos = Vector3.Lerp(camPos, target.transform.position + offset, Time.deltaTime * speed);
            transform.position = targetPos;
        }
    }
}