﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ExMan.Tasks;
using ExMan.ScriptableObjects;
using ExMan.Dialogue;

namespace ExMan
{
    public class DialogTester : MonoBehaviour {

        private static DialogTester instance;

        public GameObject buttonParent;
        public GameObject buttonPrefab;
        public Character manager;
        public Chef chef;
        public Character waitress;
        public Character client;

        private void Start()
        {
            instance = this;

            SetupCompany();
            GenerateDialogSelection();
        }

        private void SetupCompany()
        {
            CompanyManager.ReceiveMoney(100000);
            CompanyManager.HireManager(manager);
            CompanyManager.HireChef(chef);
            DialogueManager.characterDefinitions.chef.sceneObject.SetActive(false);
            DialogueManager.characterDefinitions.waitress = waitress;
            DialogueManager.characterDefinitions.client = client;
            CompanyManager.GetCompany().name = "Test Company";
        }

        private void GenerateDialogSelection()
        {
            Object[] dialogues = Resources.LoadAll("Dialogue/");
            foreach(Object dialogue in dialogues)
            {
                Button button = Instantiate(buttonPrefab).GetComponent<Button>();
                button.transform.SetParent(buttonParent.transform, false);
                button.transform.GetChild(0).GetComponent<Text>().text = dialogue.name;
                button.onClick.AddListener(() =>
                {
                    StartDialogue(dialogue.name);
                });
            }
        }

        public void StartDialogue(string name)
        {
            TaskWrapper dialog = new TaskWrapper(new StartDialogue(name), true);
            dialog.StartTask(null);
        }
    }
}
