﻿using ExMan.Components;
using ExMan.ScriptableObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace ExMan.Dialogue
{
    public class DialogueManager : MonoBehaviour
    {
        public static DialogueManager instance;

        [SerializeField]
        private UIOptions uiOptions;

        public static CharacterDefs characterDefinitions = new CharacterDefs();

        private int clickedOption = 0;
        private bool clickedChoice = false;
        private Action onDoneCallback;

        private float totalCustomerSatisfactionModifier = 0;
        private float totalStaffSatisfactionModifier = 0;
        private float totalRevenueModifier = 0;

        private List<Feedback> feedbacks = new List<Feedback>();

        public DialogueNodeCanvas dialog;
        public string dialogName;
        public bool dialogPlaying = false;

        public Character waitress;
        public Character client;

        public BaseDialogueNode currentNode { get; private set; }

        public AudioSource audioSource;

        public Dictionary<string, UnityEvent> events = new Dictionary<string, UnityEvent>();

        public string gameOverReason = "";

        private static GameObject currentMovingCharacter;

        public static GameObject CurrentMovingCharacter { get; set; }

        private void Awake()
        {
            instance = this;
            for (int i = 0; i < uiOptions.choicesButtons.Length; i++)
            {
                int index = i;
                uiOptions.choicesButtons[i].onClick.AddListener(() => ClickedOption(index));
            }
            uiOptions.continueButton.onClick.AddListener(ClickedContinueButton);

            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
            }

            uiOptions.mainPanel.SetActive(false);
            characterDefinitions.client = client;
            characterDefinitions.waitress = waitress;
            characterDefinitions.waitress.sceneObject = GameObject.Find("Characters/jackie");
        }

        private void FixedUpdate()
        {
            if (Input.GetKeyDown(KeyCode.F10) && Application.isEditor && currentNode != null)
            {
                StopCurrentDialogue();
            }
        }

        /// <summary>
        /// Starts the dialogue
        /// </summary>
        /// <param name="dialogue"></param>
        /// <param name="dialogueName"></param>
        /// <param name="doneCallback"></param>
        public static void StartDialogue(DialogueNodeCanvas dialogue, string dialogueName, Action doneCallback)
        {
            instance.StartTheDialogue(dialogue, dialogueName, doneCallback);
        }

        /// <summary>
        /// Starts the dialogue while moving the character to the position
        /// </summary>
        /// <param name="dialogue"></param>
        /// <param name="dialogueName"></param>
        /// <param name="doneCallback"></param>
        /// <param name="character"></param>
        /// <param name="positions"></param>
        /// <param name="delay">delay between multiple character movements</param>
        public static void StartDialogue(DialogueNodeCanvas dialogue, string dialogueName, Action doneCallback, GameObject[] characters, Vector3 position, float delay)
        {
            instance.dialog = dialogue;
            instance.dialogName = dialogueName;
            instance.onDoneCallback = doneCallback;
            instance.StartCoroutine(instance.MoveCharacter(characters, position, delay));
        }

        public IEnumerator MoveCharacter(GameObject[] characters, Vector3 position, float delay)
        {
            for (int i = 0; i < characters.Length; i++)
            {
                RandomWaypointMove moveController = characters[i].GetComponent<RandomWaypointMove>();
                moveController.onReachDestination += MoveController_onReachDestination;
                moveController.GoTo(position);
                currentMovingCharacter = characters[i];
                yield return new WaitForSeconds(delay);
            }
        }

        private static void MoveController_onReachDestination()
        {
            if (!instance.dialogPlaying)
            {
                instance.StartTheDialogue(instance.dialog, instance.dialogName, instance.onDoneCallback);
            }
        }

        public void StartTheDialogue(DialogueNodeCanvas dialogue, string dialogueName, Action doneCallback)
        {
            if (instance == null)
            {
                throw new InvalidOperationException("Failed to start dialogue, there is no DialogueManager in the scene");
            }

            instance.dialogName = dialogueName;
            instance.onDoneCallback = doneCallback;
            instance.StartDialogue(dialogue);
        }


        public static void StopCurrentDialogue()
        {
            if (instance == null)
            {
                throw new InvalidOperationException("Failed to stop dialogue, there is no DialogueManager in the scene");
            }

            instance.DialogueDone();
        }

        private void StartDialogue(DialogueNodeCanvas dialogue)
        {
            if (dialogue == null)
            {
                throw new InvalidOperationException("There is no dialogue canvas assigned!");
            }
            if (uiOptions != null && uiOptions.feedbackPanel != null)
            {
                uiOptions.feedbackPanel.SetActive(false);
                foreach (Feedback feedback in feedbacks)
                {
                    Destroy(feedback.gameObject);
                }
                feedbacks.Clear();
                uiOptions.mainPanel.SetActive(true);
                uiOptions.character1.text = characterDefinitions.manager.name;
                uiOptions.character2.text = "";
                uiOptions.character1Image.color = new Color(0, 0, 0, 0);
                uiOptions.character2Image.color = new Color(0, 0, 0, 0);
                totalCustomerSatisfactionModifier = 0;
                totalStaffSatisfactionModifier = 0;
                totalRevenueModifier = 0;
                dialogPlaying = true;
                Show(dialogue.startNode);
                Debug.Log("Playing dialogue " + dialogue.name);
            }
        }

        private void Show(BaseDialogueNode node)
        {
            currentNode = node;
            clickedChoice = false;

            if (node.modifierMonies != 0)
            {
                totalRevenueModifier += node.modifierMonies;
                CompanyManager.ReceiveMoney(node.modifierMonies);
            }
            if (node.modifierCustomerSatisfaction != 0)
            {
                totalCustomerSatisfactionModifier += node.modifierCustomerSatisfaction;
                CompanyManager.ModifyCustomerSatisfaction(node.modifierCustomerSatisfaction);
            }
            if (node.modifierStaffSatisfaction != 0)
            {
                totalStaffSatisfactionModifier += node.modifierStaffSatisfaction;
                CompanyManager.ModifyStaffSatisfaction(node.modifierStaffSatisfaction);
            }

            if (node is DialogueExitNode)
            {
                gameOverReason = ((DialogueExitNode)node).gameOverReason;
            }

            if (!string.IsNullOrEmpty(currentNode.eventName))
            {
                if (events.ContainsKey(currentNode.eventName))
                {
                    events[currentNode.eventName].Invoke();
                }
            }

            if (node is DialogueExitNode)
            {
                gameOverReason = ((DialogueExitNode)node).gameOverReason;
                if (string.IsNullOrEmpty(node.talkingText))
                {
                    DialogueDone();
                }
            }

            if (string.IsNullOrEmpty(node.talkingText))
            {
                if (node is DialogueMultiOptionsNode)
                {
                    ShowOptions(node as DialogueMultiOptionsNode);
                }
            }
            else
            {
                ShowText(node.talkingText, node.talkingClip);
            }
            uiOptions.character1.text = characterDefinitions.manager.name;
            if (node.talkingCharacter == CharacterTypes.Manager) // Manager talking
            {
                if (node.characterImage != null)
                {
                    uiOptions.character1Image.texture = node.characterImage;
                    uiOptions.character1Image.color = Color.white;
                }
            }
            else // Other person talking
            {
                Character c = characterDefinitions.GetCharacter(node.talkingCharacter);
                if (c != null)
                {
                    uiOptions.character2.text = c.name;
                    if (node.characterImage != null)
                    {
                        uiOptions.character2Image.texture = node.characterImage;
                        uiOptions.character2Image.color = Color.white;
                    }
                    uiOptions.character2.gameObject.SetActive(true);
                    uiOptions.character2Image.gameObject.SetActive(true);
                }
                else
                {
                    uiOptions.character2.gameObject.SetActive(false);
                    uiOptions.character2Image.gameObject.SetActive(false);
                }
            }
        }

        private void ShowText(string text, AudioClip clip)
        {
            uiOptions.choicesPanel.SetActive(false);
            uiOptions.textPanel.SetActive(true);
            uiOptions.talkingText.Write(text);
            PlayClip(clip);
        }

        private void PlayClip(AudioClip clip)
        {
            audioSource.clip = clip;
            audioSource.Play();
        }

        private void StopClip()
        {
            audioSource.Stop();
        }

        private void ClickedOption(int index)
        {
            Debug.Log("Clicked dialogue option " + index);
            DialogueMultiOptionsNode n = currentNode as DialogueMultiOptionsNode;

            clickedChoice = true;
            clickedOption = index;

            if (!string.IsNullOrEmpty(n.options[index].clickedEvent))
            {
                if (events.ContainsKey(n.options[index].clickedEvent))
                {
                    events[n.options[index].clickedEvent].Invoke();
                }
            }

            if (n != null)
            {
                RecordFeedback(n, index);
                if (n.options[index].optionImage != null)
                {
                    uiOptions.character1Image.texture = n.options[index].optionImage;
                    uiOptions.character1Image.color = Color.white;
                }
                ShowText(n.options[index].optionText, n.options[index].optionClip);
            }
            else
            {
                Debug.LogError("ClickedOption was called but the currentNode is not a multi options node!");
            }
        }

        private void ClickedContinueButton()
        {
            StopClip();

            if (currentNode is DialogueMultiOptionsNode)
            {
                var node = currentNode as DialogueMultiOptionsNode;

                if (clickedChoice)
                {
                    Show(node.Outputs[clickedOption].connection.body as BaseDialogueNode);
                }
                else
                {
                    ShowOptions(node);
                }
            }
            else if (currentNode is DialogueExitNode)
            {
                DialogueDone();
            }
            else
            {
                Show(currentNode.Outputs[0].connection.body as BaseDialogueNode);
            }
        }

        private void ShowOptions(DialogueMultiOptionsNode node)
        {
            uiOptions.textPanel.SetActive(false);
            uiOptions.choicesPanel.SetActive(true);

            var options = node.options;
            for (int i = 0; i < uiOptions.choicesButtons.Length; i++)
            {
                if (options.Count >= i && !string.IsNullOrEmpty(options[i].optionDisplay))
                {
                    uiOptions.choicesButtons[i].gameObject.SetActive(true);
                    uiOptions.choicesButtons[i].GetComponentInChildren<Text>().text = options[i].optionDisplay;
                }
                else
                {
                    uiOptions.choicesButtons[i].gameObject.SetActive(false);
                }
            }
        }

        private void RecordFeedback(DialogueMultiOptionsNode node, int index)
        {
            Feedback feedback = Instantiate(uiOptions.feedbackPrefab).GetComponent<Feedback>();
            feedback.transform.SetParent(uiOptions.feedbackContainer.transform, false);
            feedback.otherCharacter = node.talkingCharacter.ToString();
            feedback.Question = node.talkingText;
            feedback.Answer = node.options[index].optionText;
            BaseDialogueNode resultNode = node.Outputs[index].connection.body as BaseDialogueNode;
            string effects = "";
            if (resultNode.modifierCustomerSatisfaction > 0)
            {
                effects += "+";
            }
            effects += resultNode.modifierCustomerSatisfaction + " Customer Satisfaction" + Environment.NewLine;
            if (resultNode.modifierCustomerSatisfaction != 0)
            {
                effects += "Reason: " + resultNode.customerSatisfactionReason + Environment.NewLine + Environment.NewLine;
            }
            if (resultNode.modifierStaffSatisfaction > 0)
            {
                effects += "+";
            }
            effects += resultNode.modifierStaffSatisfaction + " Staff Satisfaction" + Environment.NewLine;
            if (resultNode.modifierStaffSatisfaction != 0)
            {
                effects += "Reason: " + resultNode.staffSatisfactionReason + Environment.NewLine + Environment.NewLine;
            }
            if (resultNode.modifierMonies > 0)
            {
                effects += "+";
            }
            effects += resultNode.modifierMonies + " Revenue" + Environment.NewLine;
            if (resultNode.modifierMonies != 0)
            {
                effects += "Reason: " + resultNode.moneyReason;
            }
            feedback.Effect = effects;
            feedbacks.Add(feedback);
        }

        private void DialogueDone()
        {
            Debug.Log("Dialogue is done");

            StopClip();

            if (totalCustomerSatisfactionModifier != 0 || totalStaffSatisfactionModifier != 0 || totalRevenueModifier != 0)
            {
                // Format record title
                string recordName = dialogName;
                string[] parts = recordName.Split('_');
                if (parts[parts.Length - 1] == CompanyManager.GetCompany().selectedChef.name)
                {
                    recordName = recordName.Substring(0, recordName.LastIndexOf('_'));
                }
                recordName = recordName.Replace('_', ' ');

                Logger.Log(recordName + ": CS: " + totalCustomerSatisfactionModifier + " ; " + "SS: " + totalStaffSatisfactionModifier + " ; " +
                    "Rev: " + totalRevenueModifier + ". Please refer to text files for detailed dialog feedback.");

                Summary.RecordEntry("Changes caused in " + recordName, totalCustomerSatisfactionModifier, totalStaffSatisfactionModifier, totalRevenueModifier);
            }
            uiOptions.talkingText.text = string.Empty;
            uiOptions.mainPanel.SetActive(false);
            if (feedbacks.Count > 0)
            {
                uiOptions.feedbackPanel.SetActive(true);
                SaveFeedBack();
            }
            dialogPlaying = false;
            currentNode = null;
            if (onDoneCallback != null)
            {
                onDoneCallback.Invoke();
            }
        }

        private void SaveFeedBack()
        {
            Directory.CreateDirectory(Application.dataPath + "/Feedbacks");
            for (int i = 1; i < 1000; i++)
            {
                if (!File.Exists(Application.dataPath + "/Feedbacks/" + dialogName + "_" + i.ToString("000") + ".txt"))
                {
                    using (StreamWriter sw = new StreamWriter(Application.dataPath + "/Feedbacks/" + dialogName + "_" + i.ToString("000") + ".txt"))
                    {
                        sw.WriteLine(dialogName + " Feedback:");
                        sw.WriteLine();
                        foreach (Feedback feedback in feedbacks)
                        {
                            sw.WriteLine(feedback.ToString());
                            sw.WriteLine("---------------------------------------" + Environment.NewLine);
                        }

                    }
                    break;
                }
            }
        }

        public void OpenFeedbackFolder()
        {
            System.Diagnostics.Process.Start(Application.dataPath + "/Feedbacks");
        }

        private string ParseString(string s)
        {
            s = s.Replace("{chefName}", characterDefinitions.chef.name);
            s = s.Replace("{managerName}", characterDefinitions.manager.name);
            s = s.Replace("{waitressName}", characterDefinitions.waitress.name);
            s = s.Replace("{staffName}", characterDefinitions.staff.name);
            s = s.Replace("{staff2Name}", characterDefinitions.staff2.name);
            s = s.Replace("{staff3Name}", characterDefinitions.staff3.name);
            s = s.Replace("{staff4Name}", characterDefinitions.staff4.name);
            return s.Trim();
        }

        public static void RegisterEvent(string name, UnityAction action)
        {
            if (!instance.events.ContainsKey(name))
            {
                instance.events.Add(name, new UnityEvent());
            }

            instance.events[name].AddListener(action);
        }

        public static void UnregisterEvent(string name, UnityAction action)
        {
            if (instance.events.ContainsKey(name))
            {
                instance.events[name].RemoveListener(action);
            }
            else
            {
                Debug.LogWarning("There is no event registered with the name '" + name + "' in the DialogueManager");
            }
        }

        public static void ClearEvents(string name)
        {
            if (instance.events.ContainsKey(name))
            {
                instance.events.Remove(name);
            }
            else
            {
                Debug.LogWarning("There is no event registered with the name '" + name + "' in the DialogueManager");
            }
        }

     

        [Serializable]
        public class UIOptions
        {
            public GameObject mainPanel;
            public GameObject textPanel;
            public GameObject choicesPanel;
            public GameObject feedbackPanel;
            public GameObject feedbackContainer;
            public GameObject feedbackPrefab;
            public TypeWriter talkingText;

            public Text character1;
            public Text character2;
            public RawImage character1Image;
            public RawImage character2Image;

            [Tooltip("It will automatically add the needed listeners")]
            public Button[] choicesButtons;

            public Button continueButton;
        }

        [Serializable]
        public class CharacterDefs
        {
            public Character manager;
            public Character chef;
            public Character waitress;
            public Character staff;
            public Character staff2;
            public Character staff3;
            public Character staff4;
            public Character client;
            public Character other;

            public Character GetCharacter(CharacterTypes c)
            {
                switch (c)
                {
                    case CharacterTypes.Manager:
                        return manager;

                    case CharacterTypes.Chef:
                        return chef;

                    case CharacterTypes.Waitress:
                        return waitress;

                    case CharacterTypes.Staff:
                        return staff;

                    case CharacterTypes.Staff2:
                        return staff2;

                    case CharacterTypes.Staff3:
                        return staff3;

                    case CharacterTypes.Staff4:
                        return staff4;

                    case CharacterTypes.Client:
                        return client;

                    case CharacterTypes.Other:
                        return other;

                    case CharacterTypes.None:
                    default:
                        return null;
                }
            }
        }
    }
}