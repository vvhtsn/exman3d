﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace NodeEditorFramework
{
    /// <summary>
    /// NodeInput accepts one connection to a NodeOutput by default
    /// </summary>
    public class NodeInput : NodeKnob, ISerializationCallbackReceiver
    {
        // NodeKnob Members
        protected override NodeSide defaultSide { get { return NodeSide.Left; } }

        // NodeInput Members
        [SerializeField]
        private NodeOutput connection;
        public List<NodeOutput> connections = new List<NodeOutput>();

        [FormerlySerializedAs("type")]
        public string typeID;

        private TypeData _typeData;
        internal TypeData typeData { get { CheckType(); return _typeData; } }
        // Multiple connections
        //		public List<NodeOutput> connections;

        [System.NonSerialized]
        private object value = null;

        #region General

        /// <summary>
        /// Creates a new NodeInput in NodeBody of specified type
        /// </summary>
        public static NodeInput Create(Node nodeBody, string inputName, string inputType, bool isRuntime)
        {
            return Create(nodeBody, inputName, inputType, NodeSide.Left, 20, isRuntime);
        }

        /// <summary>
        /// Creates a new NodeInput in NodeBody of specified type at the specified NodeSide
        /// </summary>
        public static NodeInput Create(Node nodeBody, string inputName, string inputType, NodeSide nodeSide, bool isRuntime)
        {
            return Create(nodeBody, inputName, inputType, nodeSide, 20, isRuntime);
        }

        /// <summary>
        /// Creates a new NodeInput in NodeBody of specified type at the specified NodeSide and position
        /// </summary>
        public static NodeInput Create(Node nodeBody, string inputName, string inputType, NodeSide nodeSide, float sidePosition, bool isRuntime)
        {
            NodeInput input = CreateInstance<NodeInput>();
            input.typeID = inputType;
            input.InitBase(nodeBody, nodeSide, sidePosition, inputName, isRuntime);
            nodeBody.Inputs.Add(input);
            return input;
        }

        public override void Delete()
        {
            RemoveConnection();
            body.Inputs.Remove(this);
            base.Delete();
        }

        #endregion General

        #region Additional Serialization

        protected internal override void CopyScriptableObjects(System.Func<ScriptableObject, ScriptableObject> replaceSerializableObject)
        {

            //connection = replaceSerializableObject.Invoke(connection) as NodeOutput;
            // Multiple connections
            for (int conCnt = 0; conCnt < connections.Count; conCnt++)
                connections[conCnt] = replaceSerializableObject.Invoke(connections[conCnt]) as NodeOutput;
        }

        #endregion Additional Serialization

        #region KnobType

        protected override void ReloadTexture()
        {
            CheckType();
            knobTexture = typeData.InKnobTex;
        }

        private void CheckType()
        {
            if (_typeData == null || !_typeData.isValid())
                _typeData = ConnectionTypes.GetTypeData(typeID);
            if (_typeData == null || !_typeData.isValid())
            {
                ConnectionTypes.FetchTypes();
                _typeData = ConnectionTypes.GetTypeData(typeID);
                if (_typeData == null || !_typeData.isValid())
                    throw new UnityException("Could not find type " + typeID + "!");
            }
        }

        #endregion KnobType

        #region Value

        public bool IsValueNull { get { return value == null; } }
        //public bool IsValueNull { get { return connection != null ? connection.IsValueNull : true; } }

        /// <summary>
        /// Gets the value of the connection anonymously. Not advised as it may lead to unwanted behaviour!
        /// </summary>
        public object GetValue()
        {
            return value;
            //return connection != null ? connection.GetValue() : null;
        }

        /// <summary>
        /// Gets the value of the connection or null. If possible, use strongly typed version instead.
        /// </summary>
        public object GetValue(Type type)
        {
            //return connection != null ? connection.GetValue(type) : null;
            if (type == null)
                throw new UnityException("Trying to get value of " + name + " with null type!");
            CheckType();
            if (type.IsAssignableFrom(typeData.Type))
                return value;
            Debug.LogError("Trying to GetValue<" + type.FullName + "> for Output Type: " + typeData.Type.FullName);
            return null;
        }

        /// <summary>
        /// Sets the value of the connection if the type matches. If possible, use strongly typed version instead.
        /// </summary>
        public void SetValue(object Value)
        {
            //if (connection != null)
            //    connection.SetValue(Value);
            CheckType();
            if (Value == null || typeData.Type.IsAssignableFrom(Value.GetType()))
                value = Value;
            else
                Debug.LogError("Trying to SetValue of type " + Value.GetType().FullName + " for Output Type: " + typeData.Type.FullName);
        }

        /// <summary>
        /// Gets the value of the connection or the default value
        /// </summary>
        public T GetValue<T>()
        {
            //return connection != null ? connection.GetValue<T>() : NodeOutput.GetDefault<T>();
            CheckType();
            if (typeof(T).IsAssignableFrom(typeData.Type))
                return (T)(value ?? (value = GetDefault<T>()));
            Debug.LogError("Trying to GetValue<" + typeof(T).FullName + "> for Output Type: " + typeData.Type.FullName);
            return GetDefault<T>();
        }

        /// <summary>
        /// Sets the value of the connection if the type matches
        /// </summary>
        public void SetValue<T>(T Value)
        {
            //if (connection != null)
            //    connection.SetValue<T>(Value);
            CheckType();
            if (typeData.Type.IsAssignableFrom(typeof(T)))
                value = Value;
            else
                Debug.LogError("Trying to SetValue<" + typeof(T).FullName + "> for Output Type: " + typeData.Type.FullName);
        }

        /// <summary>
        /// Returns the default value of type when a default constructor is existant or type is a value type, else null
        /// </summary>
        public static T GetDefault<T>()
        {
            // Try to create using an empty constructor if existant
            if (typeof(T).GetConstructor(System.Type.EmptyTypes) != null)
                return System.Activator.CreateInstance<T>();
            // Else try to get default. Returns null only on reference types
            return default(T);
        }

        #endregion Value

        #region Connecting Utility

        /// <summary>
        /// Try to connect the passed NodeOutput to this NodeInput. Returns success / failure
        /// </summary>
        public bool TryApplyConnection(NodeOutput output)
        {
            if (CanApplyConnection(output))
            { // It can connect (type is equals, it does not cause recursion, ...)
                ApplyConnection(output);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Check if the passed NodeOutput can be connected to this NodeInput
        /// </summary>
        public bool CanApplyConnection(NodeOutput output)
        {
            if (output == null || body == output.body || connections.Contains(output) || !typeData.Type.IsAssignableFrom(output.typeData.Type))
            {
                //Debug.LogError("Cannot assign " + typeData.Type.ToString() + " to " + output.typeData.Type.ToString());
                return false;
            }

            if (output.body.isChildOf(body))
            { // Recursive
                if (!output.body.allowsLoopRecursion(body))
                {
                    // TODO: Generic Notification
                    Debug.LogWarning("Cannot apply connection: Recursion detected!");
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Applies a connection between the passed NodeOutput and this NodeInput. 'CanApplyConnection' has to be checked before to avoid interferences!
        /// </summary>
        public void ApplyConnection(NodeOutput output)
        {
            if (output == null)
                return;

            //if (connections != null)
            //{
            //    NodeEditorCallbacks.IssueOnRemoveConnection(this);

            //    foreach (var item in connections)
            //        item.connections.Remove(this);
            //    //connection.connections.Remove(this);
            //}
            connections.Add(output);
            //output.connections.Add(this);
            output.connection = this;

            if (!output.body.calculated)
                NodeEditor.RecalculateFrom(output.body);
            else
                NodeEditor.RecalculateFrom(body);

            output.body.OnAddOutputConnection(output);
            body.OnAddInputConnection(this);
            NodeEditorCallbacks.IssueOnAddConnection(this);
        }

        /// <summary>
        /// Removes the connection from this NodeInput
        /// </summary>
        public void RemoveConnection()
        {
            if (connections == null)
                return;

            NodeEditorCallbacks.IssueOnRemoveConnection(this);
            foreach (var item in connections)
            {
                if (item.connection == this)
                    item.connection = null;
                //item.connections.Remove(this);
            }

            connections = new List<NodeEditorFramework.NodeOutput>();

            NodeEditor.RecalculateFrom(body);
        }

        #endregion Connecting Utility

        #region Utility

        public override Node GetNodeAcrossConnection()
        {
            //return connection != null ? connection.body : null;
            return connections.Count > 0 ? connections[0].body : null;
        }

        public void OnBeforeSerialize()
        {
            //throw new NotImplementedException();
        }

        public void OnAfterDeserialize()
        {
            if (connection != null)
                connections.Add(connection);
            connection = null;
        }

        #endregion Utility
    }
}