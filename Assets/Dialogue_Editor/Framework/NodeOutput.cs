﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace NodeEditorFramework
{
    /// <summary>
    /// Node output accepts multiple connections to NodeInputs by default
    /// </summary>
    public class NodeOutput : NodeKnob, ISerializationCallbackReceiver
    {
        // NodeKnob Members
        protected override NodeSide defaultSide { get { return NodeSide.Right; } }

        private static GUIStyle _defaultStyle;
        protected override GUIStyle defaultLabelStyle { get { if (_defaultStyle == null) { _defaultStyle = new GUIStyle(GUI.skin.label); _defaultStyle.alignment = TextAnchor.MiddleRight; } return _defaultStyle; } }

        // NodeInput Members
        [SerializeField]
        private List<NodeInput> connections = new List<NodeInput>();
        public NodeInput connection;

        [FormerlySerializedAs("type")]
        public string typeID;

        private TypeData _typeData;
        internal TypeData typeData { get { CheckType(); return _typeData; } }

        [System.NonSerialized]
        private object value = null;

        public bool calculationBlockade = false;

        #region General

        /// <summary>
        /// Creates a new NodeOutput in NodeBody of specified type
        /// </summary>
        public static NodeOutput Create(Node nodeBody, string outputName, string outputType, bool isRuntime)
        {
            return Create(nodeBody, outputName, outputType, NodeSide.Right, 20, isRuntime);
        }

        /// <summary>
        /// Creates a new NodeOutput in NodeBody of specified type
        /// </summary>
        public static NodeOutput Create(Node nodeBody, string outputName, string outputType, NodeSide nodeSide, bool isRuntime)
        {
            return Create(nodeBody, outputName, outputType, nodeSide, 20, isRuntime);
        }

        /// <summary>
        /// Creates a new NodeOutput in NodeBody of specified type at the specified Node Side
        /// </summary>
        public static NodeOutput Create(Node nodeBody, string outputName, string outputType, NodeSide nodeSide, float sidePosition, bool isRuntime)
        {
            NodeOutput output = CreateInstance<NodeOutput>();
            output.typeID = outputType;
            output.InitBase(nodeBody, nodeSide, sidePosition, outputName, isRuntime);
            nodeBody.Outputs.Add(output);
            return output;
        }

        public override void Delete()
        {
            connection.RemoveConnection();
            //while (connections.Count > 0)
            //    connections[0].RemoveConnection();
            body.Outputs.Remove(this);
            base.Delete();
        }

        #endregion General

        #region Additional Serialization

        protected internal override void CopyScriptableObjects(System.Func<ScriptableObject, ScriptableObject> replaceSerializableObject)
        {
            connection = replaceSerializableObject.Invoke(connection) as NodeInput;
            //for (int conCnt = 0; conCnt < connections.Count; conCnt++)
            //    connections[conCnt] = replaceSerializableObject.Invoke(connections[conCnt]) as NodeInput;
        }

        #endregion Additional Serialization

        #region KnobType

        protected override void ReloadTexture()
        {
            CheckType();
            knobTexture = typeData.OutKnobTex;
        }

        private void CheckType()
        {
            if (_typeData == null || !_typeData.isValid())
                _typeData = ConnectionTypes.GetTypeData(typeID);
            if (_typeData == null || !_typeData.isValid())
            {
                ConnectionTypes.FetchTypes();
                _typeData = ConnectionTypes.GetTypeData(typeID);
                if (_typeData == null || !_typeData.isValid())
                    throw new UnityException("Could not find type " + typeID + "!");
            }
        }

        #endregion KnobType

        #region Value

        public bool IsValueNull { get { return value == null; } }

        /// <summary>
        /// Gets the output value anonymously. Not advised as it may lead to unwanted behaviour!
        /// </summary>
        public object GetValue()
        {
            return value;
        }

        /// <summary>
        /// Gets the output value if the type matches or null. If possible, use strongly typed version instead.
        /// </summary>
        public object GetValue(Type type)
        {
            if (type == null)
                throw new UnityException("Trying to get value of " + name + " with null type!");
            CheckType();
            if (type.IsAssignableFrom(typeData.Type))
                return value;
            Debug.LogError("Trying to GetValue<" + type.FullName + "> for Output Type: " + typeData.Type.FullName);
            return null;
        }

        /// <summary>
        /// Sets the output value if the type matches. If possible, use strongly typed version instead.
        /// </summary>
        public void SetValue(object Value)
        {
            CheckType();
            if (Value == null || typeData.Type.IsAssignableFrom(Value.GetType()))
                value = Value;
            else
                Debug.LogError("Trying to SetValue of type " + Value.GetType().FullName + " for Output Type: " + typeData.Type.FullName);
        }

        /// <summary>
        /// Gets the output value if the type matches
        /// </summary>
        /// <returns>Value, if null default(T) (-> For reference types, null. For value types, default value)</returns>
        public T GetValue<T>()
        {
            CheckType();
            if (typeof(T).IsAssignableFrom(typeData.Type))
                return (T)(value ?? (value = GetDefault<T>()));
            Debug.LogError("Trying to GetValue<" + typeof(T).FullName + "> for Output Type: " + typeData.Type.FullName);
            return GetDefault<T>();
        }

        /// <summary>
        /// Sets the output value if the type matches
        /// </summary>
        public void SetValue<T>(T Value)
        {
            CheckType();
            if (typeData.Type.IsAssignableFrom(typeof(T)))
                value = Value;
            else
                Debug.LogError("Trying to SetValue<" + typeof(T).FullName + "> for Output Type: " + typeData.Type.FullName);
        }

        /// <summary>
        /// Resets the output value to null.
        /// </summary>
        public void ResetValue()
        {
            value = null;
        }

        /// <summary>
        /// Returns the default value of type when a default constructor is existant or type is a value type, else null
        /// </summary>
        public static T GetDefault<T>()
        {
            // Try to create using an empty constructor if existant
            if (typeof(T).GetConstructor(System.Type.EmptyTypes) != null)
                return System.Activator.CreateInstance<T>();
            // Else try to get default. Returns null only on reference types
            return default(T);
        }

        /// <summary>
        /// Returns the default value of type when a default constructor is existant, else null
        /// </summary>
        public static object GetDefault(Type type)
        {
            // Try to create using an empty constructor if existant
            if (type.GetConstructor(System.Type.EmptyTypes) != null)
                return System.Activator.CreateInstance(type);
            return null;
        }

        #endregion Value

        #region Utility

        public override Node GetNodeAcrossConnection()
        {
            //return connections.Count > 0 ? connections[0].body : null;
            return connection != null ? connection.body : null;
        }

        public void OnBeforeSerialize()
        {
            //throw new NotImplementedException();
        }

        public void OnAfterDeserialize()
        {
            if (connections.Count > 0)
                connection = connections[0];
            connections = null;
        }

        #endregion Utility
    }
}