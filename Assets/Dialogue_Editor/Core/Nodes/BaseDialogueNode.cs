﻿using ExMan;
using NodeEditorFramework;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Serialization;

[Node(true, "Dialogue/Base Dialogue Node", new Type[] { typeof(DialogueNodeCanvas) })]
public abstract class BaseDialogueNode : Node
{
    public override bool allowRecursion { get { return true; } }
    public abstract Type getObjectType { get; }

    protected virtual int height { get { return 170; } }

    public CharacterTypes talkingCharacter;
    public Texture2D characterImage;
    [FormerlySerializedAs("endResult")]
    public string talkingText;
    public AudioClip talkingClip;
    public string customerSatisfactionReason;
    public float modifierCustomerSatisfaction;
    public string staffSatisfactionReason;
    public float modifierStaffSatisfaction;
    public string moneyReason;
    public float modifierMonies;
    public string eventName = string.Empty;

    public abstract BaseDialogueNode Input(int inputValue);

    public abstract bool IsBackAvailable();

    public abstract bool IsNextAvailable();

    public override Node Clone(Vector2 pos)
    {
        BaseDialogueNode n = (BaseDialogueNode)Create(pos);
        n.talkingCharacter = talkingCharacter;
        n.talkingText = talkingText;
        n.talkingClip = talkingClip;
        n.modifierCustomerSatisfaction = modifierCustomerSatisfaction;
        n.modifierMonies = modifierMonies;
        n.modifierStaffSatisfaction = modifierStaffSatisfaction;
        n.eventName = eventName;
        return n;
    }

    public virtual BaseDialogueNode PassAhead(int inputValue)
    {
        return this;
    }

    protected internal override void NodeGUI()
    {
#if UNITY_EDITOR
        talkingCharacter = (CharacterTypes)EditorGUILayout.EnumPopup("Character:", talkingCharacter);
        talkingClip = (AudioClip)EditorGUILayout.ObjectField("Audio Clip:", talkingClip, typeof(AudioClip), false);

        var style = new GUIStyle(EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).textField);
        style.wordWrap = true;
        talkingText = EditorGUILayout.TextArea(talkingText, style, GUILayout.Height(100));
#endif
    }

    public override void DrawNodePropertyEditor()
    {
#if UNITY_EDITOR
        characterImage = (Texture2D)EditorGUILayout.ObjectField("Image:", characterImage, typeof(Texture2D), false);

        modifierCustomerSatisfaction = EditorGUILayout.FloatField("Customer Satisfaction Modifier:", modifierCustomerSatisfaction);
        customerSatisfactionReason = EditorGUILayout.TextField("Customer Satisfaction Reason: ", customerSatisfactionReason);
        modifierStaffSatisfaction = EditorGUILayout.FloatField("Staff Satisfaction Modifier:", modifierStaffSatisfaction);
        staffSatisfactionReason = EditorGUILayout.TextField("Staff Satisfaction Reason: ", staffSatisfactionReason);
        modifierMonies = EditorGUILayout.FloatField("Money Modifier:", modifierMonies);
        moneyReason = EditorGUILayout.TextField("Money Reason: ", moneyReason);
        eventName = EditorGUILayout.TextField("Enter Event:", eventName);
#endif
    }
}

public class DialogueBackType : IConnectionTypeDeclaration
{
    public string Identifier { get { return "DialogueBack"; } }
    public Type Type { get { return typeof(void); } }
    public Color Color { get { return Color.red; } }
    public string InKnobTex { get { return "Textures/In_Knob.png"; } }
    public string OutKnobTex { get { return "Textures/Out_Knob.png"; } }
}

public class DialogueForwardType : IConnectionTypeDeclaration
{
    public string Identifier { get { return "DialogueForward"; } }
    public Type Type { get { return typeof(float); } }
    public Color Color { get { return Color.cyan; } }
    public string InKnobTex { get { return "Textures/In_Knob.png"; } }
    public string OutKnobTex { get { return "Textures/Out_Knob.png"; } }
}