﻿using ExMan;
using NodeEditorFramework;
using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[Node(false, "Dialogue/Dialogue Start Node", new Type[] { typeof(DialogueNodeCanvas) })]
public class DialogueStartNode : DialogueMultiOptionsNode
{
    public override string nodeID { get { return "dialogueStartNode"; } }

    public override Node Create(Vector2 pos)
    {
        DialogueStartNode node = CreateBase(false);

        node.rect = new Rect(pos.x, pos.y, 300, height);

        return node;
    }

    public override Node CreateRuntime()
    {
        return CreateBase(true);
    }

    private DialogueStartNode CreateBase(bool isRuntime)
    {
        DialogueStartNode node = CreateInstance<DialogueStartNode>();

        node.name = "Dialogue Start";

        node.talkingCharacter = CharacterTypes.None;
        node.talkingText = string.Empty;

        node.options = new List<DataHolderForOption>();

        for (int i = 0; i < 4; i++)
            node.AddNewOption(isRuntime);

        return node;
    }

    protected internal override void NodeGUI()
    {
        base.NodeGUI();
    }

    public override BaseDialogueNode Input(int inputValue)
    {
        if (Outputs[0].GetNodeAcrossConnection() != default(Node))
            return Outputs[0].GetNodeAcrossConnection() as BaseDialogueNode;
        return null;
    }

    public override bool IsBackAvailable()
    {
        return false;
    }

    public override bool IsNextAvailable()
    {
        return Outputs[0].GetNodeAcrossConnection() != default(Node);
    }
}