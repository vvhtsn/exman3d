﻿using NodeEditorFramework;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[Serializable]
[Node(false, "Dialogue/Dialogue Exit Node", new Type[] { typeof(DialogueNodeCanvas) })]
public class DialogueExitNode : BaseDialogueNode
{
    public string gameOverReason;

    public override string nodeID { get { return "dialogueExitNode"; } }

    public override Type getObjectType
    {
        get
        {
            return typeof(DialogueExitNode);
        }
    }

    public override Node Create(Vector2 pos)
    {
        DialogueExitNode node = CreateBase(false);

        node.rect = new Rect(pos.x, pos.y, 300, 130);

        return node;
    }

    public override Node CreateRuntime()
    {
        return CreateBase(true);
    }

    private DialogueExitNode CreateBase(bool isRuntime)
    {
        DialogueExitNode node = CreateInstance<DialogueExitNode>();
        node.name = "Dialogue Exit";
        node.CreateInput("Previous Node", "DialogueForward", NodeSide.Left, 10, isRuntime);
        return node;
    }

    protected internal override void NodeGUI()
    {
#if UNITY_EDITOR
        var style = new GUIStyle(EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).textField);
        style.wordWrap = true;
        talkingText = EditorGUILayout.TextArea(talkingText, style, GUILayout.Height(100));
#endif
    }

    public override void DrawNodePropertyEditor()
    {
#if UNITY_EDITOR
        base.DrawNodePropertyEditor();
        gameOverReason = EditorGUILayout.TextField("Game over reason:", gameOverReason);
#endif
    }

    public override BaseDialogueNode Input(int inputValue)
    {
        if (Outputs[0].GetNodeAcrossConnection() != default(Node))
            return Outputs[0].GetNodeAcrossConnection() as BaseDialogueNode;
        return null;
    }

    public override bool IsBackAvailable()
    {
        return false;
    }

    public override bool IsNextAvailable()
    {
        return Outputs[0].GetNodeAcrossConnection() != default(Node);
    }
}