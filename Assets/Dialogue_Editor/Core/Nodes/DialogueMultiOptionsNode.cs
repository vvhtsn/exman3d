﻿using ExMan;
using NodeEditorFramework;
using System;
using System.Collections.Generic;
using System.Linq;

#if UNITY_EDITOR

using UnityEditor;

#endif

using UnityEngine;

[Serializable]
[Node(false, "Dialogue/Dialogue With Options Node", new Type[] { typeof(DialogueNodeCanvas) })]
public class DialogueMultiOptionsNode : BaseDialogueNode
{
    public override string nodeID { get { return "multiOptionDialogueNode"; } }
    public override Type getObjectType { get { return typeof(DialogueMultiOptionsNode); } }

    protected override int height { get { return base.height + 142; } }

    protected int outputYStartValue = 4;
    protected int outputYSizeValue = 36;

    [SerializeField]
    public List<DataHolderForOption> options;

    public override Node Create(Vector2 pos)
    {
        DialogueMultiOptionsNode node = CreateBase(false);

        node.rect = new Rect(pos.x, pos.y, 300, height);

        return node;
    }

    public override Node CreateRuntime()
    {
        return CreateBase(true);
    }

    private DialogueMultiOptionsNode CreateBase(bool isRuntime)
    {
        DialogueMultiOptionsNode node = CreateInstance<DialogueMultiOptionsNode>();
        node.name = "Dialogue";

        node.CreateInput("Previous Node", "DialogueForward", NodeSide.Left, 30, isRuntime);

        node.talkingCharacter = CharacterTypes.None;
        node.talkingText = string.Empty;

        node.options = new List<DataHolderForOption>();

        for (int i = 0; i < 4; i++)
            node.AddNewOption(isRuntime);

        return node;
    }

    public override Node Clone(Vector2 pos)
    {
        DialogueMultiOptionsNode n = (DialogueMultiOptionsNode)base.Clone(pos);

        for (int i = 0; i < options.Count; i++)
        {
            options[i].Copy(n.options[i]);
        }

        return n;
    }

    protected internal override void NodeGUI()
    {
        base.NodeGUI();

        GUILayout.Space(5);
        DrawOptions();

        //GUILayout.BeginHorizontal();
        //GUILayout.BeginVertical();

        //GUILayout.Space(5);
        //if (GUILayout.Button("Add New Option"))
        //{
        //    AddNewOption();
        //    IssueEditorCallBacks();
        //}

        //GUILayout.EndVertical();
        //GUILayout.EndHorizontal();

        //GUILayout.BeginHorizontal();
        //GUILayout.BeginVertical();

        //GUILayout.Space(5);
        //if (GUILayout.Button("Remove Last Option"))
        //{
        //    Debug.Log("Remove options is clicked");
        //    RemoveLastOption();
        //}

        //GUILayout.EndVertical();
        //GUILayout.EndHorizontal();
    }

    public override void DrawNodePropertyEditor()
    {
#if UNITY_EDITOR
        base.DrawNodePropertyEditor();

        var style = new GUIStyle(EditorGUIUtility.GetBuiltinSkin(EditorSkin.Inspector).textField);
        style.wordWrap = true;

        GUIStyle s = new GUIStyle(EditorStyles.label);
        s.normal.textColor = Color.black;

        int i = 0;
        foreach (var option in options)
        {
            i++;
            GUILayout.BeginVertical();
            GUILayout.Label("Option " + i + ":");
            option.optionImage = (Texture2D)EditorGUILayout.ObjectField("Image:", option.optionImage, typeof(Texture2D), false);
            GUILayout.BeginHorizontal();
            GUILayout.Label("Text:", s);
            option.optionText = EditorGUILayout.TextArea(option.optionText, style, GUILayout.Height(50), GUILayout.Width(223));
            GUILayout.EndHorizontal();

            option.clickedEvent = EditorGUILayout.TextField("Event:", option.clickedEvent);
            GUILayout.EndVertical();
        }
#endif
    }

    private void RemoveLastOption()
    {
        if (options.Count > 1)
        {
            DataHolderForOption option = options.Last();
            options.Remove(option);
            Outputs[option.nodeOutputIndex].Delete();
            rect = new Rect(rect.x, rect.y, rect.width, rect.height - outputYSizeValue);
        }
    }

    private void DrawOptions()
    {
#if UNITY_EDITOR
        int i = 0;
        foreach (DataHolderForOption option in options)
        {
            i++;
            GUILayout.BeginHorizontal();
            GUILayout.Label("Button " + i + ":", GUILayout.Width(60));
            GUILayout.BeginVertical();
            option.optionDisplay = EditorGUILayout.TextField(option.optionDisplay);
            option.optionClip = (AudioClip)EditorGUILayout.ObjectField(option.optionClip, typeof(AudioClip), false);
            GUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }
#endif
    }

    protected void AddNewOption(bool isRuntime)
    {
        DataHolderForOption option = new DataHolderForOption { optionDisplay = "" };
        CreateOutput("Next Node", "DialogueForward", NodeSide.Right,
            height - 142 + outputYStartValue + options.Count * outputYSizeValue, isRuntime);
        option.nodeOutputIndex = Outputs.Count - 1;
        rect = new Rect(rect.x, rect.y, rect.width, rect.height + outputYSizeValue);
        options.Add(option);
    }

    //For Resolving the Type Mismatch Issue
    private void IssueEditorCallBacks()
    {
        DataHolderForOption option = options.Last();
        NodeEditorCallbacks.IssueOnAddNodeKnob(Outputs[option.nodeOutputIndex]);
    }

    public override BaseDialogueNode Input(int inputValue)
    {
        if (Outputs[options[inputValue].nodeOutputIndex].GetNodeAcrossConnection() != default(Node))
            return Outputs[options[inputValue].nodeOutputIndex].GetNodeAcrossConnection() as BaseDialogueNode;
        return null;
    }

    public override bool IsBackAvailable()
    {
        return Outputs[0].GetNodeAcrossConnection() != default(Node);
    }

    public override bool IsNextAvailable()
    {
        return false;
    }

    [Serializable]
    public class DataHolderForOption
    {
        public string optionDisplay;
        public string optionText;
        public Texture2D optionImage;
        public AudioClip optionClip;
        public int nodeOutputIndex;
        public string clickedEvent;

        public void Copy(DataHolderForOption to)
        {
            to.optionDisplay = optionDisplay;
            to.optionText = optionText;
            to.optionImage = optionImage;
            to.optionClip = optionClip;
            to.clickedEvent = clickedEvent;
        }
    }
}