﻿using ExMan;
using NodeEditorFramework;
using System;
using UnityEngine;

[Serializable]
[Node(false, "Dialogue/Dialogue Start without options Node", new Type[] { typeof(DialogueNodeCanvas) })]
public class DialogueStartNoOptionsNode : BaseDialogueNode
{
    public override string nodeID { get { return "dialogueStartNoOptionsNode"; } }

    public override Type getObjectType
    {
        get
        {
            return typeof(DialogueStartNoOptionsNode);
        }
    }

    public override Node Create(Vector2 pos)
    {
        DialogueStartNoOptionsNode node = CreateBase(false);

        node.rect = new Rect(pos.x, pos.y, 300, height);

        return node;
    }

    public override Node CreateRuntime()
    {
        return CreateBase(true);
    }

    private DialogueStartNoOptionsNode CreateBase(bool isRuntime)
    {
        DialogueStartNoOptionsNode node = CreateInstance<DialogueStartNoOptionsNode>();

        node.name = "Dialogue Start No Options";

        node.talkingCharacter = CharacterTypes.None;
        node.talkingText = string.Empty;
        node.CreateOutput("Previous Node", "DialogueForward", NodeSide.Right, 30, isRuntime);

        return node;
    }

    protected internal override void NodeGUI()
    {
        base.NodeGUI();
    }

    public override BaseDialogueNode Input(int inputValue)
    {
        if (Outputs[0].GetNodeAcrossConnection() != default(Node))
            return Outputs[0].GetNodeAcrossConnection() as BaseDialogueNode;
        return null;
    }

    public override bool IsBackAvailable()
    {
        return false;
    }

    public override bool IsNextAvailable()
    {
        return Outputs[0].GetNodeAcrossConnection() != default(Node);
    }
}