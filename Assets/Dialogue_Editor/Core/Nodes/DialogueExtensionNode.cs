﻿using NodeEditorFramework;
using System;
using UnityEngine;

[Serializable]
[Node(false, "Dialogue/Dialogue Node", new Type[] { typeof(DialogueNodeCanvas) })]
public class DialogueExtensionNode : BaseDialogueNode
{
    public override string nodeID { get { return "dialogueExtensionNode"; } }
    public override Type getObjectType { get { return typeof(DialogueExtensionNode); } }

    public override Node Create(Vector2 pos)
    {
        DialogueExtensionNode node = CreateBase(false);

        node.rect = new Rect(pos.x, pos.y, 300, height);

        return node;
    }

    public override Node CreateRuntime()
    {
        return CreateBase(true);
    }

    public DialogueExtensionNode CreateBase(bool isRuntime)
    {
        DialogueExtensionNode node = CreateInstance<DialogueExtensionNode>();

        node.name = "Dialogue";

        node.CreateInput("Previous Node", "DialogueForward", NodeSide.Left, 30, isRuntime);
        node.CreateOutput("Previous Node", "DialogueForward", NodeSide.Right, 30, isRuntime);

        return node;
    }

    public override BaseDialogueNode Input(int inputValue)
    {
        if (Outputs[inputValue].GetNodeAcrossConnection() != default(Node))
            return Outputs[inputValue].GetNodeAcrossConnection() as BaseDialogueNode;
        return null;
    }

    public override bool IsBackAvailable()
    {
        return Outputs[0].GetNodeAcrossConnection() != default(Node);
    }

    public override bool IsNextAvailable()
    {
        return false;
    }

    protected internal override void NodeGUI()
    {
        base.NodeGUI();
    }
}