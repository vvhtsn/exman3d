﻿using NodeEditorFramework;

#if UNITY_EDITOR

using UnityEditor;

#endif

[NodeCanvasType("Dialogue Canvas")]
public class DialogueNodeCanvas : NodeCanvas
{
    public BaseDialogueNode startNode
    {
        get
        {
            return (BaseDialogueNode)nodes[startNodeIndex];
        }
    }

    public int startNodeIndex = -1;

    public override void BeforeSavingCanvas()
    {
#if UNITY_EDITOR
        if (nodes == null || nodes.Count == 0)
            return;

        int exitNodeCount = 0;
        int i = 0;
        startNodeIndex = -1;
        foreach (Node node in nodes)
        {
            if (node is DialogueStartNode || node is DialogueStartNoOptionsNode)
            {
                if (startNodeIndex != -1)
                {
                    EditorUtility.DisplayDialog("Error", "There should only be one Dialogue Start Node", "ok");
                }
                else
                {
                    startNodeIndex = i;
                }
            }
            else if (node is DialogueExitNode)
            {
                exitNodeCount++;
            }
            i++;
        }

        if (startNodeIndex == -1)
            EditorUtility.DisplayDialog("Error", "There is no Dialogue Start Node", "ok");
        if (exitNodeCount == 0)
            EditorUtility.DisplayDialog("Error", "There is no Dialogue Exit Node", "ok");
#endif
    }
}